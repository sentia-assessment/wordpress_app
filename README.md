# Docker PHP Wordpress Image
The following project is a template for setting up and configuring Automated Builds with GitLab CI and AWS ECR for PHP WordPress.

## ECR Repo
The template solution will push to a pre-staged ECR repo in the specified AWS Account entered in the `.gitlab-ci.yml` file 

## GitLab CI
The template solution is based on the following .gitlab-ci.yml file:

## Gitlab Configuration
* Configure your AWS account Access_key and secret ID in Gitlab variable section to grant the gitlab runner acccess to deploy to your AWS account

### Repository Structure
<pre>
    |-deploy/ 
    |- create-update-stack.sh        [ Shell  script  used  for deploying  latest  image  build ] 
    |-docker/
        |-src/      
        |-.htaccess                [Configuration for Permalinks] 
        |- Dockerfile              [ Steps  for  building WordPress  docker  image] 
        |- docker-compose.yml      [File  to  build  containers  locally  when  testing locally] 
        |- docker-entrypoint.sh    [shell  script  used  to  start  WordPress  in  ECS  container]   
</pre>

### Requirement

- Docker
- Git
- AWS Account

### Development

* Create new branch off development branch
```
    git checkout -b <new_branch_name>
```

#### Test Build

* Build image with fix
```
    docker-compose up
```
* Check running containers after executing command above to get the port number of the application
```
    docker ps
```
* Launch web browser, enter localhost:<port_number> in the web browser to access WordPress application