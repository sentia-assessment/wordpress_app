		<ul class="to-form-field-list">
			<li>
				<h5><?php esc_html_e('CSS styles','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('CSS styles for page width between 768px and 959px.','atrium'); ?></span>
				<div>
					<textarea id="<?php ThemeHelper::getFormName('custom_css_responsive_2'); ?>" name="<?php ThemeHelper::getFormName('custom_css_responsive_2'); ?>" rows="1" cols="1" class="css-editor"><?php echo ThemeHelper::esc_html($this->data['option']['custom_css_responsive_2']); ?></textarea>
				</div>						
			</li>
		</ul>