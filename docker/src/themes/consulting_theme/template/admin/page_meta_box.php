		<?php echo $this->data['nonce']; ?>

		<div class="to">
			<ul class="to-form-field-list">
				<li>
					<h5><?php esc_html_e('Sidebar','atrium'); ?></h5>
					<span class="to-legend"><?php esc_html_e('Select sidebar and sidebar location.','atrium'); ?></span>
					<div class="to-clear-fix">
						<span class="to-legend-field"><?php esc_html_e('Sidebar:','atrium'); ?></span>
						<select name="<?php ThemeHelper::getFormName('page_content_widget_area'); ?>" id="<?php ThemeHelper::getFormName('page_content_widget_area'); ?>">
<?php
							foreach($this->data['dictionary']['widgetArea'] as $index=>$value)
								echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_content_widget_area'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
						</select>
					</div>
					<div class="to-clear-fix">
						<span class="to-legend-field"><?php esc_html_e('Location:','atrium'); ?></span>
						<select name="<?php ThemeHelper::getFormName('page_content_widget_area_location'); ?>" id="<?php ThemeHelper::getFormName('page_content_widget_area_location'); ?>">
<?php
							foreach($this->data['dictionary']['widgetAreaLocation'] as $index=>$value)
								echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_content_widget_area_location'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
						</select>
					</div>
				</li>
				<li>
					<h5><?php esc_html_e('Widget area in footer','atrium'); ?></h5>
					<span class="to-legend"><?php esc_html_e('Select widget area in footer.','atrium'); ?></span>
					<div class="to-clear-fix">
						<select name="<?php ThemeHelper::getFormName('page_footer_widget_area'); ?>" id="<?php ThemeHelper::getFormName('page_footer_widget_area'); ?>">
<?php
							foreach($this->data['dictionary']['widgetArea'] as $index=>$value)
								echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_footer_widget_area'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
						</select>
					</div>
				</li>
				<li>
					<h5><?php esc_html_e('Top menu','atrium'); ?></h5>
					<span class="to-legend"><?php esc_html_e('Select top menu.','atrium'); ?></span>
					<div class="to-clear-fix">
						<select name="<?php ThemeHelper::getFormName('page_menu_top'); ?>" id="<?php ThemeHelper::getFormName('page_menu_top'); ?>">
<?php
						foreach($this->data['dictionary']['menu'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_menu_top'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
						</select>
					</div>
				</li>				
				<li>
					<h5><?php esc_html_e('Show page header','atrium'); ?></h5>
					<span class="to-legend"><?php esc_html_e('Show page header.','atrium'); ?></span>
					<div class="to-radio-button">
						<input type="radio" name="<?php ThemeHelper::getFormName('page_header_visible'); ?>" id="<?php ThemeHelper::getFormName('page_header_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['page_header_visible'],1); ?>/>
						<label for="<?php ThemeHelper::getFormName('page_header_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
						<input type="radio" name="<?php ThemeHelper::getFormName('page_header_visible'); ?>" id="<?php ThemeHelper::getFormName('page_header_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['page_header_visible'],0); ?>/>
						<label for="<?php ThemeHelper::getFormName('page_header_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
						<input type="radio" name="<?php ThemeHelper::getFormName('page_header_visible'); ?>" id="<?php ThemeHelper::getFormName('page_header_visible_2'); ?>" value="-1" <?php ThemeHelper::checkedIf($this->data['option']['page_header_visible'],-1); ?>/>
						<label for="<?php ThemeHelper::getFormName('page_header_visible_2'); ?>"><?php esc_html_e('Use global settings','atrium'); ?></label>
					</div>
				</li>
				<li>
					<h5><?php esc_html_e('Restrict displaying posts only from selected categories','atrium'); ?></h5>
					<span class="to-legend">
						<?php esc_html_e('Select blog categories from which posts have to be displayed.','atrium'); ?><br/>
						<?php esc_html_e('This option is available only from blog pages.','atrium'); ?>
					</span>
					<div class="to-checkbox-button">
<?php
		$i=0;
		foreach($this->data['dictionary']['postCategory'] as $index=>$value)
		{
			$i++;
?>
						<input type="checkbox" name="<?php ThemeHelper::getFormName('page_post_category[]'); ?>" id="<?php ThemeHelper::getFormName('page_post_category_'.$i); ?>" value="<?php echo ThemeHelper::esc_attr($index); ?>" <?php ThemeHelper::checkedIf($this->data['option']['page_post_category'],$index); ?>/>
						<label for="<?php ThemeHelper::getFormName('page_post_category_'.$i); ?>"><?php echo ThemeHelper::esc_html($value); ?></label>
<?php
		}
?>
					</div>
				</li>
				
			</ul>
		</div>

		<script type="text/javascript">
			jQuery(document).ready(function($)
			{	
				$('.to').themeOptionElement({init:true});
			});
		</script>