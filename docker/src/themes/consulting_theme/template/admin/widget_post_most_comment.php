		<p>
			<label for="<?php echo esc_attr($this->data['option']['title']['id']); ?>"><?php esc_html_e('Title','atrium'); ?>:</label>
			<input class="widefat" id="<?php echo esc_attr($this->data['option']['title']['id']); ?>" name="<?php echo esc_attr($this->data['option']['title']['name']); ?>" type="text" value="<?php echo esc_attr($this->data['option']['title']['value']); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->data['option']['date_box_enable']['id']); ?>"><?php esc_html_e('Show date box','atrium'); ?>:</label>
			<br/>
			<input id="<?php echo esc_attr($this->data['option']['date_box_enable']['id']); ?>" name="<?php echo esc_attr($this->data['option']['date_box_enable']['name']); ?>" type="checkbox" value="1" <?php echo $this->data['option']['date_box_enable']['value']==1 ? 'checked' : null;  ?> />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->data['option']['post_count']['id']); ?>"><?php esc_html_e('Total number of posts to show','atrium'); ?>:</label>
			<input class="widefat" id="<?php echo esc_attr($this->data['option']['post_count']['id']); ?>" name="<?php echo esc_attr($this->data['option']['post_count']['name']); ?>" type="text" value="<?php echo esc_attr($this->data['option']['post_count']['value']); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->data['option']['carousel']['id']); ?>"><?php esc_html_e('Enable carousel','atrium'); ?>:</label>
			<br/>
			<input id="<?php echo esc_attr($this->data['option']['carousel']['id']); ?>" name="<?php echo esc_attr($this->data['option']['carousel']['name']); ?>" type="checkbox" value="1" <?php echo $this->data['option']['carousel']['value']==1 ? 'checked' : null;  ?> />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->data['option']['carousel_post_count']['id']); ?>"><?php esc_html_e('Number of visible posts in carousel','atrium'); ?>:</label>
			<input class="widefat" id="<?php echo esc_attr($this->data['option']['carousel_post_count']['id']); ?>" name="<?php echo esc_attr($this->data['option']['carousel_post_count']['name']); ?>" type="text" value="<?php echo esc_attr($this->data['option']['carousel_post_count']['value']); ?>" />
		</p>
