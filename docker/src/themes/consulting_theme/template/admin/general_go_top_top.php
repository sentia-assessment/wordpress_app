		<ul class="to-form-field-list">
			<li>
				<h5><?php esc_html_e('Enable "Go to page top"','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enable or disable hash "Go to page top".','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('go_to_page_top_enable'); ?>" id="<?php ThemeHelper::getFormName('go_to_page_top_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['go_to_page_top_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('go_to_page_top_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('go_to_page_top_enable'); ?>" id="<?php ThemeHelper::getFormName('go_to_page_top_enable_2'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['go_to_page_top_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('go_to_page_top_enable_2'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>			
			</li>
			<li>
				<h5><?php esc_html_e('Hash','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Hash','atrium'); ?><br/></span>
				<div class="to-clear-fix">
					<input type="text" name="<?php ThemeHelper::getFormName('go_to_page_top_hash'); ?>" id="<?php ThemeHelper::getFormName('go_to_page_top_hash'); ?>" value="<?php echo ThemeHelper::esc_attr($this->data['option']['go_to_page_top_hash']); ?>" />
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Animation','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enable or disable animation during scrolling.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('go_to_page_top_animation_enable'); ?>" id="<?php ThemeHelper::getFormName('go_to_page_top_animation_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['go_to_page_top_animation_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('go_to_page_top_animation_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('go_to_page_top_animation_enable'); ?>" id="<?php ThemeHelper::getFormName('go_to_page_top_animation_enable_2'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['go_to_page_top_animation_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('go_to_page_top_animation_enable_2'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>			
			</li>			
			<li>
				<h5><?php esc_html_e('Duration','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Duration of animation in miliseconds','atrium'); ?><br/></span>
				<div class="to-clear-fix">
					<input type="text" name="<?php ThemeHelper::getFormName('go_to_page_top_animation_duration'); ?>" id="<?php ThemeHelper::getFormName('go_to_page_top_animation_duration'); ?>" value="<?php echo ThemeHelper::esc_attr($this->data['option']['go_to_page_top_animation_duration']); ?>" maxlength="5"/>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Easing','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Easing method of animation.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('go_to_page_top_animation_easing'); ?>" id="<?php ThemeHelper::getFormName('go_to_page_top_animation_easing'); ?>">
<?php
						foreach($this->data['dictionary']['easingType'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['go_to_page_top_animation_easing'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>
		</ul>