		<ul class="to-form-field-list">
			<li>
				<h5><?php esc_html_e('Logo','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enter URL of image','atrium'); ?><br/></span>
				<div class="to-clear-fix">
					<input type="text" name="<?php ThemeHelper::getFormName('menu_logo_src'); ?>" id="<?php ThemeHelper::getFormName('menu_logo_src'); ?>" class="to-float-left" value="<?php echo ThemeHelper::esc_attr($this->data['option']['menu_logo_src']); ?>" />
					<input type="button" name="<?php ThemeHelper::getFormName('menu_logo_src_browse'); ?>" id="<?php ThemeHelper::getFormName('menu_logo_src_browse'); ?>" class="to-button-browse to-button" value="<?php esc_attr_e('Browse','atrium'); ?>"/>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Mobile menu','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Turn on mobile menu when width of the page is lower than:','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_responsive_level'); ?>" id="<?php ThemeHelper::getFormName('menu_responsive_level_1'); ?>" value="960" <?php ThemeHelper::checkedIf($this->data['option']['menu_responsive_level'],960); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_responsive_level_1'); ?>"><?php esc_html_e('990px','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_responsive_level'); ?>" id="<?php ThemeHelper::getFormName('menu_responsive_level_2'); ?>" value="768" <?php ThemeHelper::checkedIf($this->data['option']['menu_responsive_level'],768); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_responsive_level_2'); ?>"><?php esc_html_e('768px','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_responsive_level'); ?>" id="<?php ThemeHelper::getFormName('menu_responsive_level_3'); ?>" value="480" <?php ThemeHelper::checkedIf($this->data['option']['menu_responsive_level'],480); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_responsive_level_3'); ?>"><?php esc_html_e('480px','atrium'); ?></label>
				</div>			
			</li>
			<li>
				<h5><?php esc_html_e('Sticky menu','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enable or disable sticky menu.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_sticky_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_sticky_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['menu_sticky_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_sticky_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_sticky_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_sticky_enable_2'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['menu_sticky_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_sticky_enable_2'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>			
			</li>
			<li>
				<h5><?php esc_html_e('Sticky menu hide','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Hide sticky menu during scrolling.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_hide_scroll_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_hide_scroll_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['menu_hide_scroll_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_hide_scroll_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_hide_scroll_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_hide_scroll_enable_2'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['menu_hide_scroll_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_hide_scroll_enable_2'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>			
			</li>
			<li>
				<h5><?php esc_html_e('Hover on first level menu elements','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Disable or enable hover on first level menu elements.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_hover_first_level_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_hover_first_level_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['menu_hover_first_level_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_hover_first_level_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_hover_first_level_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_hover_first_level_enable_2'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['menu_hover_first_level_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_hover_first_level_enable_2'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>			
			</li>
			<li>
				<h5><?php esc_html_e('Hover on next levels menu elements','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Disable or enable hover on next levels menu elements.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_hover_next_level_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_hover_next_level_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['menu_hover_next_level_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_hover_next_level_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_hover_next_level_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_hover_next_level_enable_2'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['menu_hover_next_level_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_hover_next_level_enable_2'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>			
			</li>
			<li>
				<h5><?php esc_html_e('Animation','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enable or disable animation during scrolling.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_animation_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_animation_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['menu_animation_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_animation_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('menu_animation_enable'); ?>" id="<?php ThemeHelper::getFormName('menu_animation_enable_2'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['menu_animation_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('menu_animation_enable_2'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>			
			</li>			
			<li>
				<h5><?php esc_html_e('Duration','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Duration of animation in miliseconds','atrium'); ?><br/></span>
				<div class="to-clear-fix">
					<input type="text" name="<?php ThemeHelper::getFormName('menu_animation_duration'); ?>" id="<?php ThemeHelper::getFormName('menu_animation_duration'); ?>" value="<?php echo ThemeHelper::esc_attr($this->data['option']['menu_animation_duration']); ?>" maxlength="5"/>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Easing','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Easing method of animation.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('menu_animation_easing'); ?>" id="<?php ThemeHelper::getFormName('menu_animation_easing'); ?>">
<?php
						foreach($this->data['dictionary']['easingType'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['menu_animation_easing'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>
		</ul>

		<script type="text/javascript">
			jQuery(document).ready(function($) 
			{
				var element=$('.to').themeOptionElement();;
				element.bindBrowseMedia('#<?php ThemeHelper::getFormName('menu_logo_src_browse'); ?>');
			});
		</script>