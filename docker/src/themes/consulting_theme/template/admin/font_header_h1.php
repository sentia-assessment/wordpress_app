		<ul class="to-form-field-list">
			<li>
				<h5><?php esc_html_e('Google Font','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enter name of Google Font and select it from list.','atrium'); ?></span>
				<div>
					<input type="text" value="<?php echo ThemeHelper::esc_attr($this->data['option']['font_h1_family_google']); ?>" id="<?php ThemeHelper::getFormName('font_h1_family_google'); ?>" name="<?php ThemeHelper::getFormName('font_h1_family_google'); ?>" maxlength="255"/>
				</div>	
			</li>
			<li>
				<h5><?php esc_html_e('System font','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enter name of system font.','atrium'); ?></span>
				<div>
					<input type="text" name="<?php ThemeHelper::getFormName('font_h1_family_system'); ?>" id="<?php ThemeHelper::getFormName('font_h1_family_system'); ?>" value="<?php echo ThemeHelper::esc_attr($this->data['option']['font_h1_family_system']); ?>" maxlength="255"/>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Font size','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Font size (in px).','atrium'); ?></span>
				<div>
					<div id="<?php ThemeHelper::getFormName('font_h1_size_1_slider'); ?>"></div>
					<input type="text" name="<?php ThemeHelper::getFormName('font_h1_size_1'); ?>" id="<?php ThemeHelper::getFormName('font_h1_size_1'); ?>" class="to-slider-range" readonly/>
					<label class="to-label-1 to-clear-fix"><?php esc_html_e('For page width greater/equal than 960px.','atrium'); ?></label>
				</div>
				<div>
					<div id="<?php ThemeHelper::getFormName('font_h1_size_2_slider'); ?>"></div>
					<input type="text" name="<?php ThemeHelper::getFormName('font_h1_size_2'); ?>" id="<?php ThemeHelper::getFormName('font_h1_size_2'); ?>" class="to-slider-range" readonly/>
					<label class="to-label-1 to-clear-fix"><?php esc_html_e('For page width between 768px and 959px.','atrium'); ?></label>
				</div>
				<div>
					<div id="<?php ThemeHelper::getFormName('font_h1_size_3_slider'); ?>"></div>
					<input type="text" name="<?php ThemeHelper::getFormName('font_h1_size_3'); ?>" id="<?php ThemeHelper::getFormName('font_h1_size_3'); ?>" class="to-slider-range" readonly/>
					<label class="to-label-1 to-clear-fix"><?php esc_html_e('For page width between 480px and 767px.','atrium'); ?></label>
				</div>
				<div>
					<div id="<?php ThemeHelper::getFormName('font_h1_size_4_slider'); ?>"></div>
					<input type="text" name="<?php ThemeHelper::getFormName('font_h1_size_4'); ?>" id="<?php ThemeHelper::getFormName('font_h1_size_4'); ?>" class="to-slider-range" readonly/>
					<label class="to-label-1 to-clear-fix"><?php esc_html_e('For page width less/equal than 479px.','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Font style','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Font style.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('font_h1_style'); ?>" id="<?php ThemeHelper::getFormName('font_h1_style'); ?>">
<?php
						foreach($this->data['dictionary']['fontStyle'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['font_h1_style'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Font weight','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Font weight.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('font_h1_weight'); ?>" id="<?php ThemeHelper::getFormName('font_h1_weight'); ?>">
<?php
						foreach($this->data['dictionary']['fontWeight'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['font_h1_weight'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>
		</ul>

		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var element=$('.to').themeOptionElement();;
				element.createGoogleFontAutocomplete('#<?php ThemeHelper::getFormName('font_h1_family_google'); ?>');
				element.createSlider('#<?php ThemeHelper::getFormName('font_h1_size_1_slider'); ?>',1,100,<?php echo (int)$this->data['option']['font_h1_size_1']; ?>);
				element.createSlider('#<?php ThemeHelper::getFormName('font_h1_size_2_slider'); ?>',1,100,<?php echo (int)$this->data['option']['font_h1_size_2']; ?>);
				element.createSlider('#<?php ThemeHelper::getFormName('font_h1_size_3_slider'); ?>',1,100,<?php echo (int)$this->data['option']['font_h1_size_3']; ?>);
				element.createSlider('#<?php ThemeHelper::getFormName('font_h1_size_4_slider'); ?>',1,100,<?php echo (int)$this->data['option']['font_h1_size_4']; ?>);
			});
		</script>