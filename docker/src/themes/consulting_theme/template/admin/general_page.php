		<ul class="to-form-field-list">
			<li>
				<h5><?php esc_html_e('404 error page','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Get settings for 404 page from selected page.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('page_404_page_id'); ?>" id="<?php ThemeHelper::getFormName('page_404_page_id'); ?>">
<?php
						foreach($this->data['dictionary']['page'] as $value)
							echo '<option value="'.ThemeHelper::esc_attr($value->ID).'" '.(ThemeHelper::selectedIf($this->data['option']['page_404_page_id'],$value->ID,false)).'>'.ThemeHelper::esc_html($value->post_title).'</option>';
?>
					</select>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Sidebar','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Select sidebar and sidebar location.','atrium'); ?></span>
				<div class="to-clear-fix">
					<span class="to-legend-field"><?php esc_html_e('Sidebar:','atrium'); ?></span>
					<select name="<?php ThemeHelper::getFormName('page_content_widget_area'); ?>" id="<?php ThemeHelper::getFormName('page_content_widget_area'); ?>">
<?php
						foreach($this->data['dictionary']['widgetArea-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_content_widget_area'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
				<div class="to-clear-fix">
					<span class="to-legend-field"><?php esc_html_e('Location:','atrium'); ?></span>
					<select name="<?php ThemeHelper::getFormName('page_content_widget_area_location'); ?>" id="<?php ThemeHelper::getFormName('page_content_widget_area_location'); ?>">
<?php
						foreach($this->data['dictionary']['widgetAreaLocation-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_content_widget_area_location'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Widget area in footer','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Select widget area in footer.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('page_footer_widget_area'); ?>" id="<?php ThemeHelper::getFormName('page_footer_widget_area'); ?>">
<?php
						foreach($this->data['dictionary']['widgetArea-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_footer_widget_area'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>			
			<li>
				<h5><?php esc_html_e('Top menu','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Select top menu.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('page_menu_top'); ?>" id="<?php ThemeHelper::getFormName('page_menu_top'); ?>">
<?php
						foreach($this->data['dictionary']['menu-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['page_menu_top'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>				
			<li>
				<h5><?php esc_html_e('Show page header','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Show page header.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('page_header_visible'); ?>" id="<?php ThemeHelper::getFormName('page_header_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['page_header_visible'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('page_header_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('page_header_visible'); ?>" id="<?php ThemeHelper::getFormName('page_header_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['page_header_visible'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('page_header_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
				</div>
			</li>
		</ul>