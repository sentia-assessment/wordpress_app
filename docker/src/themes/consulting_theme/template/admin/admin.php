
		<div class="to to-to">

			<form name="to_form" id="to_form" method="POST" action="#">

				<div id="to_notice"></div> 

				<div class="to-header to-clear-fix">

					<div class="to-header-left">

						<div>
							<h3>QuanticaLabs</h3>
							<h6><?php esc_html_e('Theme Options','atrium'); ?></h6>
						</div>

					</div>

					<div class="to-header-right">

						<div>
							<h3>Atrium - Finance Consulting Advisor</h3>
							<h6>Wordpress Theme ver. 2.4</h6>&nbsp;&nbsp;
							<a href="http://support.quanticalabs.com">Support Forum</a>
							<a href="http://themeforest.net/user/QuanticaLabs?ref=QuanticaLabs">Theme site</a>
						</div>

						<a href="http://quanticalabs.com" class="to-header-right-logo"></a>

					</div>

				</div>

				<div class="to-content to-clear-fix">

					<div class="to-content-left">

						<ul class="to-menu" id="to_menu">

							<li>
								<a href="#general_setting"><?php esc_html_e('General settings','atrium'); ?><span></span></a>
								<ul>
									<li><a href="#general_blog"><?php esc_html_e('Blog','atrium'); ?></a></li>
									<li><a href="#general_post"><?php esc_html_e('Posts','atrium'); ?></a></li>
									<li><a href="#general_page"><?php esc_html_e('Pages','atrium'); ?></a></li>
									<li><a href="#general_menu"><?php esc_html_e('Menu','atrium'); ?></a></li>
									<li><a href="#general_footer"><?php esc_html_e('Footer','atrium'); ?></a></li>
									<li><a href="#general_favicon"><?php esc_html_e('Favicon','atrium'); ?></a></li>
									<li><a href="#general_custom_js_code"><?php esc_html_e('Custom JS code','atrium'); ?></a></li>
									<li><a href="#general_content_copying"><?php esc_html_e('Content copying','atrium'); ?></a></li>
									<li><a href="#general_go_top_top"><?php esc_html_e('Go to top of page','atrium'); ?></a></li>
									<li><a href="#general_responsive_mode"><?php esc_html_e('Responsive mode','atrium'); ?></a></li>
								</ul>				
							</li>
							<li>
								<a href="#font_setting" class="to-menu-font"><?php esc_html_e('Fonts settings','atrium'); ?><span></span></a>
								<ul>
									<li><a href="#font_base"><?php esc_html_e('Base font','atrium'); ?></a></li>
									<li><a href="#font_header_h1"><?php esc_html_e('Header H1','atrium'); ?></a></li>
									<li><a href="#font_header_h2"><?php esc_html_e('Header H2','atrium'); ?></a></li>
									<li><a href="#font_header_h3"><?php esc_html_e('Header H3','atrium'); ?></a></li>
									<li><a href="#font_header_h4"><?php esc_html_e('Header H4','atrium'); ?></a></li>
									<li><a href="#font_header_h5"><?php esc_html_e('Header H5','atrium'); ?></a></li>
									<li><a href="#font_header_h6"><?php esc_html_e('Header H6','atrium'); ?></a></li>
									<li><a href="#font_google_font_setting"><?php esc_html_e('Google Fonts settings','atrium'); ?></a></li>
								</ul>
							</li>
							<li>
								<a href="#plugin_setting" class="to-menu-plugin"><?php esc_html_e('Plugins settings','atrium'); ?><span></span></a>
								<ul>

									<li><a href="#plugin_fancybox_video"><?php esc_html_e('Fancybox for videos','atrium'); ?></a></li>
									<li><a href="#plugin_fancybox_image"><?php esc_html_e('Fancybox for images','atrium'); ?></a></li>
								</ul>
							</li>							
							<li>
								<a href="#custom_css" class="to-menu-css"><?php esc_html_e('Custom CSS','atrium'); ?><span></span></a>
								<ul>
									<li><a href="#custom_css_responsive_1"><?php esc_html_e('Default styles','atrium'); ?></a></li>
									<li><a href="#custom_css_responsive_2"><?php esc_html_e('For width 768px - 959px','atrium'); ?></a></li>
									<li><a href="#custom_css_responsive_3"><?php esc_html_e('For width 480px - 767px','atrium'); ?></a></li>
									<li><a href="#custom_css_responsive_4"><?php esc_html_e('For width less/equal than 479px','atrium'); ?></a></li>
								</ul>				
							</li>	
						</ul>

					</div>

					<div class="to-content-right" id="to_panel">
<?php
		$content=array
		(
			'general_blog',
			'general_post',
			'general_page',
			'general_menu',
			'general_footer',
			'general_go_top_top',
			'general_content_copying',
			'general_responsive_mode',
			'general_custom_js_code',
			'general_favicon',
			'font_base',
			'font_header_h1',
			'font_header_h2',
			'font_header_h3',
			'font_header_h4',
			'font_header_h5',
			'font_header_h6',
			'font_google_font_setting',
			'plugin_fancybox_image',
			'plugin_fancybox_video',
			'custom_css_responsive_1',
			'custom_css_responsive_2',
			'custom_css_responsive_3',
			'custom_css_responsive_4'
		);

		foreach($content as $value)
		{
?>
						<div id="<?php echo $value; ?>">
<?php
			$Template=new ThemeTemplate($this->data,THEME_PATH_TEMPLATE.'admin/'.$value.'.php');
			echo $Template->output(false);
?>
						</div>
<?php
		}
?>
					</div>

				</div>

				<div class="to-footer to-clear-fix">

					<div class="to-footer-left">

						<ul class="to-social-list">
							<li><a href="http://themeforest.net/user/QuanticaLabs?ref=quanticalabs" class="to-social-list-envato" title="Envato"></a></li>
							<li><a href="http://www.facebook.com/QuanticaLabs" class="to-social-list-facebook" title="Facebook"></a></li>
							<li><a href="https://twitter.com/quanticalabs" class="to-social-list-twitter" title="Twitter"></a></li>
							<li><a href="http://quanticalabs.tumblr.com/" class="to-social-list-tumblr" title="Tumblr"></a></li>
						</ul>

					</div>

					<div class="to-footer-right">
						<input type="submit" value="<?php esc_attr_e('Save changes','atrium'); ?>" name="Submit" id="Submit" class="to-button"/>
					</div>			

				</div>

				<input type="hidden" name="action" id="action" value="theme_admin_option_page_save" />

				<script type="text/javascript">

					jQuery(document).ready(function($)
					{
						$('.to').themeOption();
						$('.to').themeOptionElement({init:true});
					});

				</script>

			</form>
			
		</div>