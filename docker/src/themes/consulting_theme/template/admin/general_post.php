		<ul class="to-form-field-list">
			<li>
				<h5><?php esc_html_e('Sidebar','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Select sidebar and sidebar location.','atrium'); ?></span>
				<div class="to-clear-fix">
					<span class="to-legend-field"><?php esc_html_e('Sidebar:','atrium'); ?></span>
					<select name="<?php ThemeHelper::getFormName('post_content_widget_area'); ?>" id="<?php ThemeHelper::getFormName('post_content_widget_area'); ?>">
<?php
						foreach($this->data['dictionary']['widgetArea-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['post_content_widget_area'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
				<div class="to-clear-fix">
					<span class="to-legend-field"><?php esc_html_e('Location:','atrium'); ?></span>
					<select name="<?php ThemeHelper::getFormName('post_content_widget_area_location'); ?>" id="<?php ThemeHelper::getFormName('post_content_widget_area_location'); ?>">
<?php
						foreach($this->data['dictionary']['widgetAreaLocation-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['post_content_widget_area_location'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Widget area in footer','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Select widget area in footer.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('post_footer_widget_area'); ?>" id="<?php ThemeHelper::getFormName('post_footer_widget_area'); ?>">
<?php
						foreach($this->data['dictionary']['widgetArea-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['post_footer_widget_area'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>	
			<li>
				<h5><?php esc_html_e('Top menu','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Select top menu.','atrium'); ?></span>
				<div class="to-clear-fix">
					<select name="<?php ThemeHelper::getFormName('post_menu_top'); ?>" id="<?php ThemeHelper::getFormName('post_menu_top'); ?>">
<?php
						foreach($this->data['dictionary']['menu-1'] as $index=>$value)
							echo '<option value="'.ThemeHelper::esc_attr($index).'" '.(ThemeHelper::selectedIf($this->data['option']['post_menu_top'],$index,false)).'>'.ThemeHelper::esc_html($value[0]).'</option>';
?>
					</select>
				</div>
			</li>				
			<li>
				<h5><?php esc_html_e('Show post header','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Show post header.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('post_header_visible'); ?>" id="<?php ThemeHelper::getFormName('post_header_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['post_header_visible'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_header_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('post_header_visible'); ?>" id="<?php ThemeHelper::getFormName('post_header_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['post_header_visible'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_header_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Show post categories','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Show post categories.','atrium'); ?><br/></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('post_category_visible'); ?>" id="<?php ThemeHelper::getFormName('post_category_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['post_category_visible'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_category_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('post_category_visible'); ?>" id="<?php ThemeHelper::getFormName('post_category_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['post_category_visible'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_category_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Show post author','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Show post author.','atrium'); ?><br/></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('post_author_visible'); ?>" id="<?php ThemeHelper::getFormName('post_author_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['post_author_visible'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_author_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('post_author_visible'); ?>" id="<?php ThemeHelper::getFormName('post_author_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['post_author_visible'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_author_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Show post tags','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Show post tags.','atrium'); ?><br/></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('post_tag_visible'); ?>" id="<?php ThemeHelper::getFormName('post_tag_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['post_tag_visible'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_tag_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('post_tag_visible'); ?>" id="<?php ThemeHelper::getFormName('post_tag_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['post_tag_visible'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_tag_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Show post comment count','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Show post comment count.','atrium'); ?><br/></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('post_comment_count_visible'); ?>" id="<?php ThemeHelper::getFormName('post_comment_count_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['post_comment_count_visible'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_comment_count_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('post_comment_count_visible'); ?>" id="<?php ThemeHelper::getFormName('post_comment_count_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['post_comment_count_visible'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_comment_count_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Show post navigation','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Show previous/next post navigation.','atrium'); ?><br/></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('post_prev_next_post_visible'); ?>" id="<?php ThemeHelper::getFormName('post_prev_next_post_visible_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['post_prev_next_post_visible'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_prev_next_post_visible_1'); ?>"><?php esc_html_e('Yes','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('post_prev_next_post_visible'); ?>" id="<?php ThemeHelper::getFormName('post_prev_next_post_visible_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['post_prev_next_post_visible'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('post_prev_next_post_visible_0'); ?>"><?php esc_html_e('No','atrium'); ?></label>
				</div>
			</li>			
		</ul>