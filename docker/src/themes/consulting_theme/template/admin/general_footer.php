		<ul class="to-form-field-list">
			<li>
				<h5><?php esc_html_e('Enable footer','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enable or disable footer.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('footer_enable'); ?>" id="<?php ThemeHelper::getFormName('footer_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['footer_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('footer_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('footer_enable'); ?>" id="<?php ThemeHelper::getFormName('footer_enable_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['footer_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('footer_enable_0'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Enable top footer','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enable or disable top footer (widget area).','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('footer_top_enable'); ?>" id="<?php ThemeHelper::getFormName('footer_top_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['footer_top_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('footer_top_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('footer_top_enable'); ?>" id="<?php ThemeHelper::getFormName('footer_top_enable_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['footer_top_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('footer_top_enable_0'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Enable bottom footer','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Enable or disable bottom part of footer.','atrium'); ?></span>
				<div class="to-radio-button">
					<input type="radio" name="<?php ThemeHelper::getFormName('footer_bottom_enable'); ?>" id="<?php ThemeHelper::getFormName('footer_bottom_enable_1'); ?>" value="1" <?php ThemeHelper::checkedIf($this->data['option']['footer_bottom_enable'],1); ?>/>
					<label for="<?php ThemeHelper::getFormName('footer_bottom_enable_1'); ?>"><?php esc_html_e('Enable','atrium'); ?></label>
					<input type="radio" name="<?php ThemeHelper::getFormName('footer_bottom_enable'); ?>" id="<?php ThemeHelper::getFormName('footer_bottom_enable_0'); ?>" value="0" <?php ThemeHelper::checkedIf($this->data['option']['footer_bottom_enable'],0); ?>/>
					<label for="<?php ThemeHelper::getFormName('footer_bottom_enable_0'); ?>"><?php esc_html_e('Disable','atrium'); ?></label>
				</div>
			</li>
			<li>
				<h5><?php esc_html_e('Bottom footer content','atrium'); ?></h5>
				<span class="to-legend"><?php esc_html_e('Bottom footer content.','atrium'); ?></span>
				<div>
					<?php wp_editor($this->data['option']['footer_bottom_content'],ThemeHelper::getFormName('footer_bottom_content',false)); ?>
				</div>
			</li>
		</ul>