<?php
		$Post=new ThemePost();
		
		$query=$Post->getPostMostComment($this->data['instance']);
		
		if($query)
		{
			$id='widget_theme_widget_post_most_comment_'.ThemeHelper::createId();
			
			echo $this->data['html']['start']; 
?>
			<div class="widget-theme-widget-post-most-comment-date-box-enable-<?php echo (int)$this->data['instance']['date_box_enable']; ?>"  id="<?php echo esc_attr($id); ?>">
				
				<ul class="theme-reset-list">
<?php
			global $post;
			$bPost=$post;

			while($query->have_posts())
			{
				$query->the_post();

				$Post->formatPostDate($post->post_date,$day,$month,$year);
?>
					<li class="theme-clear-fix">
<?php
				if($this->data['instance']['date_box_enable']==1)
				{
?>
						<div class="theme-widget-post-date">
							<span><?php echo $day; ?></span>
							<span><?php echo esc_html($month); ?></span>
						</div>
<?php
				}
?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						
						<div class="theme-widget-post-meta theme-clear-fix">
							<div class="theme-widget-post-meta-date"><?php echo get_the_date(); ?></div>
							<div class="theme-widget-post-meta-comment-count"><?php comments_number('No replies','1 reply','% replies'); ?></div>
						</div>
						
					</li>			
<?php
			}
		
			$post=$bPost;
?>
				</ul>
				
			</div>
<?php
			if($this->data['instance']['carousel']==1)
			{
?>
			<script type="text/javascript">
				
				jQuery(document).ready(function($) 
				{
					$('#<?php echo $id; ?>').carousel({count:<?php echo $this->data['instance']['carousel_post_count']; ?>});
				});
				
			</script>
<?php
			}

			echo $this->data['html']['stop']; 
		}