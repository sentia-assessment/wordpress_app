<?php

/******************************************************************************/
/******************************************************************************/

require_once('include.php');

add_filter('widget_text','do_shortcode');

if(is_admin())
{
	require_once('admin/functions.php');
}
else require_once('public/functions.php');

$WidgetArea=new ThemeWidgetArea();
$WidgetArea->register();

/******************************************************************************/

if(!isset($content_width)) $content_width=960;

if(function_exists('register_nav_menu')) register_nav_menu('menu_top','Menu Top');

/******************************************************************************/

add_theme_support('post-thumbnails'); 
add_theme_support('automatic-feed-links');

add_theme_support('title-tag');

add_theme_support('custom-header');
add_theme_support('custom-background');

add_theme_support('wp-block-styles');

add_editor_style('editor-style.css');

$Theme=new Theme();
$Post=new ThemePost();
$Page=new ThemePage();
$Image=new ThemeImage();
$Comment=new ThemeComment();

$Image->register();

add_filter('image_size_names_choose',array($Image,'addImageSupport'));

add_filter('excerpt_more',array($Theme,'filterExcerptMore'));
add_filter('excerpt_length',array($Theme,'automaticExcerptLength'),999);

add_filter('wp_title',array($Post,'wpTitleFilter'));

add_filter('body_class',array($Post,'filterBodyClass'));

/******************************************************************************/

add_action('save_post',array($Page,'adminSaveMetaBox'));
add_action('add_meta_boxes',array($Page,'adminInitMetaBox'));

add_action('init',array($WidgetArea,'adminInit')); 
add_action('save_post',array($WidgetArea,'adminSaveMetaBox'));
add_action('add_meta_boxes',array($WidgetArea,'adminInitMetaBox'));
add_filter('manage_edit-'.THEME_CONTEXT.'_widget_area_columns',array($WidgetArea,'adminManageEditColumn')); 
add_action('manage_'.THEME_CONTEXT.'_widget_area_posts_custom_column',array($WidgetArea,'adminManageColumn'));
add_filter('manage_edit-'.THEME_CONTEXT.'_widget_area_sortable_columns',array($WidgetArea,'adminManageEditSortableColumn'));

add_action('save_post',array($Post,'adminSaveMetaBox'));
add_action('add_meta_boxes',array($Post,'adminInitMetaBox'));

add_action('wp_ajax_comment_add',array($Comment,'addComment'));
add_action('wp_ajax_nopriv_comment_add',array($Comment,'addComment'));
add_action('wp_ajax_comment_get',array($Comment,'getComment'));
add_action('wp_ajax_nopriv_comment_get',array($Comment,'getComment'));

$WidgetPostMostComment=new ThemeWidgetPostMostComment();
$WidgetPostMostComment->register();

$WidgetPostRecent=new ThemeWidgetPostRecent();
$WidgetPostRecent->register();

add_action('tgmpa_register',array($Theme,'addPlugin'));

/******************************************************************************/

load_theme_textdomain(THEME_DOMAIN,THEME_PATH.'languages/');

/******************************************************************************/
/******************************************************************************/