<?php

/******************************************************************************/
/******************************************************************************/

class ThemeImage
{
	/**************************************************************************/
	
	function __construct()
	{
		$this->image=array
		(
			'image-1920-1200'	=> array(1920,1200,__('Image 1920x1200','atrium'),1,false),
			'image-960-504'		=> array(960,504,__('Image 960x504','atrium'),1,true),
			'image-630-504'		=> array(630,504,__('Image 630x504','atrium'),1,false),
			'image-480-384'		=> array(480,384,__('Image 480x384','atrium'),2,false)
		);
	}
	
	/**************************************************************************/
	
	function register()
	{
		foreach($this->image as $index=>$data)
			add_image_size($index,$data[0],$data[1],$data[2],$data[4]);
	}
	
	/**************************************************************************/
	
	function addImageSupport($size)
	{
		$addsize=array();
		foreach($this->image as $index=>$data)
			$addsize[$index]=$data[2];
		
		return(array_merge($size,$addsize));
	}
	
	/**************************************************************************/
	
	function getImageDimension($id)
	{
		return(array($this->image[$id][0],$this->image[$id][1]));
	}
	
	/**************************************************************************/
	
	function itemExist($id)
	{
		return((bool)$this->image[$id]);
	}
	
	/**************************************************************************/
	
	function getImageNameByColumnCount($columnCount)
	{
		foreach($this->image as $index=>$value)
		{
			if($value[3]==$columnCount) return($index);
		}
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/