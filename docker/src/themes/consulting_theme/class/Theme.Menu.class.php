<?php

/******************************************************************************/
/******************************************************************************/

class ThemeMenu
{
	/**************************************************************************/

	function __construct()
	{
		
	}
	
	/**************************************************************************/
	
	function getMenuDictionary($useNone=true,$useGlobal=true,$useLeftUnchaged=false)
	{
		$menu=get_terms('nav_menu');
		
		$data=array();
		
		if($useNone) $data[0]=array(__('[None]','atrium'));
		if($useGlobal) $data[-1]=array(__('[Use global settings]','atrium'));
		if($useLeftUnchaged) $data[-10]=array(__('[Left unchaged]','atrium'));

		foreach($menu as $singlePost)
			$data[$singlePost->term_id]=array($singlePost->name);
		
		return($data);
	}
	
	/**************************************************************************/
	
	function create()
	{
		global $aropwt_parentPost;
		
		$Validation=new ThemeValidation();
		
		$attribute=array();
		$attribute['menu_id']=0;
		
		$menu=wp_get_nav_menus();
		$menuLocation=get_nav_menu_locations();
		
		$locationId='menu_top';
		
		if(isset($menuLocation[$locationId])) 
		{
			foreach($menu as $m)
			{
				if($m->term_id==$menuLocation[$locationId])
					$attribute['menu_id']=$m->term_id;
			}
		}
		
		if($attribute['menu_id']==0)
			if(ThemeOption::getGlobalOption($aropwt_parentPost->post,'menu_top')==0) return;
		
		if($attribute['menu_id']==0)
			$attribute['menu_id']=ThemeOption::getGlobalOption($aropwt_parentPost->post,'menu_top');
		
		$attribute['logo_src']=ThemeOption::getOption('menu_logo_src');
		
		$attribute['sticky_enable']=ThemeOption::getOption('menu_sticky_enable');
		$attribute['responsive_level']=ThemeOption::getOption('menu_responsive_level');
		$attribute['hide_scroll_enable']=ThemeOption::getOption('menu_hide_scroll_enable');
		$attribute['hover_next_level_enable']=ThemeOption::getOption('menu_hover_next_level_enable');
		$attribute['hover_first_level_enable']=ThemeOption::getOption('menu_hover_first_level_enable');
		
		$attribute['animation_enable']=ThemeOption::getOption('menu_animation_enable');
		$attribute['animation_easing']=ThemeOption::getOption('menu_animation_easing');
		$attribute['animation_duration']=ThemeOption::getOption('menu_animation_duration');
		
		$attribute['css_class']='pb';
		
		if(!class_exists('PBComponentMenu'))
		{
			$html=null;
			$logoHTML=null;

			$class=array();
			$option=array();

			$key=array
			(
				'sticky_enable',
				'responsive_level',
				'hide_scroll_enable',
				'animation_enable',
				'animation_easing',
				'animation_duration'
			);

			foreach($key as $value)
				$option[$value]=$attribute[$value];
		
			$id=ThemeHelper::createId('pb_menu');
		
			$classLogo=array('pb-logo','theme-layout-column-left');
			$classMenu=array('pb-menu','theme-layout-column-right');
			$classMenuResponsive=array('pb-menu-responsive','pb-layout-column-right');
		
			$class=array('pb-menu-logo',$attribute['css_class']);
		
			if($attribute['hover_first_level_enable']==1)
				array_push($classMenu,'pb-menu-hover-first-level');
			if($attribute['hover_next_level_enable']==1)
				array_push($classMenu,'pb-menu-hover-next-level');
		
			$menuAttribute=array
			(
				'menu'				=>	$attribute['menu_id'],
				'walker'			=>	new ThemeMenuWalker(),
				'menu_class'		=>	'sf-menu theme-reset-list theme-clear-fix',
				'container'			=>	'',
				'container_class'	=>	'',
				'echo'				=>	0
			);
		
			$menuResponsiveAttribute=array
			(
				'menu'				=>	$attribute['menu_id'],
				'walker'			=>	new ThemeMenuResponsiveWalker(),
				'menu_class'		=>	'pb-clear-fix',
				'container'			=>	'',
				'container_class'	=>	'',
				'echo'				=>	0,
				'items_wrap'		=>	'<select id="%1$s" class="%2$s">%3$s</select>'
			);
		
			if($Validation->isURL($attribute['logo_src']))
			{
				$logoHTML=
				'
					<a href="'.get_home_url().'">
						<img src="'.esc_attr($attribute['logo_src']).'" alt=""/>
					</a>
				';
			}
		
			$html=
			'
				<div'.ThemeHelper::createClassAttribute($class).' id="'.$id.'">
					<div class="theme-main theme-clear-fix theme-layout-25x75">
						<div'.ThemeHelper::createClassAttribute($classLogo).'>
							'.$logoHTML.'
						</div>
						<div'.ThemeHelper::createClassAttribute($classMenu).'>
							<div class="pb-menu-selected"></div>
							'.wp_nav_menu($menuAttribute).'
						</div>
						<div'.ThemeHelper::createClassAttribute($classMenuResponsive).'>
							'.wp_nav_menu($menuResponsiveAttribute).'
						</div>					
					</div>
				</div>
				<div class="theme-script-tag">
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$(\'#'.$id.'\').menu('.json_encode($option).');
						});
					</script>
				</div>
			';
			
			return(ThemeHelper::formatCode($html));		
		}
		else
		{
			$attributeString=null;
			foreach($attribute as $index=>$value)
				$attributeString.=' '.$index.'="'.$value.'"';

			$shortcode='['.PLUGIN_PAGE_BUILDER_SHORTCODE_PREFIX.'menu'.$attributeString.']';

			echo do_shortcode($shortcode);
		}
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/