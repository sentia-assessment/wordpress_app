<?php

/******************************************************************************/
/******************************************************************************/

class ThemeBlog
{
	/**************************************************************************/
	
	function __construct()
	{
		$this->sortPostBlogField=array
		(
			'post_id'		=>	array(__('Post ID','atrium')),
			'post_date'		=>	array(__('Post date','atrium')),
			'title'			=>	array(__('Post title','atrium'))
		);

		$this->sortDirection=array
		(
			'asc'			=>	array(__('Ascending','atrium')),
			'desc'			=>	array(__('Descending','atrium'))
		);		
	}
	
	/**************************************************************************/
	
	function getPost()
	{
		global $aropwt_parentPost;
		
		$Validation=new ThemeValidation();
		
		$argument=array();
		
		$s=get_query_var('s');
		$tag=get_query_var('tag');
		$year=(int)get_query_var('year');
		$month=(int)get_query_var('monthnum');
		$categoryId=(int)get_query_var('cat');
		
		if($Validation->isNotEmpty($s))
			$argument['s']=$s;
		if($Validation->isNotEmpty($tag))
			$argument['tag']=$tag;
		if($categoryId>0)
			$argument['cat']=(int)$categoryId;
		elseif(($year>0) && ($month>0))
		{
			$argument['year']=$year;
			$argument['monthnum']=$month;
		}
		
		if(!($categoryId>0))
		{
			$option=ThemeOption::getPostMeta($aropwt_parentPost->post);
			ThemeHelper::removeUIndex($option,'page_post_category');

			if(is_array($option['page_post_category']))
			{
				if(count($option['page_post_category']))
					$argument['cat']=implode(',',array_map('intval',$option['page_post_category']));
			}
		}

		$default=array
		(
			'post_type'			=>	'post',
			'post_status'		=>	'publish',
			'posts_per_page'	=>	(int)get_option('posts_per_page'),
			'paged'				=>	(int)ThemeHelper::getPageNumber(),
			'orderby'			=>	ThemeOption::getOption('blog_sort_field'),
			'order'				=>	ThemeOption::getOption('blog_sort_direction')		
		);
		
		$query=new WP_Query(array_merge($argument,$default));
		return($query);
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/