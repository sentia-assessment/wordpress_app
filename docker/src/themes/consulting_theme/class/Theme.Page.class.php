<?php

/******************************************************************************/
/******************************************************************************/

class ThemePage
{
	/**************************************************************************/

	function __construct()
	{

	}
	
	/**************************************************************************/
	
	function adminInitMetaBox()
	{
		add_meta_box('page',__('Options','atrium'),array($this,'adminCreateMetaBox'),'page','normal','high');	
	}
	
	/**************************************************************************/
	
	function adminCreateMetaBox()
	{
		global $post;
		
		$Menu=new ThemeMenu();
		$WidgetArea=new ThemeWidgetArea();
		
		$data=array();
	
		$data['option']=ThemeOption::getPostMeta($post);
		$data['nonce']=wp_nonce_field('adminSaveMetaBox',THEME_CONTEXT.'_page_noncename',false,false);
		
		$data['dictionary']['widgetArea']=$WidgetArea->getWidgetAreaDictionary();
		$data['dictionary']['widgetAreaLocation']=$WidgetArea->getWidgetAreaLocationDictionary(false);
		
		$data['dictionary']['menu']=$Menu->getMenuDictionary(true,true,false);
		
		$data['dictionary']['postCategory']=ThemeHelper::createTermDictionary('category',array(),array(),array(),'id');
		
		ThemeHelper::setDeafultOption($data['option'],'page_menu_top',-1);
		ThemeHelper::setDeafultOption($data['option'],'page_header_visible',-1);
		
		ThemeHelper::setDeafultOption($data['option'],'page_content_widget_area',-1);
		ThemeHelper::setDeafultOption($data['option'],'page_content_widget_area_location',-1);
		ThemeHelper::setDeafultOption($data['option'],'page_footer_widget_area',-1);
		
		ThemeHelper::setDeafultOption($data['option'],'page_post_category',array());
		
		$Template=new ThemeTemplate($data,THEME_PATH_TEMPLATE.'admin/page_meta_box.php');
		echo $Template->output();			
	}
	
	/**************************************************************************/
	
	function adminSaveMetaBox($postId)
	{
		if($_POST)
		{
			if(ThemeHelper::checkSavePost($postId,THEME_CONTEXT.'_page_noncename','adminSaveMetaBox')===false) return(false);

			$option=ThemeHelper::getPostOption('page');
			update_post_meta($postId,THEME_OPTION_PREFIX,$option);
		}		
	}
	
	/**************************************************************************/

	function getImageClass($sidebar)
	{
		if($sidebar==0) return('image-960-504');
		else return('image-630-504');
	}
		
	/**************************************************************************/
	
	function displayHeader()
	{
		global $aropwt_parentPost;
		
		if(!class_exists('PBComponentHeader')) return;
		if(ThemeOption::getGlobalOption($aropwt_parentPost->post,'header_visible')!=1) return;
		
		$argument=array();
		
		$argument['important']=1;
		$argument['underline_enable']=1;
		$argument['css_class']='theme-page-header theme-main';
		
		$argumentString=null;
		foreach($argument as $index=>$value)
			$argumentString.=' '.$index.'="'.$value.'"';
		
		$shortcode='['.PLUGIN_PAGE_BUILDER_SHORTCODE_PREFIX.'header'.$argumentString.']'.$aropwt_parentPost->post->post_title.'[/'.PLUGIN_PAGE_BUILDER_SHORTCODE_PREFIX.'header]';
		
		echo do_shortcode($shortcode);
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/