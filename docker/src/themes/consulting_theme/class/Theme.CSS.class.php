<?php

/******************************************************************************/
/******************************************************************************/

class ThemeCSS
{
	/**************************************************************************/

	function __construct()
	{
		$this->fontWeight=array
		(
			'100'				=>	array(__('100','atrium')),
			'200'				=>	array(__('200','atrium')),
			'300'				=>	array(__('300','atrium')),
			'400'				=>	array(__('400','atrium')),
			'500'				=>	array(__('500','atrium')),
			'600'				=>	array(__('600','atrium')),
			'700'				=>	array(__('700','atrium')),
			'800'				=>	array(__('800','atrium')),
			'900'				=>	array(__('900','atrium')),	
			'bold'				=>	array(__('bold','atrium')),
			'bolder'			=>	array(__('bolder','atrium')),
			'inherit'			=>	array(__('inherit','atrium')),
			'lighter'			=>	array(__('lighter','atrium')),
			'normal'			=>	array(__('normal','atrium'))
		);

		$this->fontStyle=array
		(
			'inherit'			=>	array(__('inherit','atrium')),
			'italic'			=>	array(__('italic','atrium')),
			'normal'			=>	array(__('normal','atrium')),
			'oblique'			=>	array(__('oblique','atrium'))
		);	
	}
	
	/**************************************************************************/
	
	function create($option,$echo=false)
	{
		$option['property']=$this->validateProperty($option['property']);
		
		if(!count($option['property'])) return;
		
		ThemeHelper::removeUIndex($option,array('media',array()),array('selector',array()),'property',THEME_CONTEXT);
		
		$output=null;
		
		$output.=$this->createMedia($option['media']);
		$output.=$this->createSelector($option['selector']);
		$output.=$this->createProperty($option['property']);
		$output.=$this->createMedia($option['media'],false);
		
		if($echo) echo $output;
		else return($output);
	}
	
	/**************************************************************************/
	
	function validateProperty($property)
	{
		$data=array();
		$Validation=new	ThemeValidation();
		
		foreach($property as $name=>$value)
		{
			$unit='px';
			
			if(is_array($value))
			{
				if(isset($value['unit'])) $unit=$value['unit'];
				if(isset($value['value'])) $value=$value['value'];
			}

			switch($name)
			{
				case 'left':
				case 'width':
				case 'height':
				case 'font-size':
				case 'padding-top':
				case 'padding-left':
				case 'padding-right':
				case 'padding-bottom':
				case 'border-width':

					if($Validation->isNumber($value,0,9999)) $data[$name]=$value.$unit;
				
				break;
				
				case 'top':
				case 'margin-top':
				case 'margin-left':
				case 'margin-right':
				case 'margin-bottom':
					
					if($Validation->isNumber($value,-9999,9999)) $data[$name]=$value.$unit;
					
				break;
				
				case 'color':
				case 'border-color':
				case 'border-top-color':
				case 'border-right-color':
				case 'border-bottom-color':
				case 'border-left-color':	
				case 'outline-color':
				case 'background-color':
					
					if(in_array($value,array('transparent'))) $data[$name]='transparent';
					if($Validation->isHexColor($value)) $data[$name]='#'.$value;
					
				break;

				case 'font-family':
					
					foreach((array)$value as $family)
					{
						if($Validation->isNotEmpty($family)) 
						{
							ThemeHelper::removeUIndex($data,$name);
							
							if($Validation->isNotEmpty($data[$name])) $data[$name].=',';
							$data[$name].='\''.$family.'\'';
						}
					}
					
				break;
				
				case 'font-style':
					
					if(isset($this->fontStyle[$value])) $data[$name]=$value;
	
				break;
				
				case 'font-weight':
					
					if(isset($this->fontWeight[$value])) $data[$name]=$value;
	
				break;
				
				case 'display':
				case 'border-style':
				case 'background':
				case 'background-repeat':
				case 'background-position':
					
					if($Validation->isNotEmpty($value)) $data[$name]=$value;
					
				break;
				
				case 'background-image':
					
					if($Validation->isNotEmpty($value)) $data[$name]='url(\''.$value.'\',THEME_CONTEXT)';
					
				break;
				
			}
		}
		
		return($data);
	}
	
	/**************************************************************************/
	
	function createSelector($selector)
	{
		$output=null;
		
		foreach($selector as $name)
		{
			if(!is_null($output)) $output.=",\n";
			$output.=$name;
		}
		
		return("\n".$output."\n{\n");
	}
	
	/**************************************************************************/
	
	function createProperty($property)
	{
		$output=null;
		
		foreach($property as $name=>$value)
		{
			if(!is_null($output)) $output.="\n";
			$output.="\t".$name.':'.$value.';';
		}
		
		return($output."\n}\n");		
	}
	
	/**************************************************************************/
	
	function createMedia($media,$start=true)
	{
		if(!count($media)) return;
		
		if($start)
		{
			$output=null;
			foreach($media as $index=>$value)
				$output.=' and ('.$index.':'.$value.'px)';
	
			$output="@media only screen ".$output."\n{\n";
		}
		else $output="}\n\n";
		
		return($output);
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/