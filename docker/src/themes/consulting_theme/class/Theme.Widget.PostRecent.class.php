<?php

/******************************************************************************/
/******************************************************************************/

class ThemeWidgetPostRecent extends ThemeWidget 
{
	/**************************************************************************/
	
    function __construct() 
	{
		parent::__construct(THEME_CONTEXT.'_widget_post_recent',sprintf(__('%s theme: Recent posts','atrium'),THEME_NAME),array('description'=>__('Displays latest posts.','atrium')),array());
    }
	
	/**************************************************************************/
	
    function widget($argument,$instance) 
	{
		$argument['_data']['file']='widget_post_recent.php';
		parent::widget($argument,$instance);
    }
	
	/**************************************************************************/
	
    function update($new_instance,$old_instance)
	{
		$instance=array();
		$instance['title']=$new_instance['title'];
		$instance['carousel']=(int)$new_instance['carousel'];
		$instance['carousel_post_count']=(int)$new_instance['carousel_post_count'];
		$instance['post_count']=(int)$new_instance['post_count'];
		$instance['date_box_enable']=$new_instance['date_box_enable'];
		return($instance);
    }
	
	/**************************************************************************/
	
	function form($instance) 
	{	
		$instance['_data']['file']='widget_post_recent.php';
		$instance['_data']['field']=array('title','post_count','date_box_enable','carousel','carousel_post_count');
		parent::form($instance);
	}
	
	/**************************************************************************/
	
	function register($class=null)
	{
		parent::register(is_null($class) ? get_class() : $class);
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/