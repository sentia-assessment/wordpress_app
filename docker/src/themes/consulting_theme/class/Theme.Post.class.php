<?php

/******************************************************************************/
/******************************************************************************/

class ThemePost
{
	/**************************************************************************/
	
	function __construct()
	{
		$this->theme=new Theme();
	}
	
	/**************************************************************************/
	
	function formatPostDate($date,&$day,&$month,&$year)
	{
		$day=null;
		$month=null;
		$year=null;
		
		list($day,$month,$year)=explode(' ',date_i18n('d M y',strtotime($date)));
		
		$year="'".$year;
	}
	
	/**************************************************************************/
	
	function adminInitMetaBox()
	{
		add_meta_box('post',__('Options','atrium'),array($this,'adminCreateMetaBox'),'post','normal','high');	
	}
	
	/**************************************************************************/
	
	function adminCreateMetaBox() 
	{
		global $post;
		
		$data=array();
		
		$Menu=new ThemeMenu();
		$WidgetArea=new ThemeWidgetArea();

		$data['option']=ThemeOption::getPostMeta($post);
		$data['nonce']=wp_nonce_field('adminSaveMetaBox',THEME_CONTEXT.'_post_noncename',false,false);

		$data['dictionary']['widgetArea']=$WidgetArea->getWidgetAreaDictionary();
		$data['dictionary']['widgetAreaLocation']=$WidgetArea->getWidgetAreaLocationDictionary(false);
		
		$data['dictionary']['menu']=$Menu->getMenuDictionary(true,true,false);
		
		ThemeHelper::setDeafultOption($data['option'],'post_content_widget_area',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_content_widget_area_location',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_footer_widget_area',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_menu_top',-1);
		
		ThemeHelper::setDeafultOption($data['option'],'post_header_visible',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_tag_visible',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_author_visible',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_category_visible',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_comment_count_visible',-1);
		ThemeHelper::setDeafultOption($data['option'],'post_prev_next_post_visible',-1);
		
		$Template=new ThemeTemplate($data,THEME_PATH_TEMPLATE.'admin/post_meta_box.php');
		echo $Template->output();		
	}
	
	/**************************************************************************/
	
	function adminSaveMetaBox($postId) 
	{
		if($_POST)
		{
			if(ThemeHelper::checkSavePost($postId,THEME_CONTEXT.'_post_noncename','adminSaveMetaBox')===false) return(false);
			
			$option=ThemeHelper::getPostOption('post');
			update_post_meta($postId,THEME_OPTION_PREFIX,$option);
		}
	}
	
	/**************************************************************************/
	
	function getPost()
	{
		$data=new stdClass();

		$categoryId=(int)get_query_var('cat');

		if(is_tag()) 
		{
			$data->post=get_post(ThemeOption::getOption('blog_search_post_id'));
			$data->post->post_title=sprintf(__('Search result for phrase %s','atrium'),esc_html(get_query_var('tag')));
		}
		elseif(is_category($categoryId)) 
		{			
			$category=get_category($categoryId);
			$data->post=get_post(ThemeOption::getOption('blog_category_post_id'));	
			$data->post->post_title=ThemeHelper::esc_html($category->name);	
		}
		elseif(is_day()) 
		{
			$data->post=get_post(ThemeOption::getOption('blog_archive_post_id'));
			$data->post->post_title=get_the_date();
		}
		elseif(is_archive()) 
		{
			$data->post=get_post(ThemeOption::getOption('blog_archive_post_id'));
			$data->post->post_title=single_month_title(' ',false);
		}
		elseif(is_search())
		{
			$data->post=get_post(ThemeOption::getOption('blog_search_post_id'));
			$data->post->post_title=sprintf(__('Search result for phrase %s','atrium'),esc_html(get_query_var('s')));
		}
		elseif(is_404())
		{
			$data->post=get_post(ThemeOption::getOption('page_404_page_id'));
			$data->post->post_title=$data->post->post_title;
		}
		else return(false);

		return($data);
	}
	
	/**************************************************************************/
	
	function getPostMostComment($argument)
	{
		$parameter=array
		(
			'post_type'							=>	'post',
			'posts_per_page'					=>	(int)$argument['post_count'],
			'orderby'							=>	'comment_count',
			'order'								=>	'desc'
		);
		
		$query=new WP_Query($parameter);
		return($query);
	}
	
	/**************************************************************************/
	
	function getPostRecent($argument)
	{
		$parameter=array
		(
			'post_type'							=>	'post',
			'posts_per_page'					=>	(int)$argument['post_count'],
			'orderby'							=>	'date',
			'order'								=>	'desc'
		);

		$query=new WP_Query($parameter);
		return($query);
	}
	
	/**************************************************************************/
	
	function wpTitleFilter($title)
	{
		$Validation=new ThemeValidation();
		
		if(($data=$this->getPost())!==false) 
		{
			if(!$Validation->isEmpty($title)) $title=' | ';
			$title.=$data->post->post_title;
		}
		return($title);
	}
    
    /**************************************************************************/
    
    function filterBodyClass($class)
    {
        if((function_exists('has_blocks')) && (has_blocks()))
            array_push($class,'page-gutenberg-block');
         
        return($class);
    }
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/