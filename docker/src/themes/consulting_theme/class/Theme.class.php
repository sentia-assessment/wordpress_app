<?php

/******************************************************************************/
/******************************************************************************/

class Theme
{
	/**************************************************************************/
	
	var $themeDefaultOption=array
	(
		'blog_category_post_id'													=>	'0',
		'blog_archive_post_id'													=>	'0',
		'blog_search_post_id'													=>	'0',
		'blog_sort_field'														=>	'post_date',
		'blog_sort_direction'													=>	'desc',		
		'blog_automatic_excerpt_length'											=>	'55',
		'post_content_widget_area'												=>	'',
		'post_content_widget_area_location'										=>	'2',
		'post_footer_widget_area'												=>	'',
		'post_menu_top'															=>	'',
		'post_header_visible'													=>	'1',
		'post_category_visible'													=>	'1',
		'post_author_visible'													=>	'1',
		'post_tag_visible'														=>	'1',
		'post_comment_count_visible'											=>	'1',
		'post_prev_next_post_visible'											=>	'1',
		'page_404_page_id'														=>	'0',
		'page_content_widget_area'												=>	'',
		'page_content_widget_area_location'										=>	'2',
		'page_footer_widget_area'												=>	'',
		'page_menu_top'															=>	'',
		'page_header_visible'													=>	'1',
		'menu_logo_src'															=>	'',
		'menu_responsive_level'													=>	'960',
		'menu_sticky_enable'													=>	'1',
		'menu_hide_scroll_enable'												=>	'0',
		'menu_hover_first_level_enable'											=>	'1',
		'menu_hover_next_level_enable'											=>	'1',
		'menu_animation_enable'													=>	'1',
		'menu_animation_duration'												=>	'1000',
		'menu_animation_easing'													=>	'easeOutQuint',
		'footer_enable'															=>	'1',
		'footer_top_enable'														=>	'1',
		'footer_bottom_enable'													=>	'1',
		'footer_bottom_content'													=>	'',
		'favicon_url'															=>	'',
		'right_click_enable'													=>	'1',
		'copy_selection_enable'													=>	'1',
		'responsive_mode_enable'												=>	'1',
		'custom_js_code'														=>	'',
		'go_to_page_top_enable'													=>	'1',
		'go_to_page_top_hash'													=>	'up',
		'go_to_page_top_animation_enable'										=>	'1',
		'go_to_page_top_animation_duration'										=>	'500',
		'go_to_page_top_animation_easing'										=>	'easeInCubic',
		'font_base_family_google'												=>	'Source Sans Pro',
		'font_base_family_system'												=>	'',
		'font_base_size_1'														=>	'16',
		'font_base_size_2'														=>	'16',
		'font_base_size_3'														=>	'16',
		'font_base_size_4'														=>	'16',
		'font_base_style'														=>	'normal',
		'font_base_weight'														=>	'normal',
		'font_h1_family_google'													=>	'Source Sans Pro',
		'font_h1_family_system'													=>	'',
		'font_h1_size_1'														=>	'72',
		'font_h1_size_2'														=>	'54',
		'font_h1_size_3'														=>	'54',
		'font_h1_size_4'														=>	'42',
		'font_h1_style'															=>	'normal',
		'font_h1_weight'														=>	'600',		
		'font_h2_family_google'													=>	'Source Sans Pro',
		'font_h2_family_system'													=>	'',
		'font_h2_size_1'														=>	'60',
		'font_h2_size_2'														=>	'52',
		'font_h2_size_3'														=>	'52',
		'font_h2_size_4'														=>	'34',
		'font_h2_style'															=>	'normal',
		'font_h2_weight'														=>	'200',		
		'font_h3_family_google'													=>	'Source Sans Pro',
		'font_h3_family_system'													=>	'',
		'font_h3_size_1'														=>	'42',
		'font_h3_size_2'														=>	'36',
		'font_h3_size_3'														=>	'36',
		'font_h3_size_4'														=>	'30',
		'font_h3_style'															=>	'normal',
		'font_h3_weight'														=>	'300',
		'font_h4_family_google'													=>	'Source Sans Pro',
		'font_h4_family_system'													=>	'',
		'font_h4_size_1'														=>	'24',
		'font_h4_size_2'														=>	'24',
		'font_h4_size_3'														=>	'24',
		'font_h4_size_4'														=>	'22',
		'font_h4_style'															=>	'normal',
		'font_h4_weight'														=>	'normal',		
		'font_h5_family_google'													=>	'Source Sans Pro',
		'font_h5_family_system'													=>	'',
		'font_h5_size_1'														=>	'20',
		'font_h5_size_2'														=>	'20',
		'font_h5_size_3'														=>	'20',
		'font_h5_size_4'														=>	'20',
		'font_h5_style'															=>	'normal',
		'font_h5_weight'														=>	'normal',		
		'font_h6_family_google'													=>	'Source Sans Pro',
		'font_h6_family_system'													=>	'',
		'font_h6_size_1'														=>	'18',
		'font_h6_size_2'														=>	'18',
		'font_h6_size_3'														=>	'18',
		'font_h6_size_4'														=>	'18',
		'font_h6_style'															=>	'normal',
		'font_h6_weight'														=>	'normal',	
		'fancybox_image_padding'												=>	'10',
		'fancybox_image_margin'													=>	'20',
		'fancybox_image_min_width'												=>	'100',
		'fancybox_image_min_height'												=>	'100',
		'fancybox_image_max_width'												=>	'9999',
		'fancybox_image_max_height'												=>	'9999',
		'fancybox_image_helper_button_enable'									=>	'1',
		'fancybox_image_autoresize'												=>	'1',
		'fancybox_image_autocenter'												=>	'1',
		'fancybox_image_fittoview'												=>	'1',
		'fancybox_image_arrow'													=>	'1',
		'fancybox_image_close_button'											=>	'1',
		'fancybox_image_close_click'											=>	'0',
		'fancybox_image_next_click'												=>	'0',
		'fancybox_image_mouse_wheel'											=>	'1',
		'fancybox_image_autoplay'												=>	'0',
		'fancybox_image_loop'													=>	'1',
		'fancybox_image_playspeed'												=>	'3000',
		'fancybox_image_animation_effect_open'									=>	'fade',
		'fancybox_image_animation_effect_close'									=>	'fade',
		'fancybox_image_animation_effect_next'									=>	'elastic',
		'fancybox_image_animation_effect_previous'								=>	'elastic',
		'fancybox_image_easing_open'											=>	'swing',
		'fancybox_image_easing_close'											=>	'swing',
		'fancybox_image_easing_next'											=>	'swing',
		'fancybox_image_easing_previous'										=>	'swing',
		'fancybox_image_speed_open'												=>	'250',
		'fancybox_image_speed_close'											=>	'250',
		'fancybox_image_speed_next'												=>	'250',
		'fancybox_image_speed_previous'											=>	'250',
		'fancybox_video_padding'												=>	'10',
		'fancybox_video_margin'													=>	'20',
		'fancybox_video_min_width'												=>	'100',
		'fancybox_video_min_height'												=>	'100',
		'fancybox_video_max_width'												=>	'9999',
		'fancybox_video_max_height'												=>	'9999',
		'fancybox_video_autoresize'												=>	'1',
		'fancybox_video_autocenter'												=>	'1',
		'fancybox_video_fittoview'												=>	'1',
		'fancybox_video_close_button'											=>	'1',
		'install'																=>	'1',
		'custom_css_responsive_1'												=>	'',
		'custom_css_responsive_2'												=>	'',
		'custom_css_responsive_3'												=>	'',
		'custom_css_responsive_4'												=>	''
	);	

	/**************************************************************************/
	/**************************************************************************/
	
	function __construct()
	{

	}
	
	/**************************************************************************/
	
	function prepareLibrary()
	{
		$this->libraryDefault=array
		(
			'script'															=>	array
			(
				'use'															=>	1,
				'inc'															=>	true,
				'path'															=>	THEME_URL_SCRIPT,
				'file'															=>	'',
				'in_footer'														=>	true,				
				'dependencies'													=>	array('jquery')
			),
			'style'																=>	array
			(
				'use'															=>	1,
				'inc'															=>	true,
				'path'															=>	THEME_URL_STYLE,
				'file'															=>	'',
				'dependencies'													=>	array()
			)			
		);
		
		$this->library=array
		(
			'script'															=>	array
			(
				'jquery'														=>	array
				(
					'use'														=>	3,
					'path'														=>	'',
					'in_footer'													=>	false,
					'dependencies'												=>	array()
				),
				'jquery-ui-core'												=>	array
				(
					'path'														=>	''
				),
				'jquery-ui-button'												=>	array
				(
					'path'														=>	''
				),
				'jquery-ui-slider'												=>	array
				(
					'path'														=>	''
				),
				'jquery-ui-autocomplete'										=>	array
				(
					'path'														=>	''
				),
				'jquery-qtip'													=>	array
				(
					'use'														=>	3,
					'file'														=>	'jquery.qtip.min.js'
				),
				'jquery-dropkick'												=>	array
				(
					'file'														=>	'jquery.dropkick.js'
				),
				'jquery-colorpicker'											=>	array
				(
					'file'														=>	'jquery.colorpicker.js'
				),
				'thickbox'														=>	array
				(
					'path'														=>	'',
					'dependencies'												=>	array()
				),
				'jquery-blockUI'												=>	array
				(
					'use'														=>	3,
					'file'														=>	'jquery.blockUI.js'
				),
				'jquery-ba-bqq'													=>	array
				(
					'use'														=>	3,
					'file'														=>	'jquery.ba-bqq.min.js'
				),
				'jquery-infieldlabel'											=>	array
				(
					'use'														=>	3,
					'file'														=>	'jquery.infieldlabel.min.js'
				),
				'jquery-themeOption'											=>	array
				(
					'file'														=>	'jquery.themeOption.js'
				),
				'jquery-themeOptionElement'										=>	array
				(
					'file'														=>	'jquery.themeOptionElement.js'
				),
				'jquery-fancybox'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.fancybox.js'
				),
				'jquery-fancybox-media'											=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.fancybox-media.js'
				),
				'jquery-fancybox-buttons'										=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.fancybox-buttons.js'
				),
				'jquery-fancybox-launch'										=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.fancybox.launch.js'
				),
				'jquery-actual'													=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.actual.min.js'
				),
				'jquery-mousewheel'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.mousewheel.js'
				),	
				'jquery-touchswipe'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.touchSwipe.min.js'
				),
				'jquery-easing'													=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.easing.js'
				),
				'jquery-carouFredSel'											=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.carouFredSel.packed.js'
				),
				'jquery-waypoint'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.waypoints.min.js'
				),				
				'jquery-waypoint-sticky'										=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.waypoints-sticky.min.js'
				),				
				'superfish'														=>	array
				(
					'use'														=>	2,
					'file'														=>	'superfish.js'
				),					
				'jquery-menu'													=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.menu.js'
				),				
				'jquery-scrollTo'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.scrollTo.min.js'
				),	
				'jquery-responsiveElement'										=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.responsiveElement.js'
				),
				'jquery-windowDimensionListener'								=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.windowDimensionListener.js'
				),	
				'jquery-hoverImage'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.hoverImage.js'
				),
				'jquery-preloaderImage'											=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.preloaderImage.js'
				),	
				'jquery-carousel'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.carousel.js'
				),
				'jquery-comment'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.comment.js'
				),
				'linkify'														=>	array
				(
					'use'														=>	2,
					'file'														=>	'linkify.js'
				),								
				'public'														=>	array
				(
					'use'														=>	2,
					'file'														=>	'public.js'
				)	
			),
			'style'																=>	array
			(
				'jquery-ui'														=>	array
				(
					'file'														=>	'jquery.ui.min.css'
				),
				'jquery-dropkick'												=>	array
				(
					'file'														=>	'jquery.dropkick.css'
				),
				'jquery-colorpicker'											=>	array
				(
					'file'														=>	'jquery.colorpicker.css'
				),	
				'thickbox'														=>	array
				(
					'path'														=>	'/'.WPINC.'/js/thickbox/',
					'file'														=>	'thickbox.css'
				),
				'google-font'                                                   =>	array
				(
                    'use'                                                       =>  3,
					'path'														=>	'',
                    'file'														=>	add_query_arg(array('family'=>'Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|Droid+Serif:400,400italic,700,700italic','subset'=>'cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese'),'//fonts.googleapis.com/css')                    
				),             
				'admin'															=>	array
				(
					'file'														=>	'admin.css'
				),
				'jquery-themeOption'											=>	array
				(
					'file'														=>	'jquery.themeOption.css'
				),
				'jquery-themeOption-overwrite'									=>	array
				(
					'file'														=>	'jquery.themeOption.overwrite.css'
				),	
				'jquery-qtip'													=>	array
				(
					'use'														=>	2,
					'file'														=>	'jquery.qtip.min.css'
				),
				'superfish'														=>	array
				(
					'use'														=>	2,
					'file'														=>	'superfish.css'
				),
				'jquery-fancybox'												=>	array
				(
					'use'														=>	2,
					'file'														=>	'fancybox/jquery.fancybox.css'
				),
				'jquery-fancybox-buttons'										=>	array
				(
					'use'														=>	2,
					'file'														=>	'fancybox/helpers/jquery.fancybox-buttons.css'
				),
				'widget'														=>	array
				(
					'use'														=>	2,
					'file'														=>	'widget.css'
				),
				'responsive-width-000-959'										=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'responsive/width-000-959.css',
					'dependencies'												=>	array('style')
				),
				'responsive-width-000-767'										=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'responsive/width-000-767.css',
					'dependencies'												=>	array('style')
				),
				'responsive-width-768-959'										=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'responsive/width-768-959.css',
					'dependencies'												=>	array('style')
				),
				'responsive-width-480-767'										=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'responsive/width-480-767.css',
					'dependencies'												=>	array('style')
				),
				'responsive-width-000-479'										=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'responsive/width-000-479.css',
					'dependencies'												=>	array('style')
				),
				'responsive-disable'											=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'responsive-disable.css',
					'dependencies'												=>	array('style')
				),
				'ts-frontend'													=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'TS.Frontend.css',
					'dependencies'												=>	array()
				),	
				'pb-frontend'													=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'PB.Frontend.css',
					'dependencies'												=>	array()
				),
				'pb-frontend-main'												=>	array
				(
					'use'														=>	2,
					'inc'														=>	false,
					'file'														=>	'PB.Frontend.main.css',
					'dependencies'												=>	array()
				),
				'style-custom'													=>	array
				(
					'use'														=>	2,
					'path'														=>	THEME_URL_MULTISITE_SITE_STYLE,
					'file'														=>	'style.css',
					'dependencies'												=>	array('style')
				),
				'style'															=>	array
				(
					'use'														=>	2,
					'path'														=>	null,
					'file'														=>	get_stylesheet_uri(),
					'dependencies'												=>	array()
				)
			)
		);
		
		foreach($this->library as $libraryType=>$libraryTypeData)
		{
			$library=array_keys($libraryTypeData);
			
			foreach($library as $libraryName)
				$this->library[$libraryType][$libraryName]=array_merge($this->libraryDefault[$libraryType],$this->library[$libraryType][$libraryName]);
		}
	}
	
	/**************************************************************************/
	
	function addLibrary($type,$use)
	{
		foreach($this->library[$type] as $index=>$data)
		{
			if(!$data['inc']) continue;
			
			if($data['use']!=3)
			{
				if($data['use']!=$use) continue;
			}
			
			if($type=='script')
			{
				wp_enqueue_script($index,$data['path'].$data['file'],$data['dependencies'],false,$data['in_footer']);
			}
			else 
			{
				wp_enqueue_style($index,$data['path'].$data['file'],$data['dependencies'],false);
			}
		}
	}
	
	/**************************************************************************/
	
	function includeLibrary($test,$script=array(),$style=array())
	{
		if($test!=1) return;

		foreach((array)$script as $value)
		{
			if(array_key_exists($value,$this->library['script']))
				$this->library['script'][$value]['inc']=true;
		}
		foreach((array)$style as $value)
		{
			if(array_key_exists($value,$this->library['style']))
				$this->library['style'][$value]['inc']=true;	
		}
	}
	
	/**************************************************************************/

	function adminInit()
	{
		$this->prepareLibrary();
		
		$this->addLibrary('style',1);
		$this->addLibrary('script',1);
	}
	
	/**************************************************************************/
	
	function adminPrintScript()
	{

	}
	
	/**************************************************************************/
	
	function adminMenuInit()
	{
		add_action('admin_print_scripts',array($this,'adminPrintScript'));
		add_theme_page(__('Theme Options','atrium'),__('Theme Options','atrium'),'edit_theme_options','ThemeOptions',array($this,'adminOptionPanelCreate'));
	}
	
	/**************************************************************************/
	
	function adminOptionPanelCreate()
	{
		$data=array();
		
		$CSS=new ThemeCSS();
		$Menu=new ThemeMenu();
		$Blog=new ThemeBlog();
		$Easing=new ThemeEasing();
		$Fancybox=new ThemeFancybox();
		$GoogleFont=new ThemeGoogleFont();
		$WidgetArea=new ThemeWidgetArea();
		
		$data['option']=ThemeOption::getOptionObject();
				
		$data['dictionary']['easingType']=$Easing->easingType;
		$data['dictionary']['fancyboxTransitionType']=$Fancybox->transitionType;
		
		$data['dictionary']['googleFont']=$GoogleFont->unpack();
		
		$data['dictionary']['fontStyle']=$CSS->fontStyle;
		$data['dictionary']['fontWeight']=$CSS->fontWeight;
		
		$data['dictionary']['page']=get_pages(array('hierarchical'=>0));
		
		$data['dictionary']['sortDirection']=$Blog->sortDirection;
		$data['dictionary']['sortPostBlogField']=$Blog->sortPostBlogField;

		$data['dictionary']['widgetArea-1']=$WidgetArea->getWidgetAreaDictionary(true,false,false);
		$data['dictionary']['widgetAreaLocation-1']=$WidgetArea->getWidgetAreaLocationDictionary(true,false,false);
		
		$data['dictionary']['widgetArea-2']=$WidgetArea->getWidgetAreaDictionary(true,true,true);
		$data['dictionary']['widgetAreaLocation-2']=$WidgetArea->getWidgetAreaLocationDictionary(true,true,true);
		
		$data['dictionary']['menu-1']=$Menu->getMenuDictionary(true,false,false);
		$data['dictionary']['menu-2']=$Menu->getMenuDictionary(true,true,true);
		
		$Template=new ThemeTemplate($data,THEME_PATH_TEMPLATE.'admin/admin.php');
		echo $Template->output();			
	}
	
	/**************************************************************************/
	
	function setupTheme()
	{	
		$install=(int)Themeoption::getOption('install');
		if($install==1) return;

		$data=array();
		
		$optionSave=array();
		$option=$this->themeDefaultOption;
	
		$Template=new ThemeTemplate($data,THEME_PATH_TEMPLATE.'footer_bottom_content.php');
		$option['footer_bottom_content']=$Template->output();	
		
		$optionCurrent=ThemeOption::getOptionObject();
		
		foreach($option as $index=>$value)
		{
			if(!array_key_exists($index,$optionCurrent))
				$optionSave[$index]=$value;
		}
		
		$optionSave=array_merge($optionSave,$optionCurrent);
		
		foreach($optionSave as $index=>$value)
		{
			if(!array_key_exists($index,$option))
				unset($optionSave[$index]);
		}

		
		$optionSave['install']=1;

		ThemeOption::resetOption();
		ThemeOption::updateOption($optionSave);
		
		$GoogleFont=new ThemeGoogleFont();
		$GoogleFont->download();
		
		$this->createCSSFile();
		
		/***/
		
		$argument=array
		(
			'post_type'							=>	array('post'),
			'post_status '						=>	'any',
			'posts_per_page'					=>	-1
		);
		
		$query=new WP_Query($argument);
		if($query===false) return;
	
		foreach($query->posts as $value)
		{
			$option=ThemeOption::getPostMeta($value);
			if(!is_array($option)) continue;
			
			if(!array_key_exists('post_prev_next_post_visible',$option))
			{
				$option['post_prev_next_post_visible']=-1;
				update_post_meta($value->ID,THEME_OPTION_PREFIX,$option);
			}
		}
	}
	
	/**************************************************************************/
	
	function switchTheme()
	{
		ThemeOption::updateOption(array('install'=>0));
	}
	
	/**************************************************************************/
	
	function adminOptionPanelSave()
	{
		$option=ThemeHelper::getPostOption();

		$response=array('global'=>array('error'=>1));

		$Blog=new ThemeBlog();
		$Notice=new ThemeNotice();
		$Easing=new ThemeEasing();
		$FancyBox=new ThemeFancybox();
		$Validation=new ThemeValidation($Notice);
		
		$invalidValue=esc_html__('Invalid value','atrium');
		
		/* Blog */
		if(!in_array($option['blog_sort_field'],array_keys($Blog->sortPostBlogField)))
			$Notice->addError(ThemeHelper::getFormName('blog_sort_field',false),$invalidValue);		
		if(!in_array($option['blog_sort_direction'],array_keys($Blog->sortDirection)))
			$Notice->addError(ThemeHelper::getFormName('blog_sort_direction',false),$invalidValue);	
		$Validation->notice('isNumber',array($option['blog_automatic_excerpt_length'],0,999),array(ThemeHelper::getFormName('blog_automatic_excerpt_length',false),$invalidValue));
		
		/* Post */
		$Validation->notice('isNumber',array($option['post_header_visible'],0,1),array(ThemeHelper::getFormName('post_header_visible',false),$invalidValue));
		$Validation->notice('isNumber',array($option['post_category_visible'],0,1),array(ThemeHelper::getFormName('post_category_visible',false),$invalidValue));
		$Validation->notice('isNumber',array($option['post_author_visible'],0,1),array(ThemeHelper::getFormName('post_author_visible',false),$invalidValue));
		$Validation->notice('isNumber',array($option['post_tag_visible'],0,1),array(ThemeHelper::getFormName('post_tag_visible',false),$invalidValue));
		$Validation->notice('isNumber',array($option['post_comment_count_visible'],0,1),array(ThemeHelper::getFormName('post_comment_count_visible',false),$invalidValue));
		$Validation->notice('isNumber',array($option['post_prev_next_post_visible'],0,1),array(ThemeHelper::getFormName('post_prev_next_post_visible',false),$invalidValue));

		/* Page */
		$Validation->notice('isNumber',array($option['page_header_visible'],0,1),array(ThemeHelper::getFormName('page_header_visible',false),$invalidValue));

		/* Plugins / Menu top */
		if(!in_array($option['menu_responsive_level'],array(960,768,480)))
			$Notice->addError(ThemeHelper::getFormName('menu_responsive_level',false),$invalidValue);	
		$Validation->notice('isNumber',array($option['menu_sticky_enable'],0,1),array(ThemeHelper::getFormName('menu_sticky_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['menu_hide_scroll_enable'],0,1),array(ThemeHelper::getFormName('menu_hide_scroll_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['menu_hover_first_level_enable'],0,1),array(ThemeHelper::getFormName('menu_hover_first_level_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['menu_hover_next_level_enable'],0,1),array(ThemeHelper::getFormName('menu_hover_next_level_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['menu_animation_enable'],0,1),array(ThemeHelper::getFormName('menu_animation_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['menu_animation_duration'],0,99999),array(ThemeHelper::getFormName('menu_animation_duration',false),$invalidValue));
		if(!in_array($option['menu_animation_easing'],array_keys($Easing->easingType)))
			$Notice->addError(ThemeHelper::getFormName('menu_animation_easing',false),$invalidValue);			

		/* Footer */
		$Validation->notice('isNumber',array($option['footer_enable'],0,1),array(ThemeHelper::getFormName('footer_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['footer_top_enable'],0,1),array(ThemeHelper::getFormName('footer_top_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['footer_bottom_enable'],0,1),array(ThemeHelper::getFormName('footer_bottom_enable',false),$invalidValue));
				
		/* Content copying */
		$Validation->notice('isNumber',array($option['right_click_enable'],0,1),array(ThemeHelper::getFormName('right_click_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['copy_selection_enable'],0,1),array(ThemeHelper::getFormName('copy_selection_enable',false),$invalidValue));
		
		/* Responsive mode */
		$Validation->notice('isNumber',array($option['responsive_mode_enable'],0,1),array(ThemeHelper::getFormName('responsive_mode_enable',false),$invalidValue));
		
		/* Go to page to */
		$Validation->notice('isNumber',array($option['go_to_page_top_enable'],0,1),array(ThemeHelper::getFormName('go_to_page_top_enable',false),$invalidValue));
		$Validation->notice('isNotEmpty',array($option['go_to_page_top_hash']),array(ThemeHelper::getFormName('go_to_page_top_hash',false),$invalidValue));
		$Validation->notice('isNumber',array($option['go_to_page_top_animation_enable'],0,1),array(ThemeHelper::getFormName('go_to_page_top_animation_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['go_to_page_top_animation_duration'],0,99999),array(ThemeHelper::getFormName('go_to_page_top_animation_duration',false),$invalidValue));
		if(!in_array($option['go_to_page_top_animation_easing'],array_keys($Easing->easingType)))
			$Notice->addError(ThemeHelper::getFormName('go_to_page_top_animation_easing',false),$invalidValue);			
		
		/* Font */
		$CSS=new ThemeCSS();
		$font=array('base','h1','h2','h3','h4','h5','h6');
		foreach($font as $value)
		{
			for($i=1;$i<=4;$i++)
				$Validation->notice('isNumber',array($option['font_'.$value.'_size_'.$i],1,100),array(ThemeHelper::getFormName('font_'.$value.'_size_1',false),$invalidValue));
			
			if(!in_array($option['font_'.$value.'_style'],array_keys($CSS->fontStyle)))
				$Notice->addError(ThemeHelper::getFormName('font_'.$value.'_style',false),$invalidValue);	
			if(!in_array($option['font_'.$value.'_weight'],array_keys($CSS->fontWeight)))
				$Notice->addError(ThemeHelper::getFormName('font_'.$value.'_weight',false),$invalidValue);	
		}
		
		/* Plugin / Fancybox for images */
		$Validation->notice('isNumber',array($option['fancybox_image_padding'],0,999),array(ThemeHelper::getFormName('fancybox_image_padding',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_margin'],0,999),array(ThemeHelper::getFormName('fancybox_image_margin',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_min_width'],1,9999),array(ThemeHelper::getFormName('fancybox_image_min_width',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_min_height'],1,9999),array(ThemeHelper::getFormName('fancybox_image_min_height',false),$invalidValue));		
		$Validation->notice('isNumber',array($option['fancybox_image_max_width'],1,9999),array(ThemeHelper::getFormName('fancybox_image_max_width',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_max_height'],1,9999),array(ThemeHelper::getFormName('fancybox_image_max_height',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_helper_button_enable'],0,1),array(ThemeHelper::getFormName('fancybox_image_helper_button_enable',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_autoresize'],0,1),array(ThemeHelper::getFormName('fancybox_image_autoresize',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_autocenter'],0,1),array(ThemeHelper::getFormName('fancybox_image_autocenter',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_fittoview'],0,1),array(ThemeHelper::getFormName('fancybox_image_fittoview',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_arrow'],0,1),array(ThemeHelper::getFormName('fancybox_image_arrow',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_close_button'],0,1),array(ThemeHelper::getFormName('fancybox_image_close_button',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_close_click'],0,1),array(ThemeHelper::getFormName('fancybox_image_close_click',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_next_click'],0,1),array(ThemeHelper::getFormName('fancybox_image_next_click',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_mouse_wheel'],0,1),array(ThemeHelper::getFormName('fancybox_image_mouse_wheel',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_autoplay'],0,1),array(ThemeHelper::getFormName('fancybox_image_autoplay',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_loop'],0,1),array(ThemeHelper::getFormName('fancybox_image_loop',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_playspeed'],1,99999),array(ThemeHelper::getFormName('fancybox_image_playspeed',false),$invalidValue));
		if(!in_array($option['fancybox_image_animation_effect_open'],array_keys($FancyBox->transitionType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_animation_effect_open',false),$invalidValue);	
		if(!in_array($option['fancybox_image_animation_effect_close'],array_keys($FancyBox->transitionType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_animation_effect_close',false),$invalidValue);	
		if(!in_array($option['fancybox_image_animation_effect_next'],array_keys($FancyBox->transitionType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_animation_effect_next',false),$invalidValue);	
		if(!in_array($option['fancybox_image_animation_effect_previous'],array_keys($FancyBox->transitionType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_animation_effect_previous',false),$invalidValue);	
		if(!in_array($option['fancybox_image_easing_open'],array_keys($Easing->easingType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_easing_open',false),$invalidValue);	
		if(!in_array($option['fancybox_image_easing_close'],array_keys($Easing->easingType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_easing_close',false),$invalidValue);	
		if(!in_array($option['fancybox_image_easing_next'],array_keys($Easing->easingType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_easing_next',false),$invalidValue);	
		if(!in_array($option['fancybox_image_easing_previous'],array_keys($Easing->easingType)))
			$Notice->addError(ThemeHelper::getFormName('fancybox_image_easing_previous',false),$invalidValue);	
		$Validation->notice('isNumber',array($option['fancybox_image_speed_open'],1,99999),array(ThemeHelper::getFormName('fancybox_image_speed_open',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_speed_close'],1,99999),array(ThemeHelper::getFormName('fancybox_image_speed_close',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_speed_next'],1,99999),array(ThemeHelper::getFormName('fancybox_image_speed_next',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_image_speed_previous'],1,99999),array(ThemeHelper::getFormName('fancybox_image_speed_previous',false),$invalidValue));
		
		/* Plugin / Fancybox for videos */
		$Validation->notice('isNumber',array($option['fancybox_video_padding'],0,999),array(ThemeHelper::getFormName('fancybox_video_padding',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_margin'],0,999),array(ThemeHelper::getFormName('fancybox_video_margin',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_min_width'],1,9999),array(ThemeHelper::getFormName('fancybox_video_min_width',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_min_height'],1,9999),array(ThemeHelper::getFormName('fancybox_video_min_height',false),$invalidValue));		
		$Validation->notice('isNumber',array($option['fancybox_video_max_width'],1,9999),array(ThemeHelper::getFormName('fancybox_video_max_width',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_max_height'],1,9999),array(ThemeHelper::getFormName('fancybox_video_max_height',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_autoresize'],0,1),array(ThemeHelper::getFormName('fancybox_video_autoresize',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_autocenter'],0,1),array(ThemeHelper::getFormName('fancybox_video_autocenter',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_fittoview'],0,1),array(ThemeHelper::getFormName('fancybox_video_fittoview',false),$invalidValue));
		$Validation->notice('isNumber',array($option['fancybox_video_close_button'],0,1),array(ThemeHelper::getFormName('fancybox_video_close_button',false),$invalidValue));

		if($Notice->isError())
		{
			$response['local']=$Notice->getError();
		}
		else
		{
			$response['global']['error']=0;
			ThemeOption::updateOption($option);
			
			$this->createCSSFile();
		}

		$response['global']['notice']=$Notice->createHTML(THEME_PATH_TEMPLATE.'notice.php');

		echo json_encode($response);
		exit;
	}
	
	/**************************************************************************/
	
	function publicInit()
	{
		require_once(ABSPATH.'wp-admin/includes/plugin.php');
		
		$this->prepareLibrary();
		
		$GoogleFont=new ThemeGoogleFont();
		$GoogleFont->setToStyle($this->library['style']);
		
		$this->includeLibrary(ThemeOption::getOption('responsive_mode_enable'),null,array('responsive-width-000-959','responsive-width-000-767','responsive-width-768-959','responsive-width-480-767','responsive-width-000-479'));
		$this->includeLibrary(!ThemeOption::getOption('responsive_mode_enable'),null,array('responsive-disable'));

		$this->includeLibrary(!class_exists('TSThemeStyle'),null,array('ts-frontend'));
		$this->includeLibrary(!class_exists('PBPageBuilder'),null,array('pb-frontend','pb-frontend-main'));
		
		$this->addLibrary('style',2);
		$this->addLibrary('script',2);
		
		$aPattern=array
		(
			'rightClick'			=>	'/^right_click_/',
			'selection'				=>	'/^copy_selection_/',
			'fancyboxImage'			=>	'/^fancybox_image_/',
			'fancyboxVideo'			=>	'/^fancybox_video_/',
			'goToPageTop'			=>	'/^go_to_page_top_/'
		);
		
		$option=ThemeOption::getOptionObject();
		
		foreach($aPattern as $indexPattern=>$valuePattern)
		{
			foreach($option as $index=>$value)
			{
				if(preg_match($valuePattern,$index,$result))
				{
					$nIndex=preg_replace($valuePattern,'',$index);
					$data[$indexPattern][$nIndex]=$value;
				}
			}
		}
		
		$data['config']['theme_url']=THEME_URL;
		
		$param=array
		(
			'l10n_print_after'=>'themeOption='.json_encode($data).';'
		);
			
		wp_localize_script('public','themeOption',$param);
	}
		
	/**************************************************************************/

	function automaticExcerptLength()
	{
		global $post;
		
		$length=55;

		switch($post->post_type)
		{
			case 'post':
				$length=ThemeOption::getOption('blog_automatic_excerpt_length');
			break;
		}
		
		return($length);
	}
	
	/**************************************************************************/
	
	function filterExcerptMore()
	{
		$Theme=new Theme();
		$length=$Theme->automaticExcerptLength();
		
		if($length>0) return('<span class="excerpt-more"> [...]</span>');
		
		return('');
	}
	
	/**************************************************************************/
	
	function addPlugin()
	{
		$plugin=array
		(
			array
			(
				'name'                                                          =>	'Classic Editor',
				'slug'                                                          =>	'classic-editor',
				'required'                                                      =>	true,
				'force_activation'                                              =>	true,
				'force_deactivation'                                            =>	true
			),
			array
			(
				'name'                                                          =>	'Page Builder',
				'slug'                                                          =>	'page-builder',
				'source'                                                        =>	THEME_PATH_SOURCE.'page-builder.zip',
				'required'                                                      =>	true,
				'version'                                                       =>	'2.1',
				'force_activation'                                              =>	true,
				'force_deactivation'                                            =>	true
			),
			array
			(
				'name'                                                          =>	'Theme Styles',
				'slug'                                                          =>	'theme-style',
				'source'                                                        =>	THEME_PATH_SOURCE.'theme-style.zip',
				'required'                                                      =>	true,
				'version'                                                       =>	'3.9',
				'force_activation'                                              =>	true,
				'force_deactivation'                                            =>	true
			),
			array
			(
				'name'                                                          =>	'Theme Demo Data Installer',
				'slug'                                                          =>	'theme-demo-data-installer',
				'source'                                                        =>	THEME_PATH_SOURCE.'theme-demo-data-installer.zip',
				'required'                                                      =>	false,
				'version'                                                       =>	'3.9',
				'force_activation'                                              =>	false,
				'force_deactivation'                                            =>	true
			),
			array
			(
				'name'                                                          =>	'Better WordPress Minify',
				'slug'                                                          =>	'bwp-minify',
				'required'                                                      =>	false,
				'force_activation'                                              =>	false,
				'force_deactivation'                                            =>	false
			),		
		);
	
		$config=array
		(
			'default_path'							=>	'',                      
			'menu'									=>	'tgmpa-install-plugins', 
			'has_notices'							=>	true,
			'dismissable'							=>	true,
			'dismiss_msg'							=>	'',
			'is_automatic'							=>	true,
			'message'								=>	'', 
			'strings'								=>	array
			(
				'page_title'						=>	__('Install Required Plugins','atrium'),
				'menu_title'						=>	__('Install Plugins','atrium'),
				'installing'						=>	__('Installing Plugin: %s','atrium'),
				'oops'								=>	__('Something went wrong with the plugin API.','atrium'),
				'notice_can_install_required'		=>	_n_noop('This theme requires the following plugin: %1$s.','This theme requires the following plugins: %1$s.','atrium'),
				'notice_can_install_recommended'	=>	_n_noop('This theme recommends the following plugin: %1$s.','This theme recommends the following plugins: %1$s.', 'atrium' ),
				'notice_cannot_install'				=>	_n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.','atrium'),
				'notice_can_activate_required'		=>	_n_noop('The following required plugin is currently inactive: %1$s.','The following required plugins are currently inactive: %1$s.', 'atrium' ),
				'notice_can_activate_recommended'	=>	_n_noop('The following recommended plugin is currently inactive: %1$s.','The following recommended plugins are currently inactive: %1$s.', 'atrium' ),
				'notice_cannot_activate'			=>	_n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.','atrium'),
				'notice_ask_to_update'				=>	_n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'atrium' ),
				'notice_cannot_update'				=>	_n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.','atrium'),
				'install_link'						=>	_n_noop('Begin installing plugin','Begin installing plugins','atrium'),
				'activate_link'						=>	_n_noop('Begin activating plugin','Begin activating plugins','atrium'),
				'return'							=>	__('Return to Required Plugins Installer','atrium'),
				'plugin_activated'					=>	__('Plugin activated successfully.','atrium' ),
				'complete'							=>	__('All plugins installed and activated successfully. %s','atrium'),
				'nag_type'							=>	'updated'
			)
		);

		tgmpa($plugin,$config);
	}
	
	/**************************************************************************/
	
	function createCSSFile()
	{
		if($this->createMultisitePath()===false) return;
		
		$content=null;
		
		$CSS=new ThemeCSS();
		$Validation=new ThemeValidation();
		
		ThemeOption::refreshOption();

		$media=array
		(
			1	=>	array(),
			2	=>	array('min-width'=>768,'max-width'=>959),
			3	=>	array('min-width'=>480,'max-width'=>767),
			4	=>	array('max-width'=>479)
		);

		if(ThemeOption::getOption('responsive_mode_enable')!=1)
			unset($media[2],$media[3],$media[4]);

		$selector=array
		(
			'base'		=>	array
			(
				'a',
				'body',
				'input',
				'select',
				'textarea'
			),
			'h1'		=>	array
			(
				'h1',
				'h1 a'
			),
			'h2'		=>	array
			(
				'h2',
				'h2 a'
			),
			'h3'		=>	array
			(
				'h3',
				'h3 a'
			),
			'h4'		=>	array
			(
				'h4',
				'h4 a',

			),
			'h5'		=>	array
			(
				'h5',
				'h5 a',
				'h3.comment-reply-title',
				'h3.comment-reply-title a'
			),
			'h6'		=>	array
			(
				'h6',
				'h6 a'
			)
		);

		foreach($selector as $index=>$value)
		{
			$content.=$CSS->create(array
			(
				'selector'	=>	$value,
				'property'	=>	array
				(
					'font-family'	=>	array(ThemeOption::getOption('font_'.$index.'_family_google'),ThemeOption::getOption('font_'.$index.'_family_system')),
					'font-size'		=>	ThemeOption::getOption('font_'.$index.'_size'),
					'font-style'	=>	ThemeOption::getOption('font_'.$index.'_style'),
					'font-weight'	=>	ThemeOption::getOption('font_'.$index.'_weight')
				)
			));	
		}

		foreach($media as $mediaIndex=>$mediaValue)
		{
			foreach($selector as $selectorIndex=>$selectorValue)
			{
				$content.=$CSS->create(array
				(
					'media'		=>	$mediaValue,
					'selector'	=>	$selectorValue,
					'property'	=>	array
					(
						'font-size'	=>	ThemeOption::getOption('font_'.$selectorIndex.'_size_'.$mediaIndex)
					)
				));		
			}
		}

		if(ThemeOption::getOption('responsive_mode_enable')==1)
		{
			if(ThemeOption::getOption('menu_top_mobile_visible_step')!=0)
			{
				if(ThemeOption::getOption('menu_top_mobile_visible_step')==1) $maxWidth=959;
				if(ThemeOption::getOption('menu_top_mobile_visible_step')==2) $maxWidth=769;
				if(ThemeOption::getOption('menu_top_mobile_visible_step')==3) $maxWidth=479;

				$content.=$CSS->create(array
				(
					'media'		=>	array('min-width'=>0,'max-width'=>$maxWidth),
					'selector'	=>	array
					(
						'ul.sf-menu',
					),
					'property'	=>	array
					(
						'display'		=> 'none'
					)
				));

				$content.=$CSS->create(array
				(
					'media'		=>	array('min-width'=>0,'max-width'=>$maxWidth),
					'selector'	=>	array
					(
						'select.dp-menu',
					),
					'property'	=>	array
					(
						'display'		=> 'block'
					)
				));	
			}
		}

		if((isset($media[1])) && ($Validation->isNotEmpty(ThemeOption::getOption('custom_css_responsive_1')))) 
			$content.=ThemeOption::getOption('custom_css_responsive_1');

		if((isset($media[2])) && ($Validation->isNotEmpty(ThemeOption::getOption('custom_css_responsive_2'))))
		{
			$content.=
			'
			@media only screen  and (min-width:768px) and (max-width:959px)
			{
			'.ThemeOption::getOption('custom_css_responsive_2').'
			}
			';
		}

		if((isset($media[3])) && ($Validation->isNotEmpty(ThemeOption::getOption('custom_css_responsive_3'))))
		{
			$content.=
			'
			@media only screen  and (min-width:480px) and (max-width:767px)
			{
			'.ThemeOption::getOption('custom_css_responsive_3').'
			}
			';
		}

		if((isset($media[4])) && ($Validation->isNotEmpty(ThemeOption::getOption('custom_css_responsive_4'))))
		{
			$content.=
			'
			@media only screen  and (max-width:479px)
			{
			'.ThemeOption::getOption('custom_css_responsive_4').'
			}
			';
		}
		
		$file=THEME_PATH_MULTISITE_SITE_STYLE.'style.css';
		
		$ThemeWPFileSystem=new ThemeWPFileSystem();
		if($ThemeWPFileSystem->put_contents($file,$content,0755)===false) return(false);
		
		return(true);	
	}
	
	/**************************************************************************/
	
	function createMultisitePath()
	{
		$data=array
		(
			THEME_PATH_MULTISITE_SITE,
			THEME_PATH_MULTISITE_SITE_STYLE
		);

		foreach($data as $path)
		{
			if(!ThemeFile::dirExist($path)) mkdir($path);			
			if(!ThemeFile::dirExist($path)) return(false);
		}
		
		return(true);
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/