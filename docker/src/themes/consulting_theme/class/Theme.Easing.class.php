<?php

/******************************************************************************/
/******************************************************************************/

class ThemeEasing
{
	/**************************************************************************/
	
	function __construct()
	{		
		$this->easingType=array
		(
			'easeInQuad'			=>	array(__('easeInQuad','atrium')),
			'easeOutQuad'			=>	array(__('easeOutQuad','atrium')),
			'easeInOutQuad'			=>	array(__('easeInOutQuad','atrium')),
			'easeInCubic'			=>	array(__('easeInCubic','atrium')),
			'easeOutCubic'			=>	array(__('easeOutCubic','atrium')),
			'easeInOutCubic'		=>	array(__('easeInOutCubic','atrium')),
			'easeInQuart'			=>	array(__('easeInQuart','atrium')),
			'easeOutQuart'			=>	array(__('easeOutQuart','atrium')),
			'easeInOutQuart'		=>	array(__('easeInOutQuart','atrium')),
			'easeInOutQuart'		=>	array(__('easeInOutQuart','atrium')),
			'easeInQuint'			=>	array(__('easeInQuint','atrium')),
			'easeOutQuint'			=>	array(__('easeOutQuint','atrium')),
			'easeInOutQuint'		=>	array(__('easeInOutQuint','atrium')),
			'easeInSine'			=>	array(__('easeInSine','atrium')),
			'easeOutSine'			=>	array(__('easeOutSine','atrium')),
			'easeInOutSine'			=>	array(__('easeInOutSine','atrium')),
			'easeInExpo'			=>	array(__('easeInExpo','atrium')),
			'easeOutExpo'			=>	array(__('easeOutExpo','atrium')),
			'easeInOutExpo'			=>	array(__('easeInOutExpo','atrium')),
			'easeInCirc'			=>	array(__('easeInCirc','atrium')),
			'easeOutCirc'			=>	array(__('easeOutCirc','atrium')),
			'easeInOutCirc'			=>	array(__('easeInOutCirc','atrium')),
			'easeInElastic'			=>	array(__('easeInElastic','atrium')),
			'easeOutElastic'		=>	array(__('easeOutElastic','atrium')),
			'easeInOutElastic'		=>	array(__('easeInOutElastic','atrium')),
			'easeInBack'			=>	array(__('easeInBack','atrium')),
			'easeOutBack'			=>	array(__('easeOutBack','atrium')),
			'easeInOutBack'			=>	array(__('easeInOutBack','atrium')),
			'easeInBounce'			=>	array(__('easeInBounce','atrium')),
			'easeOutBounce'			=>	array(__('easeOutBounce','atrium')),
			'easeInOutBounce'		=>	array(__('easeInOutBounce','atrium'))
		);	
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/