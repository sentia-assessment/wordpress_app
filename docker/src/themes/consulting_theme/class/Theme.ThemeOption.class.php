<?php

/******************************************************************************/
/******************************************************************************/

class ThemeOption
{
	/**************************************************************************/
	
	static function createOption($refresh=false)
	{
		$GlobalData=new ThemeGlobalData();
		return($GlobalData->setGlobalData(THEME_OPTION_GLOBAL_ARRAY_KEY,array('ThemeOption','createOptionObject'),$refresh));				
	}
	
	/**************************************************************************/
	
	static function createOptionObject()
	{	
		return((array)get_option(THEME_OPTION_PREFIX));
	}
	
	/**************************************************************************/
	
	static function refreshOption()
	{
		return(self::createOption(true));
	}
	
	/**************************************************************************/
	
	static function getOption($name)
	{
		self::createOption();
		
		global $aropwt_globalData;
		
		if(!array_key_exists($name,$aropwt_globalData[THEME_OPTION_GLOBAL_ARRAY_KEY])) return(null);
		return($aropwt_globalData[THEME_OPTION_GLOBAL_ARRAY_KEY][$name]);		
	}
	
	/**************************************************************************/
	
	static function getGlobalOption($post,$name)
	{
		self::createOption();
		
		$value=0;
		$prefix=array
		(
			'post'						=>	array('post','post'),
			'page'						=>	array('page','page')
		);
		
		if(!is_object($post)) return($value);
		if(!in_array($post->post_type,array_keys($prefix))) return($value);
		
		$option=self::getPostMeta($post);
		
		$key=$prefix[$post->post_type][0].'_'.$name;
			
		if(array_key_exists($key,(array)$option)) $value=$option[$key];
		
		$key=$prefix[$post->post_type][1].'_'.$name;
		
		if($value==-1) $value=self::getOption($key);
		
		return($value);
	}
	
	/**************************************************************************/
	
	static function getOptionObject()
	{
		global $aropwt_globalData;
		return($aropwt_globalData[THEME_OPTION_GLOBAL_ARRAY_KEY]);
	}
	
	/**************************************************************************/
	
	static function updateOption($option)
	{
		$nOption=array();
		foreach($option as $index=>$value) $nOption[$index]=$value;
		
		$oOption=self::refreshOption();

		update_option(THEME_OPTION_PREFIX,array_merge($oOption,$nOption));
		
		self::refreshOption();
	}
	
	/**************************************************************************/
	
	static function resetOption()
	{
		update_option(THEME_OPTION_PREFIX,array());
		self::refreshOption();		
	}
	
	/**************************************************************************/
	
	static function getPostMeta($post)
	{
		$id=is_object($post) ? $post->ID : (int)$post;
		return(get_post_meta($id,THEME_OPTION_PREFIX,true));
	}
	
	/**************************************************************************/
}

/******************************************************************************/
/******************************************************************************/