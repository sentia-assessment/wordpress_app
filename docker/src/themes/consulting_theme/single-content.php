<?php
		global $post,$aropwt_parentPost;
		
		the_post();

		$Page=new ThemePage();
		$Post=new ThemePost();
		$Validation=new ThemeValidation();
		$WidgetArea=new ThemeWidgetArea();
		
		$widgetAreaData=$WidgetArea->getWidgetAreaByPost($aropwt_parentPost->post,true,true);
		
		$visibleOption=array();

		$visibleOption['post_tag_visible']=ThemeOption::getGlobalOption($post,'tag_visible');
		$visibleOption['post_author_visible']=ThemeOption::getGlobalOption($post,'author_visible');
		$visibleOption['post_category_visible']=ThemeOption::getGlobalOption($post,'category_visible');
		$visibleOption['post_comment_count_visible']=ThemeOption::getGlobalOption($post,'comment_count_visible') && comments_open(get_the_id());
		$visibleOption['post_prev_next_post_visible']=ThemeOption::getGlobalOption($post,'prev_next_post_visible');
?>
		<div <?php post_class('theme-clear-fix theme-post'); ?> id="post-<?php the_ID(); ?>">
<?php
		if(has_post_thumbnail())
		{
			$image=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
?>
			<div class="theme-post-section-top">
					
				<div class="theme-post-image theme-post-image-type-image <?php echo $Page->getImageClass($widgetAreaData['location']); ?>">
					<a href="<?php echo $image[0]; ?>" class="theme-preloader-image">
						<?php echo get_the_post_thumbnail(get_the_ID(),$Page->getImageClass($widgetAreaData['location'])); ?>
						<span><span></span></span>
					</a>
				</div>
						
			</div>
<?php
		}
?>				
			<div class="theme-post-section-bottom">
						
				<div class="theme-post-section-bottom-left">
<?php
		$Post->formatPostDate($post->post_date,$day,$month,$year);
?>
					<div class="theme-post-date">
						<span><?php echo $day; ?></span>
						<span><?php echo esc_html($month.' '.$year); ?></span>
					</div>
<?php
				if($visibleOption['post_comment_count_visible']==1)
				{
?>
					<div class="theme-post-comment-count">
						<span><?php comments_number('0','1','%'); ?></span>
						<span><?php echo esc_html('Replies'); ?></span>
					</div>
<?php
				}
?>
				</div>
						
				<div class="theme-post-section-bottom-right">
							
					<div class="theme-post-content">
						<?php the_content(); ?>
						<div class="theme-blog-pagination">
<?php 
				wp_link_pages(array
				(
					'before'		=>	null,
					'after'			=>	null,
					'link_before'	=> '<span>',
					'link_after'	=> '</span>'
				)); 
?>
						</div>
					</div>
<?php
		if(($visibleOption['post_author_visible']) || ($visibleOption['post_author_visible']) || ($visibleOption['post_author_visible']))
		{
?>
					<div class="theme-post-meta">
<?php
			if($visibleOption['post_author_visible']==1)
			{
?>
						<div class="theme-post-meta-author">
							<?php echo esc_html('By').' '.get_the_author(); ?>
						</div>
<?php
			}

			if($visibleOption['post_category_visible']==1)
			{
				$category=get_the_category(get_the_ID());
				$count=count($category);
						
				if($count)
				{		
?>
						<div class="theme-post-meta-category">
									
							<ul class="theme-reset-list">
<?php
					foreach($category as $index=>$value)
					{
						$separator=$index==$count-1 ? '' : ',&nbsp;';
						$title=$Validation->isEmpty($value->description) ? sprintf(__('View all posts filed under %s','atrium'),$value->name) : strip_tags(apply_filters('category_description',$value->description,$value));
?>
								<li><a href="<?php echo get_category_link($value->term_id); ?>" title="<?php esc_attr($title); ?>"><?php echo esc_html($value->name); ?></a><?php echo $separator; ?></li>
<?php
					}
?>
							</ul>

						</div>
<?php
				}
			}

			if($visibleOption['post_tag_visible']==1)
			{
				$i=0;
				$tag=get_the_tags();
						
				if($tag) 
				{
					$count=count($tag);
?>
						<div class="theme-post-meta-tag">
									
							<ul class="theme-reset-list">
			
<?php
					foreach($tag as $index=>$value)
					{
						$i++;
						$separator=$i==$count ? '' : ',&nbsp;';
?>
								<li><a href="<?php echo get_tag_link($value->term_id); ?>"><?php echo $value->name; ?></a><?php echo $separator; ?></li>
<?php
					}
?>
							</ul>
			
						</div>
<?php
				}
			}
?>
					</div>
<?php
		}
		
		if($visibleOption['post_prev_next_post_visible']==1)
		{
			$html=null;
			
			$prevPost=get_previous_post();
			if(!empty($prevPost)) $html.='<a class="theme-post-navigation-prev" href="'.get_permalink($prevPost->ID).'"><span class="theme-post-navigation-arrow"></span><span class="theme-post-navigation-content">'.get_the_title($prevPost->ID).'</span></a>';
			
			$nextPost=get_next_post();
			if(!empty($nextPost)) $html.='<a class="theme-post-navigation-next" href="'.get_permalink($nextPost->ID).'"><span class="theme-post-navigation-content">'.get_the_title($nextPost->ID).'</span><span class="theme-post-navigation-arrow"></span></a>';		
			
			if($Validation->isNotEmpty($html))
			{
?>	
					<div class="theme-post-navigation theme-clear-fix">
						<?php echo $html; ?>
					</div>
<?php
			}
		}
?>
				</div>
				
			</div>
<?php
		if(!post_password_required())
		{
?>
			<div id="comments">
				<?php comments_template(); ?>
			</div>
<?php 
			$Comment=new ThemeComment();

			$commenter=wp_get_current_commenter();
			$req=get_option('require_name_email');
			$aria_req=($req ? ' aria-required=\'true\'' : '');

			$field=array();

			$field['author']=
			'
				<p class="theme-clear-fix">
					<label for="author" class="theme-infield-label">'.__('Name','atrium').($req ? ' <span class="required">*</span>' : '').'</label>
					<input id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'" size="30"'.$aria_req.'/>
				</p>
			';

			$field['email']=
			'
				<p class="theme-clear-fix">
					<label for="email" class="theme-infield-label">'.__('Email','atrium').($req ? ' <span class="required">*</span>' : '').'</label>
					<input id="email" name="email" type="text" value="'.esc_attr($commenter['comment_author_email']).'" size="30"'.$aria_req.'/>
				</p>
			';

			$field['url']=
			'
				<p class="theme-clear-fix">
					<label for="url" class="theme-infield-label">'.__('Website','atrium').'</label>
					<input id="url" name="url" type="text" value="'.esc_attr($commenter['comment_author_url']).'" size="30"/>
				</p>
			';

			$commentField=
			'
				<p class="theme-clear-fix">
					<label for="comment" class="theme-infield-label">'.__('Comment','atrium').' <span class="required">*</span></label>
					<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
				</p>	
			';

			$argument=array
			(
				'id_form'				=>	'comment-form',
				'title_reply'			=>	'<span class="pb-header-content">'.__('Share Your Thoughts','atrium').'</span><span class="pb-header-underline"></span>',
				'cancel_reply_link'		=>	__('Cancel reply','atrium'),
				'comment_field'			=>	$commentField,
				'fields'				=>	apply_filters('comment_form_default_fields',$field),
				'label_submit'			=>	__('Post comment','atrium')
			);

			comment_form($argument); 
?>
		<script type="text/javascript">
			jQuery(document).ready(function($) 
			{
				$().ThemeComment({'requestURL':'<?php echo admin_url('admin-ajax.php'); ?>','page':<?php echo $Comment->page; ?>});
			});
		</script>
<?php
		}
?>			
	</div>