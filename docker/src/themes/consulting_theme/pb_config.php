<?php

/******************************************************************************/
/******************************************************************************/

$path='/multisite/'.get_current_blog_id().'/style/';

PBData::set('theme_path_multisite_style',get_template_directory().$path);
PBData::set('theme_url_multisite_style',get_template_directory_uri().$path);

/******************************************************************************/

PBData::set('parent_post_variable_name','aropwt_parentPost');

/******************************************************************************/

PBComponentData::set('list','bullet',array
(
	'default'		=>	array
	(
		'url'			=>	get_template_directory_uri().'/media/image/public/icon_bullet/default/',
		'path'			=>	get_template_directory().'/media/image/public/icon_bullet/default/',
		'css_class'		=>	''
	),
	'footer'			=>	array
	(
		'url'			=>	get_template_directory_uri().'/media/image/public/icon_bullet/footer/',
		'path'			=>	get_template_directory().'/media/image/public/icon_bullet/footer/',
		'css_class'		=>	'div.theme-footer-top'
	),	
));

PBComponentData::set('list','bullet_path',get_template_directory().'/media/image/public/icon_bullet/');
PBComponentData::set('list','bullet_url',get_template_directory_uri().'/media/image/public/icon_bullet/');

PBComponentData::set('notice','icon_path',get_template_directory().'/media/image/public/icon_feature/medium/');
PBComponentData::set('notice','icon_url',get_template_directory_uri().'/media/image/public/icon_feature/medium/');

PBComponentData::set('social_icon','icon_path',get_template_directory().'/media/image/public/icon_social/');
PBComponentData::set('social_icon','icon_url',get_template_directory_uri().'/media/image/public/icon_social/');

PBComponentData::set('team','social_icon_path',get_template_directory().'/media/image/public/icon_social/');
PBComponentData::set('team','social_icon_url',get_template_directory_uri().'/media/image/public/icon_social/');

PBComponentData::set('feature','icon',array
(
	'small'		=>	array
	(
		'label'		=>	__('Small','atrium'),
		'path'		=>	get_template_directory().'/media/image/public/icon_feature/small/',
		'url'		=>	get_template_directory_uri().'/media/image/public/icon_feature/small/',
	),
	'medium'	=>	array
	(
		'label'		=>	__('Medium','atrium'),
		'path'		=>	get_template_directory().'/media/image/public/icon_feature/medium/',
		'url'		=>	get_template_directory_uri().'/media/image/public/icon_feature/medium/'
	),
	'large'		=>	array
	(
		'label'		=>	__('Large','atrium'),
		'path'		=>	get_template_directory().'/media/image/public/icon_feature/large/',
		'url'		=>	get_template_directory_uri().'/media/image/public/icon_feature/large/'
	)
));

PBComponentData::set('button','icon_path',get_template_directory().'/media/image/public/icon_feature/tiny/');
PBComponentData::set('button','icon_url',get_template_directory_uri().'/media/image/public/icon_feature/tiny/');

PBComponentData::set('menu','responsive_level',array
(
	'960'		=>	array
	(
		'label'		=>	__('960','atrium')
	),
	'768'	=>	array
	(
		'label'		=>	__('768','atrium')
	),
	'480'		=>	array
	(
		'label'		=>	__('480','atrium')
	)
));

PBComponentData::set('gallery','image_default','image-480-384');
PBComponentData::set('nivo_slider','image_default','image-480-384');
PBComponentData::set('resent_post','image_default','image-480-384');
PBComponentData::set('image_carousel','image_default','image-1920-1200');

/******************************************************************************/
/******************************************************************************/