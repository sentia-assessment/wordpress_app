/******************************************************************************/
/******************************************************************************/

;(function($,doc,win) 
{
	"use strict";
	
	var responsiveElement=function(object,option)
	{
		/**********************************************************************/
		
		var $this=$(object);
		
		var $optionDefault=
		{
			width		:	460,
			children	:	'*'
		};
		
		var $option=$.extend($optionDefault,option);

		/**********************************************************************/

		this.create=function() 
		{
			this.responsive($option.children,$option.width,$option.className);
			
			var self=this;
			$(window).bind('resize',function() 
			{
				self.responsive($option.children,$option.width,$option.className);
			});
		};
		
		/**********************************************************************/
		
		this.responsive=function()
		{
			$this.each(function() 
			{
				var actualWidth=$(this).actual('outerWidth',{includeMargin:false});
				if($option.children==null)
				{
					if(actualWidth<=$option.width) $(this).addClass($option.className);
					else $(this).removeClass($option.className);				
				}
				else
				{
					if(actualWidth<=$option.width) $(this).children($option.children).addClass($option.className);
					else $(this).children($option.children).removeClass($option.className);	
				}
			});
		};
		
		/**********************************************************************/
	}
	
	/**************************************************************************/
	
	$.fn.responsiveElement=function(option) 
	{
		var element=new responsiveElement(this,option);
		element.create();
	};
	
	/**************************************************************************/

})(jQuery,document,window);