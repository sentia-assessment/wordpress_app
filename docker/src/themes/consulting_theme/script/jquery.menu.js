/******************************************************************************/
/******************************************************************************/

;(function($,doc,win) 
{
	"use strict";
	
	var Menu=function(object,option)
	{
		/**********************************************************************/
		
		var $this=$(object);
		var $option=option;

		/**********************************************************************/

		this.build=function() 
		{
			var self=this;
			var menu=$this;
			var menuList=menu.find('ul.sf-menu');
			
			/***/
	
			if($option.animation_enable==0)
				$option.animation_duration=0;
	
			menuList.superfish(
			{ 
				delay		:	400, 
				animation	:	{opacity:'show',height:'show'}, 
				speed		:	300,                         
				cssArrows	:	false                       
			}); 
			
			/***/
			
			if($option.sticky_enable==1)
			{
				$('body').preloaderImage(
				{
					preloaderImageClassName		:	'pb-preloader-image',
					onReady						:	function() 
					{
						$.waypoints('refresh');
						
						$this.waypoint('sticky',
						{
							wrapper		:	'<div>',
							stuckClass	:	'pb-menu-logo-sticky'
						});
					}
				});		
			}
			
			/***/
			
			if(($option.hide_scroll_enable==1) && ($option.sticky_enable==1))
			{
				$(window).scroll(function() 
				{
					var height=$this.actual('height');
					
					if($this.hasClass('pb-menu-logo-sticky'))
					{
						$this.stop().animate({'opacity':0,'margin-top':-1*height},{duration:250,complete:function() {}});
						
						window.clearTimeout($.data(this,'scrollTimer'));
						$.data(this,'scrollTimer',setTimeout(function() 
						{
							$this.stop().animate({'opacity':1,'margin-top':0},{duration:100,complete:function() {}});
						},100));
					}
					else
					{
						$this.css({'opacity':1,'margin-top':0});
					}
				});
			}
			
			/***/
			
			self.setMenuResponsive();
			
			$this.find('.pb-menu-responsive>select').on('change',function() 
			{
				window.location=$(this).val();
			});
			
			/***/
			
			$(window).resize(function()  
			{
				$('.pb-menu-logo .pb-menu>ul.sf-menu').each(function() 
				{	
					var menuElement=$(this).find('li.pb-menu-item-selected').parent('li');
					self.setSelected(menuElement,$option.animation_duration);
				});				
			});

			/***/

			$(window).bind('hashchange',function(e) 
			{
				self.scrollTo();
			});	
			
			$(window).resize(function() 
			{
				self.setMenuResponsive();
				$.waypoints('refresh');
			});
			
			$this.css('display','block');
			
			$('body.pb>.pb-line').waypoint(function(direction)
			{
				if(direction=='down')
				{
					self.setSelectedTo($(this));
				}
			},
			{
				offset	:	'50%'
			})
			.waypoint(function(direction)
			{
				if(direction=='up')
				{
					var prevPage=$(this).prevAll('.pb-line').first();
					if(prevPage.length==1) self.setSelectedTo(prevPage);
				}
			},
			{
				offset	:	'50%'
			});
		};
		
		/**********************************************************************/
		
		this.setSelectedTo=function(line)
		{
			var self=this;
			var hash=this.getValueFromClass(line,'pb-line-css-group-');
	
			$('.pb-menu-logo .pb-menu>ul.sf-menu').each(function() 
			{	
				var menuElement=$(this).find('a[href="#'+hash+'"]').parent('li');
				self.setSelected(menuElement,$option.animation_duration);
			});
		};
		
		/**********************************************************************/

		this.setSelected=function(menuElement,duration)
		{
			if(menuElement.length!=1)
			{
				$('.pb-menu-selected').css({width:0});
				return;
			}			
			
			var menu=menuElement.parents('.pb-menu');
			var menuSelected=menu.children('.pb-menu-selected');

			menu.find('ul.sf-menu>ul>li').removeClass('pb-menu-item-selected');

			var width=$(menuElement).actual('innerWidth');
			
			$(menuElement).addClass('pb-menu-item-selected');

			menuSelected.stop().animate({left:parseInt($(menuElement).position().left),width:width},{duration:duration,queue:false,complete:function() 
			{

			}});		
		};
		
		/**********************************************************************/
		
		this.scrollTo=function()
		{
			var hash=window.location.hash.substring(1);

			if($.trim(hash).length==0) return;
			if(hash=='page') return;

			var object=$('.pb-line-css-group-'+hash+':first');

			if(object.length!=1) return;

			var offset=0;
			if($option.sticky_enable==1)
				offset=-1*$this.actual('height');
			
			var option={};
			
			option.offset=offset;
			
			if($option.animation_enable==1)
			{
				option.easing=$option.animation_easing;
				option.duration=parseInt($option.animation_duration);
			}
			
			option.onAfter=function() { window.location.hash='page'; }

			$.scrollTo(object,option);				
		}
		
		/**********************************************************************/

		this.setMenuResponsive=function()
		{
			var width=$this.parent().actual('width');
			
			if(width<$option.responsive_level)
			{
				$this.find('.pb-menu').css('display','none');
				$this.find('.pb-menu-responsive').css('display','block');
			}
			else
			{
				$this.find('.pb-menu').css('display','block');
				$this.find('.pb-menu-responsive').css('display','none');				
			}
		};
		
		/**********************************************************************/
		
		this.getValueFromClass=function(object,pattern)
		{
			var reg=new RegExp(pattern);
			var className=jQuery(object).attr('class').split(' ');

			for(var i in className)
			{
				if(reg.test(className[i]))
					return(className[i].substring(pattern.length));
			}

			return(false);		
		};
		
		/**********************************************************************/
	};
	
	/**************************************************************************/
	
	$.fn.menu=function(option) 
	{
		var menu=new Menu(this,option);
		menu.build();
	};
	
	/**************************************************************************/

})(jQuery,document,window);