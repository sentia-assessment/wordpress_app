/******************************************************************************/
/******************************************************************************/

;(function($,doc,win) 
{
	"use strict";
	
	var preloaderImage=function(object,option)
	{
		/**********************************************************************/
		
		var $this=$(object);
		
		var $optionDefault=
		{
			onReady						:	'',
			animationEnable				:	true
		};
		
		var $option=$.extend($optionDefault,option);

		/**********************************************************************/

		this.create=function() 
		{
			var self=this;
	
			$this.find('.pb-preloader-image img,.theme-preloader-image img').each(function() 
			{
				$(this).one('load',function()
				{
					if(!$option.animationEnable)
					{
						$(this).css({'opacity':1});
						$(this).parents('.pb-preloader-image,.theme-preloader-image').removeClass('pb-preloader-image').removeClass('theme-preloader-image');
						$(this).unbind('load');
					}
					else
					{
						var height=parseInt($(this).actual('height'));

						$(this).parents('.pb-preloader-image,.theme-preloader-image').animate({height:height},500,function() 
						{
							$(this).css({'height':'auto'});

							$(this).find('img').animate({opacity:1},300,function() 
							{					
								$(this).parents('.pb-preloader-image,.theme-preloader-image').removeClass('pb-preloader-image').removeClass('theme-preloader-image');
								$(this).unbind('load');
							}); 
						});
					}
				}).each(function() 
				{
					if(this.complete) $(this).load();
				});
			});	
			
			if($option.onReady!==false) self.wait();
		};
		
		/**********************************************************************/
	
		this.wait=function()
		{
			var self=this;
			var preloaderClock=window.setTimeout(function() 
			{
				if($this.find('.pb-preloader-image,.theme-preloader-image').length)
					self.wait($this,$option.onReady);
				else
				{
					window.clearTimeout(preloaderClock);
					$option.onReady.call();
				}
			},200);	
		};
		
		/**********************************************************************/
		
		this.getRandom=function(min,max)
		{
			return((Math.floor(Math.random()*(max-min)))+min);
		};

		/**********************************************************************/
	}
	
	/**************************************************************************/
	
	$.fn.preloaderImage=function(option) 
	{
		var element=new preloaderImage(this,option);
		element.create();
	};
	
	/**************************************************************************/

})(jQuery,document,window);