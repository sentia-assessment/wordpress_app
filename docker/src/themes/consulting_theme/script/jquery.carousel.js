/******************************************************************************/
/******************************************************************************/

;(function($,doc,win) 
{
	"use strict";
	
	var Carousel=function(object,option)
	{
		/**********************************************************************/
		
		var $this=$(object);
		
		var $optionDefault=
		{
			count	:	3
		};
		
		var $option=$.extend($optionDefault,option);

		/**********************************************************************/

		this.create=function() 
		{
			var self=this;
			var header=$this.prev('h5');
			
			if(header.length==1)
			{
				header.append('<a href="#" class="theme-carousel-pagination-next"></a>');
				header.append('<a href="#" class="theme-carousel-pagination-prev"></a>');
			}
	
			$this.children('ul').carouFredSel(
			{
				auto				:	false,
				circular			:	true,
				infinite			:	true,
				direction			:	'up',
				items				: 
				{
					start			:	0,
					height			:	'variable',
					visible			:	parseInt($option.count),
					minimum			:	parseInt($option.count)
				},
				scroll				: 
				{
					items			:	1,
					duration		:	200,
					pauseOnHover	:	true
				},
				swipe				:
				{
					onTouch			:	true,
					onMouse			:	true
				},
				next				:	$this.parent().find('.theme-carousel-pagination-next'),
				prev				:	$this.parent().find('.theme-carousel-pagination-prev'),
				onCreate			:	function()
				{
					$(window).trigger('resize');
				}
			});	
			
			$().windowDimensionListener({'change':function(width,height) 
			{
				if(width || height) self.updateCarouselSize();
			}});			

			$(window).load(function()
			{
				self.updateCarouselSize();
			});
		};
		
		/**********************************************************************/
		
		this.updateCarouselSize=function()
		{
			var carousel=$this.find('.caroufredsel_wrapper').children('ul:first');
			carousel.trigger('updateSizes');
		};
		
		/**********************************************************************/
	}
	
	/**************************************************************************/
	
	$.fn.carousel=function(option) 
	{
		var element=new Carousel(this,option);
		element.create();
	};
	
	/**************************************************************************/

})(jQuery,document,window);