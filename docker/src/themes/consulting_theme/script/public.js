
/******************************************************************************/
/******************************************************************************/

jQuery(document).ready(function($) 
{	
	/**************************************************************************/
	
	$.fn.qtip.zindex=10;
	
	/**************************************************************************/
	
	$('.widget_search label').inFieldLabels();
	
	/**************************************************************************/
	
	$('.widget_recent_comments>ul>li').each(function() 
	{
		$(this).html('<span>'+$(this).html().replace('<a','</span><a'));
	});
	
	$('.widget_recent_comments').css('display','block');
	
	/**************************************************************************/
	
	$('.widget_nav_menu a').bind('click',function(e) 
	{
		if($(this).attr('href').substr(0,1)=='#')
		{			
			e.preventDefault();
			
			var menu=$(this).parents('.widget_nav_menu');
					
			menu.find('li').removeClass('current-menu-item current-menu-ancestor current_page_item');
			menu.find('ul>li>ul').css('display','none');
			
			$(this).parents('ul.sub-menu').css('display','block');
			$(this).parents('li').addClass('current-menu-item');
			
			$(this).next('ul.sub-menu').css('display','block');
		}
	});
	
	$('.widget_nav_menu').each(function() 
	{
		$(this).find('li.current-menu-item,li.current_page_item').parents('ul.sub-menu').css('display','block');
	});
	
	/**************************************************************************/
	
	$('.theme-post').preloaderImage(
	{
		onReady						:	function() 
		{

		}
	});
	
	/**************************************************************************/
	
	$('.theme-post-image').hoverImage();
	
	/**************************************************************************/
	
	$('body.single-post .theme-post-image-type-image>a').fancyBoxLaunch();
	
	/**************************************************************************/
	
	if(themeOption.goToPageTop.enable==1)
	{
		$(window).bind('hashchange',function(e) 
		{
			e.preventDefault();
			
			var hash=window.location.hash.substring(1);
			if(hash==themeOption.goToPageTop.hash)
			{
				var options={};
				
				if(themeOption.goToPageTop.animation_enable==1)
					options={duration:parseInt(themeOption.goToPageTop.animation_duration),easing:themeOption.goToPageTop.animation_easing};
				
				$.scrollTo(0,options);
			}
		});
	};
	
	/**************************************************************************/
	
	if(themeOption.rightClick.enable==0)
	{
		document.oncontextmenu=function() { return false; }
		$(document).mousedown(function(e)
		{ 
			if(e.button==2) return false; 
			return true; 
		});
	}
	
	if(themeOption.selection.enable==0)
	{
		$('body').attr('unselectable','on').css('user-select','none').on('selectstart',false);
	}
	
	/**************************************************************************/
	
	var selectorPrefix='.theme-footer-top>';
	var selectorExclude=':not(.pb-layout-responsive-0)';
	
	$(selectorPrefix+'.theme-layout-50x50'+selectorExclude).responsiveElement({className:'theme-responsive-column-a'});
	$(selectorPrefix+'.theme-layout-33x33x33'+selectorExclude).responsiveElement({width:650,className:'theme-responsive-column-a'});
	$(selectorPrefix+'.theme-layout-25x25x25x25'+selectorExclude).responsiveElement({width:650,className:'theme-responsive-column-a'});
	$(selectorPrefix+'.theme-layout-66x33'+selectorExclude).responsiveElement({className:'theme-responsive-column-a'});
	$(selectorPrefix+'.theme-layout-33x66'+selectorExclude).responsiveElement({className:'theme-responsive-column-a'});
	$(selectorPrefix+'.theme-layout-25x75'+selectorExclude).responsiveElement({className:'theme-responsive-column-a'});
	$(selectorPrefix+'.theme-layout-75x25'+selectorExclude).responsiveElement({className:'theme-responsive-column-a'});	

	/**************************************************************************/
});

/******************************************************************************/
/******************************************************************************/