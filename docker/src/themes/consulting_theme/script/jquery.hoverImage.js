/******************************************************************************/
/******************************************************************************/

;(function($,doc,win) 
{
	"use strict";
	
	var hoverImage=function(object,option)
	{
		/**********************************************************************/
		
		var $this=$(object);
		
		var $optionDefault=
		{
	
		};
		
		var $option=$.extend($optionDefault,option);

		/**********************************************************************/

		this.create=function() 
		{
			var self=this;
			
			$this.hover(function() 
			{
				var link=$(this).find('a');
				var hover=link.children('span');
				
				if((link.hasClass('pb-preloader-image')) || (link.hasClass('theme-preloader-image'))) return;

				hover.css({'left':'-100%'}).stop().animate({left:0},{duration:self.getRandom(100,500),easing:'easeOutQuint',complete:function() 
				{

				}});

			},function() 
			{
				var link=$(this).find('a');
				var hover=link.children('span');
				
				if((link.hasClass('pb-preloader-image')) || (link.hasClass('theme-preloader-image'))) return;

				hover.stop().animate({left:'100%'},{duration:self.getRandom(100,500),easing:'easeOutQuint',complete:function() 
				{

				}});	
			});
		};
		
		/**********************************************************************/
		
		this.getRandom=function(min,max)
		{
			return((Math.floor(Math.random()*(max-min)))+min);
		};
		
		/**********************************************************************/
	}
	
	/**************************************************************************/
	
	$.fn.hoverImage=function(option) 
	{
		var element=new hoverImage(this,option);
		element.create();
	};
	
	/**************************************************************************/

})(jQuery,document,window);