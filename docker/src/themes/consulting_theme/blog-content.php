<?php
		global $post,$aropwt_parentPost;

		$Blog=new ThemeBlog();
		$Page=new ThemePage();
		$Post=new ThemePost();
		$Validation=new ThemeValidation();
		$WidgetArea=new ThemeWidgetArea();
		
		$widgetAreaData=$WidgetArea->getWidgetAreaByPost($aropwt_parentPost->post,true,true);

		$query=$Blog->getPost();
		$postCount=count($query->posts);

		if($postCount)
		{
?>
		<div class="theme-clear-fix">
			
			<ul class="theme-reset-list theme-clear-fix theme-blog">
<?php
			while($query->have_posts())
			{
				$query->the_post();
		
				$visibleOption=array();

				$visibleOption['post_tag_visible']=ThemeOption::getGlobalOption($post,'tag_visible');
				$visibleOption['post_author_visible']=ThemeOption::getGlobalOption($post,'author_visible');
				$visibleOption['post_category_visible']=ThemeOption::getGlobalOption($post,'category_visible');
				$visibleOption['post_comment_count_visible']=ThemeOption::getGlobalOption($post,'comment_count_visible') && comments_open(get_the_id());
?>
				<li id="post-<?php the_ID(); ?>" <?php post_class('theme-clear-fix theme-post'); ?>>
<?php
				if(has_post_thumbnail())
				{
?>
					<div class="theme-post-section-top">
					
						<div class="theme-post-image theme-post-image-type-link <?php echo $Page->getImageClass($widgetAreaData['location']); ?>">
							<a href="<?php the_permalink(); ?>" class="theme-preloader-image">
								<?php echo get_the_post_thumbnail(get_the_ID(),$Page->getImageClass($widgetAreaData['location'])); ?>
								<span><span></span></span>
							</a>
						</div>
						
					</div>
<?php
				}
?>				
					<div class="theme-post-section-bottom">
						
						<div class="theme-post-section-bottom-left">
<?php
				if(is_sticky())
				{
?>
							<div class="theme-post-sticky-box"></div>							
<?php
				}

				$Post->formatPostDate($post->post_date,$day,$month,$year);
?>
							<div class="theme-post-date">
								<span><?php echo $day; ?></span>
								<span><?php echo esc_html($month.' '.$year); ?></span>
							</div>
<?php
				if($visibleOption['post_comment_count_visible']==1)
				{
?>
							<div class="theme-post-comment-count">
								<span><?php comments_number('0','1','%'); ?></span>
								<span><?php echo esc_html('Replies'); ?></span>
							</div>
<?php
				}
?>
						</div>
						
						<div class="theme-post-section-bottom-right">
							
							<h4 class="theme-post-header"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<div class="theme-post-excerpt"><?php the_excerpt(); ?></div>
			
							<a href="<?php the_permalink(); ?>" class="theme-post-button-more"><?php echo esc_html('Continue reading','atrium'); ?></a>
<?php
				if(($visibleOption['post_author_visible']) || ($visibleOption['post_author_visible']) || ($visibleOption['post_author_visible']))
				{
?>
							<div class="theme-post-meta">
<?php
					if($visibleOption['post_author_visible']==1)
					{
?>
								<div class="theme-post-meta-author">
									<?php echo esc_html('By').' '.get_the_author(); ?>
								</div>
<?php
					}

					if($visibleOption['post_category_visible']==1)
					{
						$category=get_the_category(get_the_ID());
						$count=count($category);
						
						if($count)
						{		
?>
								<div class="theme-post-meta-category">
									
									<ul class="theme-reset-list">
<?php
							foreach($category as $index=>$value)
							{
								$separator=$index==$count-1 ? '' : ',&nbsp;';
								$title=$Validation->isEmpty($value->description) ? sprintf(__('View all posts filed under %s','atrium'),$value->name) : strip_tags(apply_filters('category_description',$value->description,$value));
?>
									<li><a href="<?php echo get_category_link($value->term_id); ?>" title="<?php esc_attr($title); ?>"><?php echo esc_html($value->name); ?></a><?php echo $separator; ?></li>
<?php
							}
?>
									</ul>

								</div>
<?php
						}
					}

					if($visibleOption['post_tag_visible']==1)
					{
						$i=0;
						$tag=get_the_tags();
						
						if($tag) 
						{
							$count=count($tag);
?>
								<div class="theme-post-meta-tag">
									
									<ul class="theme-reset-list">
			
<?php
							foreach($tag as $index=>$value)
							{
								$i++;
								$separator=$i==$count ? '' : ',&nbsp;';
?>
										<li><a href="<?php echo get_tag_link($value->term_id); ?>"><?php echo $value->name; ?></a><?php echo $separator; ?></li>
<?php
							}
?>
									</ul>
			
								</div>
<?php
						}
					}
?>
							</div>
<?php
				}
?>
						</div>
						
					</div>
	
				</li>
<?php
			}
?>
			</ul>
			
			<div class="theme-blog-pagination">
<?php	
		global $wp_rewrite;  
		
		$total=$query->max_num_pages; 
		$current=max(1,ThemeHelper::getPageNumber()); 
		
		$pagination=array
		(
			'base'			=>	@add_query_arg('paged','%#%'),
			'format'		=>	'',
			'current'		=>	$current,  
			'total'			=>	$total,  
			'next_text'		=>	__('Next','atrium'),
			'prev_text'		=>	__('Previous','atrium')
		);

		if($wp_rewrite->using_permalinks() )
			$pagination['base']=user_trailingslashit(trailingslashit(remove_query_arg('s',get_pagenum_link(1))).'page/%#%/','paged');

		if(is_search()) $pagination['add_args']=array('s'=>urlencode(get_query_var('s')));

		echo paginate_links($pagination);
?>
			</div>
	
		</div>
<?php
		}
		else echo do_shortcode('[pb_notice icon="cross.png" icon_bg_color="ED2B2B"][pb_notice_first_line]'.__('Error','atrium').'[/pb_notice_first_line][pb_notice_second_line]'.__('No post found.','atrium').'[/pb_notice_second_line][/pb_notice]');