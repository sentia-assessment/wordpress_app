<?php
		global $aropwt_parentPost;

		$WidgetArea=new ThemeWidgetArea();
		$Validation=new ThemeValidation();

		if((ThemeOption::getOption('footer_enable')==1) && ((ThemeOption::getOption('footer_top_enable')==1) || (ThemeOption::getOption('footer_bottom_enable')==1)))
		{
?>
				<div class="theme-footer">
<?php
			if((ThemeOption::getOption('footer_top_enable')==1) && (!is_home()))
			{
?>
					<div class="theme-footer-top">
<?php
				$widgetAreaData=$WidgetArea->getWidgetAreaByPost($aropwt_parentPost->post,false,true);
				$layout=$WidgetArea->setWidgetAreaLayout($widgetAreaData['id']);
?>
						<div class="theme-main <?php echo $layout; ?>">
							<?php $WidgetArea->create($widgetAreaData); ?>
						</div>
<?php
				$WidgetArea->unsetWidgetAreaLayout();
?>
					</div>
<?php
			}
			if(ThemeOption::getOption('footer_bottom_enable')==1)
			{
				$content=ThemeOption::getOption('footer_bottom_content');
				if($Validation->isNotEmpty($content))
				{
?>
					<div class="theme-footer-bottom theme-clear-fix">
						<div class="theme-main theme-clear-fix">
							<?php echo do_shortcode($content); ?>
						</div>
					</div>
<?php
				}
			}
?>
				</div>
<?php
		}
		
		wp_footer();
		
		if($Validation->isNotEmpty(ThemeOption::getOption('custom_js_code')))
		{
?>
				<script type="text/javascript">
					<?php echo ThemeOption::getOption('custom_js_code'); ?>
				</script>
<?php
		}
?>
			</body>
			
		</html>
<?php ob_end_flush();