<?php
		/* Template Name: Main */

		get_header(); 

		global $aropwt_parentPost;

		if(is_object($aropwt_parentPost->post))
			echo apply_filters('the_content',do_shortcode(get_post_field('post_content',$aropwt_parentPost->post->ID)));
		
		get_footer(); 