<?php ob_start(); ?>
<!DOCTYPE html>
<?php
		global $post,$aropwt_parentPost;

		$Post=new ThemePost();
		$Validation=new ThemeValidation();
		
		if(($aropwt_parentPost=$Post->getPost())===false) 
		{
			$aropwt_parentPost=new stdClass();
			$aropwt_parentPost->post=$post;
		}
?>
		<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

			<head>

				<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
				<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
				<meta name="format-detection" content="telephone=no"/>
<?php
		if(ThemeOption::getOption('responsive_mode_enable')==1)
		{
?>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<?php
		}
?>
				<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
				<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<?php				
		if($Validation->isNotEmpty(ThemeOption::getOption('favicon_url')))
		{
?>
				<link rel="shortcut icon" href="<?php echo ThemeHelper::esc_attr(ThemeOption::getOption('favicon_url')); ?>" />
<?php
		}
					
		wp_head(); 
?>
			</head>

			<body <?php body_class(); ?>>
<?php
		$Page=new ThemePage();
		$Menu=new ThemeMenu();
		
		echo $Menu->create();
		
		if(!is_home()) $Page->displayHeader();