<?php
		if(is_404())
		{
			global $aropwt_parentPost;
			
			if(is_object($aropwt_parentPost->post))
				echo apply_filters('the_content',do_shortcode(get_post_field('post_content',$aropwt_parentPost->post->ID)));
		}
		else
		{
			if(have_posts())
			{
				while(have_posts())
				{
					the_post();
					the_content();
?>
					<div class="theme-blog-pagination">
<?php 
				wp_link_pages(array
				(
					'before'		=>	null,
					'after'			=>	null,
					'link_before'	=> '<span>',
					'link_after'	=> '</span>'
				)); 
?>
					</div>
<?php
				}
			}
		}