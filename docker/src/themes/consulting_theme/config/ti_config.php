<?php

/******************************************************************************/
/******************************************************************************/

define('PLUGIN_THEME_INSTALLER_THEME_CONTEXT','theme');
define('PLUGIN_THEME_INSTALLER_THEME_CLASS_PREFIX','');
define('PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX','theme_option');

define('PLUGIN_THEME_INSTALLER_SKIN_OPTION_NAME','theme_skin');

/***/

TIData::set('import','option',array('widget_data','content_data','theme_option'));

/***/

TIData::set('post_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_blog_category_post_id',array('title'=>'Blog','postType'=>'page'));
TIData::set('post_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_blog_archive_post_id',array('title'=>'Blog','postType'=>'page'));
TIData::set('post_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_blog_search_post_id',array('title'=>'Blog','postType'=>'page'));

/***/

TIData::set('post_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_post_content_widget_area',array('title'=>'Sidebar Single Post','postType'=>PLUGIN_THEME_INSTALLER_THEME_CONTEXT.'_widget_area'));
TIData::set('post_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_post_footer_widget_area',array('title'=>'Footer Default','postType'=>PLUGIN_THEME_INSTALLER_THEME_CONTEXT.'_widget_area'));

/***/

TIData::set('post_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_page_404_page_id',array('title'=>'Page Not Found','postType'=>'page'));
TIData::set('post_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_page_footer_widget_area',array('title'=>'Footer Default','postType'=>PLUGIN_THEME_INSTALLER_THEME_CONTEXT.'_widget_area'));

/***/

TIData::set('term_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_post_menu_top',array('title'=>'Default','taxonomy'=>'nav_menu'));
TIData::set('term_id',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_page_menu_top',array('title'=>'Default','taxonomy'=>'nav_menu'));

/***/

TIData::set('path',PLUGIN_THEME_INSTALLER_THEME_OPTION_PREFIX.'_menu_logo_src',array('title'=>'logo','postType'=>'attachment'));

/***/

TIData::set('post_id','page_on_front',array('title'=>'Home','postType'=>'page'));

TIData::set('option','show_on_front','page');
TIData::set('option','posts_per_page',10);
TIData::set('option','thread_comments',1);
TIData::set('option','thread_comments_depth',2);
TIData::set('option','page_comments',1);
TIData::set('option','comments_per_page',5);
TIData::set('option','permalink_structure','/%postname%/');

TIData::set('option','show_avatars',1);
TIData::set('option','avatar_default','mystery');

TIData::set('option','blogname','Atrium - Responsive One Page WordPress Theme');
TIData::set('option','blogdescription','');

/******************************************************************************/
/******************************************************************************/