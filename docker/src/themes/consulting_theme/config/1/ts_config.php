<?php

/******************************************************************************/
/******************************************************************************/

TSCSSRule::addMenuItem(array
(
	'id'				=>	'accordion',
	'label'				=>	__('Accordion','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'base',
	'label'				=>	__('Base','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'blockquote',
	'label'				=>	__('Blockquote','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'blog',
	'label'				=>	__('Blog','atrium')
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'button',
	'label'				=>	__('Button','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'call_to_action',
	'label'				=>	__('Call to action','atrium')
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'comment_form',
	'label'				=>	__('Comment form','atrium')
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'comments_list',
	'label'				=>	__('Comments list','atrium')
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'contact_form',
	'label'				=>	__('Contact form','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'counter_box',
	'label'				=>	__('Counter box','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'dropcap',
	'label'				=>	__('Dropcap','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	


TSCSSRule::addMenuItem(array
(
	'id'				=>	'feature',
	'label'				=>	__('Feature','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'gallery',
	'label'				=>	__('Gallery','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'header',
	'label'				=>	__('Header','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'header_and_subheader',
	'label'				=>	__('Header and subheader','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));		

TSCSSRule::addMenuItem(array
(
	'id'				=>	'image_carousel',
	'label'				=>	__('Image carousel','atrium')
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'italicized_section',
	'label'				=>	__('Italicized section','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'menu',
	'label'				=>	__('Menu','atrium')
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'nivo_slider',
	'label'				=>	__('Nivo slider','atrium')
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'notices',
	'label'				=>	__('Notices','atrium')
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'preformatted_text',
	'label'				=>	__('Preformatted text','atrium')
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'recent_posts',
	'label'				=>	__('Recent posts','atrium')
));		

TSCSSRule::addMenuItem(array
(
	'id'				=>	'social_icons',
	'label'				=>	__('Social icons','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'sticky_posts',
	'label'				=>	__('Sticky posts','atrium')
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'tab',
	'label'				=>	__('Tab','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));

TSCSSRule::addMenuItem(array
(
	'id'				=>	'team',
	'label'				=>	__('Team','atrium')
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'testimonial',
	'label'				=>	__('Testimonial','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'twitter_user_timeline',
	'label'				=>	__('Twitter user timeline','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_archives',
	'label'				=>	__('Widget: Archives','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_categories',
	'label'				=>	__('Widget: Categories','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_calendar',
	'label'				=>	__('Widget: Calendar','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_custom_menu',
	'label'				=>	__('Widget: Custom menu','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_meta',
	'label'				=>	__('Widget: Meta','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_pages',
	'label'				=>	__('Widget: Pages','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_recent_comments',
	'label'				=>	__('Widget: Recent comments','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_recent_posts',
	'label'				=>	__('Widget: Recent posts','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_rss',
	'label'				=>	__('Widget: RSS','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_search',
	'label'				=>	__('Widget: Search','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_tags',
	'label'				=>	__('Widget: Tags','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_theme_most_commented',
	'label'				=>	__('Widget: Theme most commented','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

TSCSSRule::addMenuItem(array
(
	'id'				=>	'widget_theme_recent_posts',
	'label'				=>	__('Widget: Theme recent posts','atrium'),
	'children'			=>	array
	(
		array
		(
			'id'		=>	'default',
			'label'		=>	__('Default','atrium')
		),
		array
		(
			'id'		=>	'footer',
			'label'		=>	__('Footer','atrium')
		),			
	)
));	

/******************************************************************************/
/******************************************************************************/

TSCSSRule::addPanel('accordion_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'accordion_default_tab_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-default',
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-default a'
				)					
			),
			array
			(
				'id'				=>	'accordion_default_tab_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-default'
				)					
			),					
			array
			(
				'id'				=>	'accordion_default_tab_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-default'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'accordion_default_tab_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover',
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover a'
				)					
			),
			array
			(
				'id'				=>	'accordion_default_tab_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover'
				)					
			),					
			array
			(
				'id'				=>	'accordion_default_tab_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Active state','atrium'),
			'subheader'				=>	__('Active tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'accordion_default_tab_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-active',
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-active a'
				)					
			),
			array
			(
				'id'				=>	'accordion_default_tab_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-active'
				)					
			),					
			array
			(
				'id'				=>	'accordion_default_tab_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-accordion.ui-accordion .ui-accordion-header.ui-state-active'
				)					
			)
		)
	)
));	

TSCSSRule::addPanel('accordion_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'accordion_footer_tab_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-default',
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-default a'
				)					
			),
			array
			(
				'id'				=>	'accordion_footer_tab_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-default'
				)					
			),					
			array
			(
				'id'				=>	'accordion_footer_tab_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-default'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'accordion_footer_tab_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover',
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover a'
				)					
			),
			array
			(
				'id'				=>	'accordion_footer_tab_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover'
				)					
			),					
			array
			(
				'id'				=>	'accordion_footer_tab_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Active state','atrium'),
			'subheader'				=>	__('Active tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'accordion_footer_tab_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-active',
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-active a'
				)					
			),
			array
			(
				'id'				=>	'accordion_footer_tab_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-active'
				)					
			),					
			array
			(
				'id'				=>	'accordion_footer_tab_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-accordion.ui-accordion .ui-accordion-header.ui-state-active'
				)					
			)
		)
	)
));	

/******************************************************************************/

TSCSSRule::addPanel('base',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Selection','atrium'),
			'subheader'				=>	__('Selection colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'base_selection_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'::selection'
				),
				'customCSS'			=>	'::-moz-selection { color:__VALUE__; }'
			),
			array
			(
				'id'				=>	'base_selection_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'::selection'
				),
				'customCSS'			=>	'::-moz-selection { background-color:__VALUE__; }'
			)
		)
	)
));

TSCSSRule::addPanel('base_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'base_default_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'body',
					'input',
					'select',
					'textarea'		
				)					
			),
			array
			(
				'id'				=>	'base_default_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'a'
				)					
			),
			array
			(
				'id'				=>	'base_default_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'a:hover'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('base_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Footer top','atrium'),
			'subheader'				=>	__('Footer top colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'base_footer_top_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'div.theme-footer-top'
				)					
			),
			array
			(
				'id'				=>	'base_footer_top_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'div.theme-footer-top a'
				)					
			),	
			array
			(
				'id'				=>	'base_footer_top_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top a:hover'
				)					
			),	
			array
			(
				'id'				=>	'base_footer_top_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(
					'div.theme-footer-top'
				)					
			),
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Footer bottom','atrium'),
			'subheader'				=>	__('Footer bottom colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'base_footer_bottom_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'889097',
				'selector'			=>	array
				(
					'div.theme-footer-bottom'
				)					
			),
			array
			(
				'id'				=>	'base_footer_bottom_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-bottom a'
				)					
			),
			array
			(
				'id'				=>	'base_footer_bottom_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-bottom a:hover'
				)					
			),
			array
			(
				'id'				=>	'base_footer_bottom_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'21272E',
				'selector'			=>	array
				(
					'div.theme-footer-bottom'
				)					
			),
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('blockquote_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blockquote_default_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border left color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-blockquote',
                    '.page-gutenberg-block .wp-block-quote',
                    '.page-gutenberg-block .wp-block-pullquote'
				),
				'additionalRule'	=>	array
				(
					'padding-left'			=>	'20',
					'border-style'			=>	'solid',
					'border-left-width'		=>	'2'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blockquote_default_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-blockquote',
                    '.page-gutenberg-block .wp-block-quote',
                    '.page-gutenberg-block .wp-block-quote cite',
                    '.page-gutenberg-block .wp-block-pullquote p',
                    '.page-gutenberg-block .wp-block-pullquote cite'
				)
			),
			array
			(
				'id'				=>	'blockquote_default_normal_state_default_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (default state)','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-blockquote a',
                    '.page-gutenberg-block .wp-block-quote p a',
                    '.page-gutenberg-block .wp-block-pullquote p a'
				)
			),
			array
			(
				'id'				=>	'blockquote_default_hover_state_default_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-blockquote a:hover',
                    '.page-gutenberg-block .wp-block-quote p a:hover',
                    '.page-gutenberg-block .wp-block-pullquote p a:hover'
				)
			)
		)
	)
));

TSCSSRule::addPanel('blockquote_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blockquote_footer_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border left color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .pb-blockquote'
				),
				'additionalRule'	=>	array
				(
					'padding-left'			=>	'20',
					'border-style'			=>	'solid',
					'border-left-width'		=>	'2'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blockquote_footer_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.theme-footer-top .pb-blockquote'
				)
			),
			array
			(
				'id'				=>	'blockquote_footer_normal_state_default_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (default state)','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.theme-footer-top .pb-blockquote a'
				)
			),
			array
			(
				'id'				=>	'blockquote_footer_hover_state_default_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.theme-footer-top .pb-blockquote a:hover'
				)
			)
		)
	)
));

/**************************************************************************************/

TSCSSRule::addPanel('blog',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-post .theme-post-date'
				)					
			),
			array
			(
				'id'				=>	'blog_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-post .theme-post-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Comments count box','atrium'),
			'subheader'				=>	__('Comments count colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_comment_count_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-post .theme-post-comment-count'
				)					
			),
			array
			(
				'id'				=>	'blog_comment_count_box_border_color',
				'type'				=>	'border-bottom-color',
				'label'				=>	__('Border bottom color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.theme-post .theme-post-comment-count'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'h4.theme-post-header',
					'h4.theme-post-header a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Excerpt','atrium'),
			'subheader'				=>	__('Excerpt colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_excerpt_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-post-excerpt>p'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Button "Continue reading"','atrium'),
			'subheader'				=>	__('Button "Continue reading" colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_button_more_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'a.theme-post-button-more'
				)					
			),
			array
			(
				'id'				=>	'blog_button_more_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'a.theme-post-button-more'
				)					
			),
			array
			(
				'id'				=>	'blog_button_more_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'a.theme-post-button-more'
				)					
			),
			array
			(
				'id'				=>	'blog_button_more_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'a.theme-post-button-more:hover'
				)					
			),
			array
			(
				'id'				=>	'blog_button_more_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'a.theme-post-button-more:hover'
				)					
			),
			array
			(
				'id'				=>	'blog_button_more_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'a.theme-post-button-more:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Post meta box','atrium'),
			'subheader'				=>	__('Post meta box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_post_meta_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'div.theme-post-meta'
				)					
			),
			array
			(
				'id'				=>	'blog_post_meta_box_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'div.theme-post-meta a'
				)					
			),
			array
			(
				'id'				=>	'blog_post_meta_box_link_color_hover',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'div.theme-post-meta a:hover'
				)					
			),
			array
			(
				'id'				=>	'blog_post_meta_box_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'div.theme-post-meta'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Post navigation','atrium'),
			'subheader'				=>	__('Post navigation colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_post_navigation_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'div.theme-post-navigation a'
				)					
			),
			array
			(
				'id'				=>	'blog_post_navigation_text_color_hover',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'div.theme-post-navigation a:hover'
				)					
			),
			array
			(
				'id'				=>	'blog_post_navigation_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'div.theme-post-navigation a'
				)					
			),
			array
			(
				'id'				=>	'blog_post_navigation_border_color_hover',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-post-navigation a:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'blog_pagination_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>a'
				)					
			),
			array
			(
				'id'				=>	'blog_pagination_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>a'
				)					
			),
			array
			(
				'id'				=>	'blog_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>a'
				)					
			),					
			array
			(
				'id'				=>	'blog_pagination_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>a:hover'
				)					
			),
			array
			(
				'id'				=>	'blog_pagination_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>a:hover'
				)					
			),
			array
			(
				'id'				=>	'blog_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>a:hover'
				)					
			),
			array
			(
				'id'				=>	'blog_pagination_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (active state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>span'
				)					
			),
			array
			(
				'id'				=>	'blog_pagination_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>span'
				)					
			),
			array
			(
				'id'				=>	'blog_pagination_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-blog-pagination>span'
				)					
			)
		)
	)
));		

/******************************************************************************/

TSCSSRule::addPanel('button_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'button_default_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-button>a'
				)					
			),
			array
			(
				'id'				=>	'button_default_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-button>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'button_default_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-button>a:hover'
				)					
			),
			array
			(
				'id'				=>	'button_default_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.pb-button>a:hover'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('button_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'button_footer_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-button>a'
				)					
			),
			array
			(
				'id'				=>	'button_footer_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-button>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'button_footer_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-button>a:hover'
				)					
			),
			array
			(
				'id'				=>	'button_footer_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-button>a:hover'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('call_to_action',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'call_to_action_default_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'F2F2F2',
				'selector'			=>	array
				(
					'.pb-call-to-action'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_box_border_top_color',
				'type'				=>	'border-top-color',
				'label'				=>	__('Border top color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-call-to-action'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_box_border_right_color',
				'type'				=>	'border-right-color',
				'label'				=>	__('Border right color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-call-to-action'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_box_border_bottom_color',
				'type'				=>	'border-bottom-color',
				'label'				=>	__('Border bottom color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-call-to-action'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_box_border_left_color',
				'type'				=>	'border-left-color',
				'label'				=>	__('Border left color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-call-to-action'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('First line','atrium'),
			'subheader'				=>	__('First line colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'call_to_action_default_first_line_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>h3'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Second line','atrium'),
			'subheader'				=>	__('Second line colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'call_to_action_default_second_line_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>h5'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Button','atrium'),
			'subheader'				=>	__('Button colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'call_to_action_default_button_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>div>a'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_button_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>div>a'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_button_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>div>a'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_button_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>div>a:hover'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_button_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>div>a:hover'
				)					
			),
			array
			(
				'id'				=>	'call_to_action_default_button_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-call-to-action .pb-call-to-action-box>div>a:hover'
				)					
			)
		)
	)
));		

/******************************************************************************/

TSCSSRule::addPanel('comment_form',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Fields','atrium'),
			'subheader'				=>	__('Field colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_form_default_normal_state_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'#comment-form textarea',
					'#comment-form input[type="text"]'
				)
			),
			array
			(
				'id'				=>	'comment_form_default_normal_state_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'F2F2F2',
				'selector'			=>	array
				(
					'#comment-form textarea',
					'#comment-form input[type="text"]'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'comment_form_default_normal_state_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'F2F2F2',
				'selector'			=>	array
				(
					'#comment-form textarea',
					'#comment-form input[type="text"]'
				)					
			),
			array
			(
				'id'				=>	'comment_form_default_focus_state_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color - focus','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'#comment-form textarea:focus',
					'#comment-form input[type="text"]:focus'
				)					
			),
			array
			(
				'id'				=>	'comment_form_default_focus_state_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color - focus','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'#comment-form textarea:focus',
					'#comment-form input[type="text"]:focus'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'comment_form_default_focus_state_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color - focus','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'#comment-form textarea:focus',
					'#comment-form input[type="text"]:focus'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Buttons','atrium'),
			'subheader'				=>	__('Button colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_form_default_normal_state_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'#comment-form input[type="submit"]'
				)					
			),
			array
			(
				'id'				=>	'comment_form_default_normal_state_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'#comment-form input[type="submit"]'			
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)
			),
			array
			(
				'id'				=>	'comment_form_default_normal_state_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'#comment-form input[type="submit"]'
				)					
			),
			array
			(
				'id'				=>	'comment_form_default_hover_state_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'#comment-form input[type="submit"]:hover'
				)					
			),
			array
			(
				'id'				=>	'comment_form_default_hover_state_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'#comment-form input[type="submit"]:hover'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)				
			),
			array
			(
				'id'				=>	'comment_form_default_hover_state_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'#comment-form input[type="submit"]:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Notices','atrium'),
			'subheader'				=>	__('Notice colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_form_default_notice_success_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color - success notice','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'#comment-form .qtip.pb-qtip.pb-qtip-success'
				)					
			),
			array
			(
				'id'				=>	'comment_form_default_notice_success_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color - success notice','atrium'),
				'default'			=>	'67CC2A',
				'selector'			=>	array
				(
					'#comment-form .qtip.pb-qtip.pb-qtip-success'
				),
				'customCSS'			=>	'#comment-form  .qtip.pb-qtip.pb-qtip-success .qtip-tip { border-color:__VALUE__; }'
			),
			array
			(
				'id'				=>	'comment_form_default_notice_error_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color - error notice','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'#comment-form .qtip.pb-qtip.pb-qtip-error'
				)					
			),
			array
			(
				'id'				=>	'comment_form_default_notice_error_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color - error notice','atrium'),
				'default'			=>	'FF9600',
				'selector'			=>	array
				(
					'#comment-form .qtip.pb-qtip.pb-qtip-error'
				),
				'customCSS'			=>	'#comment-form .qtip.pb-qtip.pb-qtip-error .qtip-tip { border-color:__VALUE__; }'
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('comments_list',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Avatar','atrium'),
			'subheader'				=>	__('Avatar colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_list_avatar_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-avatar'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid'							
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Content','atrium'),
			'subheader'				=>	__('Content colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_list_content_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-content'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Reply button','atrium'),
			'subheader'				=>	__('Reply button colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_list_button_reply_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-content .theme-comment-reply'
				)					
			),
			array
			(
				'id'				=>	'comment_list_button_reply_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-content .theme-comment-reply'
				)					
			),
			array
			(
				'id'				=>	'comment_list_button_reply_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-content .theme-comment-reply'
				)					
			),
			array
			(
				'id'				=>	'comment_list_button_reply_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-content .theme-comment-reply:hover'
				)					
			),
			array
			(
				'id'				=>	'comment_list_button_reply_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-content .theme-comment-reply:hover'
				)					
			),
			array
			(
				'id'				=>	'comment_list_button_reply_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-content .theme-comment-reply:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Comment meta box','atrium'),
			'subheader'				=>	__('Comment meta box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_list_meta_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-meta'
				)					
			),
			array
			(
				'id'				=>	'comment_list_meta_box_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-meta a'
				)					
			),
			array
			(
				'id'				=>	'comment_list_meta_box_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'#comments_list>ul>li .theme-comment-meta'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'comment_list_pagination_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.theme-comment-pagination a'
				)					
			),
			array
			(
				'id'				=>	'comment_list_pagination_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.theme-comment-pagination a'
				)					
			),
			array
			(
				'id'				=>	'comment_list_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-comment-pagination a'
				)					
			),					
			array
			(
				'id'				=>	'comment_list_pagination_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.theme-comment-pagination a:hover'
				)					
			),
			array
			(
				'id'				=>	'comment_list_pagination_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-comment-pagination a:hover'
				)					
			),
			array
			(
				'id'				=>	'comment_list_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-comment-pagination a:hover'
				)					
			),
			array
			(
				'id'				=>	'comment_list_pagination_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (active state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-comment-pagination span'
				)					
			),
			array
			(
				'id'				=>	'comment_list_pagination_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-comment-pagination span'
				)					
			),
			array
			(
				'id'				=>	'comment_list_pagination_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-comment-pagination span'
				)					
			)
		)
	)
));		

/******************************************************************************/

TSCSSRule::addPanel('contact_form_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Fields','atrium'),
			'subheader'				=>	__('Field colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'contact_form_default_normal_state_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.pb-contact-form select',
					'.pb-contact-form textarea',
					'.pb-contact-form input[type="text"]',
					'.pb-contact-form input[type="password"]',
					'.pb-contact-form>div>ul>li label.pb-infield-label'
				)
			),
			array
			(
				'id'				=>	'contact_form_default_normal_state_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'F2F2F2',
				'selector'			=>	array
				(
					'.pb-contact-form select',
					'.pb-contact-form textarea',
					'.pb-contact-form input[type="text"]',
					'.pb-contact-form input[type="password"]'							
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'contact_form_default_normal_state_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'F2F2F2',
				'selector'			=>	array
				(
					'.pb-contact-form select',
					'.pb-contact-form textarea',
					'.pb-contact-form input[type="text"]',
					'.pb-contact-form input[type="password"]'							
				)					
			),
			array
			(
				'id'				=>	'contact_form_default_focus_state_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (on focus)','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.pb-contact-form select:focus',
					'.pb-contact-form textarea:focus',
					'.pb-contact-form input[type="text"]:focus',
					'.pb-contact-form input[type="password"]:focus',						
				)					
			),
			array
			(
				'id'				=>	'contact_form_default_focus_state_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (on focus)','atrium'),
				'default'			=>	'F2F2F2',
				'selector'			=>	array
				(
					'.pb-contact-form select:focus',
					'.pb-contact-form textarea:focus',
					'.pb-contact-form input[type="text"]:focus',
					'.pb-contact-form input[type="password"]:focus'						
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'contact_form_default_focus_state_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (on focus)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-contact-form select:focus',
					'.pb-contact-form textarea:focus',
					'.pb-contact-form input[type="text"]:focus',
					'.pb-contact-form input[type="password"]:focus',						
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Buttons','atrium'),
			'subheader'				=>	__('Button colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'contact_form_default_normal_state_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.pb-contact-form input[type="submit"]',
					'.pb-contact-form input[type="button"]'						
				)					
			),
			array
			(
				'id'				=>	'contact_form_default_normal_state_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-contact-form input[type="button"]',
					'.pb-contact-form input[type="submit"]',							
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)
			),
			array
			(
				'id'				=>	'contact_form_default_normal_state_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-contact-form input[type="submit"]',
					'.pb-contact-form input[type="button"]'							
				)					
			),
			array
			(
				'id'				=>	'contact_form_default_hover_state_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.pb-contact-form input[type="submit"]:hover',
					'.pb-contact-form input[type="button"]:hover'							
				)					
			),
			array
			(
				'id'				=>	'contact_form_default_hover_state_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-contact-form input[type="submit"]:hover',
					'.pb-contact-form input[type="button"]:hover'						
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)				
			),
			array
			(
				'id'				=>	'contact_form_default_hover_state_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-contact-form input[type="submit"]:hover',
					'.pb-contact-form input[type="button"]:hover'							
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Notices','atrium'),
			'subheader'				=>	__('Notice colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'contact_form_default_notice_success_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color - success notice','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-contact-form .qtip.pb-qtip.pb-qtip-success'
				)					
			),
			array
			(
				'id'				=>	'contact_form_default_notice_success_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color - success notice','atrium'),
				'default'			=>	'67CC2A',
				'selector'			=>	array
				(
					'.pb-contact-form .qtip.pb-qtip.pb-qtip-success'
				),
				'customCSS'			=>	'.pb-contact-form .qtip.pb-qtip.pb-qtip-success .qtip-tip { border-color:__VALUE__; }'
			),
			array
			(
				'id'				=>	'contact_form_default_notice_error_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color - error notice','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-contact-form .qtip.pb-qtip.pb-qtip-error'
				)					
			),
			array
			(
				'id'				=>	'contact_form_default_notice_error_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color - error notice','atrium'),
				'default'			=>	'FF9600',
				'selector'			=>	array
				(
					'.pb-contact-form .qtip.pb-qtip.pb-qtip-error'
				),
				'customCSS'			=>	'.pb-contact-form .qtip.pb-qtip.pb-qtip-error .qtip-tip { border-color:__VALUE__; }'
			)
		)
	)
));

TSCSSRule::addPanel('contact_form_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Fields','atrium'),
			'subheader'				=>	__('Field colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'contact_form_footer_normal_state_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'div.theme-footer-top select',
					'div.theme-footer-top textarea',
					'div.theme-footer-top input[type="text"]',
					'div.theme-footer-top input[type="password"]',
					'div.theme-footer-top .pb-contact-form>div>ul>li label.pb-infield-label'
				)
			),
			array
			(
				'id'				=>	'contact_form_footer_normal_state_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top select',
					'div.theme-footer-top textarea',
					'div.theme-footer-top input[type="text"]',
					'div.theme-footer-top input[type="password"]'							
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'contact_form_footer_normal_state_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top select',
					'div.theme-footer-top textarea',
					'div.theme-footer-top input[type="text"]',
					'div.theme-footer-top input[type="password"]'							
				)					
			),
			array
			(
				'id'				=>	'contact_form_footer_focus_state_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (on focus)','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'div.theme-footer-top select:focus',
					'div.theme-footer-top textarea:focus',
					'div.theme-footer-top input[type="text"]:focus',
					'div.theme-footer-top input[type="password"]:focus',						
				)					
			),
			array
			(
				'id'				=>	'contact_form_footer_focus_state_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (on focus)','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top select:focus',
					'div.theme-footer-top textarea:focus',
					'div.theme-footer-top input[type="text"]:focus',
					'div.theme-footer-top input[type="password"]:focus'						
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'contact_form_footer_focus_state_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (on focus)','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top select:focus',
					'div.theme-footer-top textarea:focus',
					'div.theme-footer-top input[type="text"]:focus',
					'div.theme-footer-top input[type="password"]:focus',						
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Buttons','atrium'),
			'subheader'				=>	__('Button colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'contact_form_footer_normal_state_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'div.theme-footer-top input[type="submit"]',
					'div.theme-footer-top input[type="button"]'						
				)					
			),
			array
			(
				'id'				=>	'contact_form_footer_normal_state_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top input[type="button"]',
					'div.theme-footer-top input[type="submit"]',							
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)
			),
			array
			(
				'id'				=>	'contact_form_footer_normal_state_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(
					'div.theme-footer-top input[type="submit"]',
					'div.theme-footer-top input[type="button"]'							
				)					
			),
			array
			(
				'id'				=>	'contact_form_footer_hover_state_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top input[type="submit"]:hover',
					'div.theme-footer-top input[type="button"]:hover'							
				)					
			),
			array
			(
				'id'				=>	'contact_form_footer_hover_state_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top input[type="submit"]:hover',
					'div.theme-footer-top input[type="button"]:hover'						
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)				
			),
			array
			(
				'id'				=>	'contact_form_footer_hover_state_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(
					'div.theme-footer-top input[type="submit"]:hover',
					'div.theme-footer-top input[type="button"]:hover'							
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Notices','atrium'),
			'subheader'				=>	__('Notice colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'contact_form_footer_notice_success_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color - success notice','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-contact-form .pb-qtip.pb-qtip-success.qtip'
				)					
			),
			array
			(
				'id'				=>	'contact_form_footer_notice_success_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color - success notice','atrium'),
				'default'			=>	'67CC2A',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-contact-form .pb-qtip.pb-qtip-success.qtip'
				),
				'customCSS'			=>	'div.theme-footer-top .pb-contact-form .pb-qtip.pb-qtip-success.qtip .qtip-tip { border-color:__VALUE__; }'
			),
			array
			(
				'id'				=>	'contact_form_footer_notice_error_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color - error notice','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-contact-form .pb-qtip.pb-qtip-error.qtip'
				)					
			),
			array
			(
				'id'				=>	'contact_form_footer_notice_error_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color - error notice','atrium'),
				'default'			=>	'FF9600',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-contact-form .pb-qtip.pb-qtip-error.qtip'
				),
				'customCSS'			=>	'div.theme-footer-top .pb-contact-form .pb-qtip.pb-qtip-error.qtip .qtip-tip { border-color:__VALUE__; }'
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('counter_box_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Counter','atrium'),
			'subheader'				=>	__('Counter colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_default_counter_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.pb-counter-box ul>li>div>div.pb-counter-box-counter'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_default_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-counter-box ul>li>div>h4'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Content','atrium'),
			'subheader'				=>	__('Content colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_default_content_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-counter-box ul>li>div>p'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Progress bar','atrium'),
			'subheader'				=>	__('Progress bar colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_default_progress_bar_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-counter-box ul>li>div>div.pb-counter-box-progress-bar>.pb-counter-box-progress-bar-background',
					'.pb-counter-box ul>li>div>div.pb-counter-box-progress-bar>.pb-counter-box-progress-bar-foreground',		
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_default_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-counter-box .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'counter_box_default_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-counter-box .pb-slider-pagination a:hover',
					'.pb-counter-box .pb-slider-pagination a.active',
					'.pb-counter-box .pb-slider-pagination a.selected'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('counter_box_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Counter','atrium'),
			'subheader'				=>	__('Counter colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_footer_counter_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-counter-box ul>li>div>div.pb-counter-box-counter'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_footer_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-counter-box ul>li>div>h4'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Content','atrium'),
			'subheader'				=>	__('Content colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_footer_content_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-counter-box ul>li>div>p'
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Progress bar','atrium'),
			'subheader'				=>	__('Progress bar colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_footer_progress_bar_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-counter-box ul>li>div>div.pb-counter-box-progress-bar>.pb-counter-box-progress-bar-background',
					'div.theme-footer-top .pb-counter-box ul>li>div>div.pb-counter-box-progress-bar>.pb-counter-box-progress-bar-foreground',		
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'counter_box_footer_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-counter-box .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'counter_box_footer_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-counter-box .pb-slider-pagination a:hover',
					'div.theme-footer-top .pb-counter-box .pb-slider-pagination a.active',
					'div.theme-footer-top .pb-counter-box .pb-slider-pagination a.selected'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('dropcap_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'dropcap_default_first_letter_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'p.pb-dropcap>span.pb-dropcap-first-letter'
				)					
			),
			array
			(
				'id'				=>	'dropcap_default_first_letter_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'p.pb-dropcap>span.pb-dropcap-first-letter'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('dropcap_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'dropcap_footer_first_letter_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top p.pb-dropcap>span.pb-dropcap-first-letter'
				)					
			),
			array
			(
				'id'				=>	'dropcap_footer_first_letter_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top p.pb-dropcap>span.pb-dropcap-first-letter'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('feature_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_default_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-feature ul>li>div>.pb-feature-header',
					'.pb-feature ul>li>div>.pb-feature-header a'							
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Content','atrium'),
			'subheader'				=>	__('Content','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_default_content_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-feature ul>li>div>.pb-feature-content'	
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Icon','atrium'),
			'subheader'				=>	__('Icon','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_default_icon_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-feature ul>li>div>.pb-feature-icon'	
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_default_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-feature .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'feature_default_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-feature .pb-slider-pagination a:hover',
					'.pb-feature .pb-slider-pagination a.active',
					'.pb-feature .pb-slider-pagination a.selected'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('feature_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_footer_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-feature ul>li>div>.pb-feature-header',
					'div.theme-footer-top .pb-feature ul>li>div>.pb-feature-header a'							
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Content','atrium'),
			'subheader'				=>	__('Content','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_footer_content_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-feature ul>li>div>.pb-feature-content'	
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Icon','atrium'),
			'subheader'				=>	__('Icon','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_footer_icon_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-feature ul>li>div>.pb-feature-icon'	
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'feature_footer_pagination_normal_state_border_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-feature .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'feature_footer_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-feature .pb-slider-pagination a:hover',
					'div.theme-footer-top .pb-feature .pb-slider-pagination a.active',
					'div.theme-footer-top .pb-feature .pb-slider-pagination a.selected'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('gallery_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box under image','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'gallery_default_normal_state_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-gallery>ul>li>div.pb-gallery-text-box',
					'.wp-caption'
				)					
			),
			array
			(
				'id'				=>	'gallery_default_hover_state_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-gallery>ul>li:hover>div.pb-gallery-text-box',
					'.wp-caption:hover'
				)					
			),
			array
			(
				'id'				=>	'gallery_default_normal_state_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-gallery>ul>li>div.pb-gallery-text-box',
					'.wp-caption'
				)					
			),
			array
			(
				'id'				=>	'gallery_default_hover_state_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.pb-gallery>ul>li:hover>div.pb-gallery-text-box',
					'.wp-caption:hover'
				)					
			),
		)
	)
));

TSCSSRule::addPanel('gallery_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box under image','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'gallery_footer_normal_state_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-gallery>ul>li>div.pb-gallery-text-box'
				)					
			),
			array
			(
				'id'				=>	'gallery_footer_hover_state_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-gallery>ul>li:hover>div.pb-gallery-text-box'
				)					
			),
			array
			(
				'id'				=>	'gallery_footer_normal_state_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-gallery>ul>li>div.pb-gallery-text-box'
				)					
			),
			array
			(
				'id'				=>	'gallery_footer_hover_state_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-gallery>ul>li:hover>div.pb-gallery-text-box'
				)					
			),
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('header_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H1','atrium'),
			'subheader'				=>	__('Header H1','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_default_h1',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'h1',
					'h1 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H2','atrium'),
			'subheader'				=>	__('Header H2','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_default_h2',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'h2',
					'h2 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H3','atrium'),
			'subheader'				=>	__('Header H3','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_default_h3',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'h3',
					'h3 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H4','atrium'),
			'subheader'				=>	__('Header H4','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_default_h4',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'h4',
					'h4 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H5','atrium'),
			'subheader'				=>	__('Header H5','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_default_h5',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'h5',
					'h5 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H6','atrium'),
			'subheader'				=>	__('Header H6','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_default_h6',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'h6',
					'h6 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header underline','atrium'),
			'subheader'				=>	__('Header underline','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_default_underline_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-header .pb-header-underline',
					'h3.comment-reply-title .pb-header-underline'
				)					
			)
		)
	)			
));

TSCSSRule::addPanel('header_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H1','atrium'),
			'subheader'				=>	__('Header H1','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_footer_h1',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top h1',
					'div.theme-footer-top h1 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H2','atrium'),
			'subheader'				=>	__('Header H2','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_footer_h2',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top h2',
					'div.theme-footer-top h2 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H3','atrium'),
			'subheader'				=>	__('Header H3','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_footer_h3',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top h3',
					'div.theme-footer-top h3 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H4','atrium'),
			'subheader'				=>	__('Header H4','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_footer_h4',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top h4',
					'div.theme-footer-top h4 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H5','atrium'),
			'subheader'				=>	__('Header H5','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_footer_h5',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top h5',
					'div.theme-footer-top h5 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header H6','atrium'),
			'subheader'				=>	__('Header H6','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_footer_h6',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top h6',
					'div.theme-footer-top h6 a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header underline','atrium'),
			'subheader'				=>	__('Header underline','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_footer_underline_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-header .pb-header-underline'
				)					
			)
		)
	)			
));

/******************************************************************************/

TSCSSRule::addPanel('header_and_subheader_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Subheader text color','atrium'),
			'subheader'				=>	__('Subheader text color','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_subheader_default_subheader_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-header-subheader .pb-subheader span'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('header_and_subheader_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Subheader text color','atrium'),
			'subheader'				=>	__('Subheader text color','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'header_subheader_footer_subheader_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-header-subheader .pb-subheader span'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('image_carousel',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'image_carousel_default_box_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-image-carousel>.caroufredsel_wrapper>ul>li>div>div.pb-image-carousel-box>.pb-main>span',
					'.pb-image-carousel>.caroufredsel_wrapper>ul>li>div>div.pb-image-carousel-box>.pb-main>a>span'
				)					
			),
			array
			(
				'id'				=>	'image_carousel_default_box_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Header text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-image-carousel>.caroufredsel_wrapper>ul>li>div>div.pb-image-carousel-box>.pb-main span.pb-image-carousel-box-header'
				)					
			),
			array
			(
				'id'				=>	'image_carousel_default_box_subheader_text_color',
				'type'				=>	'color',
				'label'				=>	__('Subheader text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-image-carousel>.caroufredsel_wrapper>ul>li>div>div.pb-image-carousel-box>.pb-main span.pb-image-carousel-box-subheader'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'image_carousel_default_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-image-carousel .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'image_carousel_default_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-image-carousel .pb-slider-pagination a:hover',
					'.pb-image-carousel .pb-slider-pagination a.active',
					'.pb-image-carousel .pb-slider-pagination a.selected'
				)					
			)
		)				
	)
));	

/******************************************************************************/

TSCSSRule::addPanel('italicized_section_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text color','atrium'),
			'subheader'				=>	__('Text color','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'text_italic_default_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-text-italic p'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('italicized_section_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text color','atrium'),
			'subheader'				=>	__('Text color','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'text_italic_footer_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-text-italic p'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('menu',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Background color','atrium'),
			'subheader'				=>	__('Background color of menu','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'menu_logo_default_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-menu-logo'
				)					
			),
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Menu items on first level','atrium'),
			'subheader'				=>	__('Styles of menu items on first level','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'menu_logo_default_first_level_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li>a'
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_first_level_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu.pb-menu-hover-first-level>ul>li:hover>a'
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_first_level_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (active state)','atrium'),
				'default'			=>	'139641',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li.current-menu-item>a',
					'.pb-menu-logo .pb-menu>ul>li.current-menu-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li.current-page-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li.current-menu-item:hover>a',
					'.pb-menu-logo .pb-menu>ul>li.current-menu-ancestor:hover>a',
					'.pb-menu-logo .pb-menu>ul>li.current-page-ancestor:hover>a',
					'.pb-menu-logo .pb-menu .pb-menu-selected'							
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_first_level_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li>a'	
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_first_level_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu.pb-menu-hover-first-level>ul>li:hover>a'	
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_first_level_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (active state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li.current-menu-item>a',
					'.pb-menu-logo .pb-menu>ul>li.current-menu-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li.current-page-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li.current-menu-item:hover>a',
					'.pb-menu-logo .pb-menu>ul>li.current-menu-ancestor:hover>a',
					'.pb-menu-logo .pb-menu>ul>li.current-page-ancestor:hover>a'						
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_first_level_underline',
				'type'				=>	'color',
				'label'				=>	__('Background color of underline','atrium'),
				'default'			=>	'139641',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu.pb-menu-hover-first-level>ul>li>a>span',
					'.pb-menu-logo .pb-menu.pb-menu-hover-first-level>ul>li>a:hover>span'						
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Menu items on next levels','atrium'),
			'subheader'				=>	__('Styles of menu items on next levels','atrium')	
		),
		'field'						=>	array					
		(					
			array
			(
				'id'				=>	'menu_logo_default_next_level_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'139641',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li ul>li>a'
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_next_level_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'107E36',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu.pb-menu-hover-next-level>ul>li ul>li:hover>a'
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_next_level_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (active state)','atrium'),
				'default'			=>	'107E36',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-item>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-page-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-item:hover>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-ancestor:hover>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-page-ancestor:hover>a'						
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_next_level_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li ul>li>a'
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_next_level_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu.pb-menu-hover-next-level>ul>li ul>li:hover>a'
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_next_level_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (active state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-item>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-page-ancestor>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-item:hover>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-menu-ancestor:hover>a',
					'.pb-menu-logo .pb-menu>ul>li ul>li.current-page-ancestor:hover>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Mobile menu','atrium'),
			'subheader'				=>	__('Mobile menu colors','atrium')	
		),
		'field'						=>	array					
		(					
			array
			(
				'id'				=>	'menu_logo_default_mobile_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu-responsive select'
				)					
			),
			array
			(
				'id'				=>	'menu_logo_default_mobile_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu-responsive select'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'menu_logo_default_mobile_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-menu-logo .pb-menu-responsive select'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('nivo_slider',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'nivo_slider_default_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-nivo-slider .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'nivo_slider_default_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-nivo-slider .pb-slider-pagination a:hover',
					'.pb-nivo-slider .pb-slider-pagination a.active'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('notices',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'notice_default_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content'
				)					
			),
			array
			(
				'id'				=>	'notice_default_box_border_top_color',
				'type'				=>	'border-top-color',
				'label'				=>	__('Border top color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content'
				)					
			),
			array
			(
				'id'				=>	'notice_default_box_border_right_color',
				'type'				=>	'border-right-color',
				'label'				=>	__('Border right color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content'
				)					
			),
			array
			(
				'id'				=>	'notice_default_box_border_bottom_color',
				'type'				=>	'border-bottom-color',
				'label'				=>	__('Border bottom color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content'
				)					
			),
			array
			(
				'id'				=>	'notice_default_box_border_left_color',
				'type'				=>	'border-left-color',
				'label'				=>	__('Border left color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('First line','atrium'),
			'subheader'				=>	__('First line colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'notice_default_first_line_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content>h5'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Second line','atrium'),
			'subheader'				=>	__('Second line colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'notice_default_second_line_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content>p'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Icon','atrium'),
			'subheader'				=>	__('Icon colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'notice_default_icon_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-notice'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Timeline','atrium'),
			'subheader'				=>	__('Timeline colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'notice_default_timeline_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content>.pb-notice-timeline>div'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Close button','atrium'),
			'subheader'				=>	__('Close button colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'notice_default_close_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'CCCCCC',
				'selector'			=>	array
				(
					'.pb-notice>.pb-notice-content .pb-notice-close-button'
				)
			)
		)
	),
));

/******************************************************************************/

TSCSSRule::addPanel('preformatted_text',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Preformatted text','atrium'),
			'subheader'				=>	__('Preformatted text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'preformatted_text_default_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-preformatted-text',
                    '.page-gutenberg-block .wp-block-verse',
                    '.page-gutenberg-block .wp-block-preformatted'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',							
				)
			),
			array
			(
				'id'				=>	'preformatted_text_default_normal_state_label_color',
				'type'				=>	'color',
				'label'				=>	__('Label color','atrium'),
				'default'			=>	'888888',
				'selector'			=>	array
				(
					'.pb-preformatted-text a'
				)					
			),
			array
			(
				'id'				=>	'preformatted_text_default_normal_state_label_color',
				'type'				=>	'color',
				'label'				=>	__('Label color (hover state)','atrium'),
				'default'			=>	'888888',
				'selector'			=>	array
				(
					'.pb-preformatted-text a:hover'
				)					
			),

			array
			(
				'id'				=>	'preformatted_text_default_pre_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'999999',
				'selector'			=>	array
				(
					'.pb-preformatted-text pre',
                    '.page-gutenberg-block .wp-block-verse',
                    '.page-gutenberg-block .wp-block-preformatted'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('recent_posts',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date','atrium'),
			'subheader'				=>	__('Date colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'recent_post_default_date_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li>div>div.pb-recent-post-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Comment count box','atrium'),
			'subheader'				=>	__('Comment count box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'recent_post_default_comment_count_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li div.pb-recent-post-image .pb-recent-post-comment-count>span.pb-recent-post-comment-count-value',
				),
				'customCSS'			=>	'.pb-recent-post>ul>li div.pb-recent-post-image .pb-recent-post-comment-count>span.pb-recent-post-comment-count-arrow { border-top-color:__VALUE__; }'
			),
			array
			(
				'id'				=>	'recent_post_default_comment_count_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li div.pb-recent-post-image .pb-recent-post-comment-count>span.pb-recent-post-comment-count-value',
				),
			)
		)
	),		
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'recent_post_default_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li>div>.pb-recent-post-header',
					'.pb-recent-post>ul>li>div>.pb-recent-post-header>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Excerpt','atrium'),
			'subheader'				=>	__('Excerpt colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'recent_post_default_excerpt_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li>div>div.pb-recent-post-excerpt'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Post meta box','atrium'),
			'subheader'				=>	__('Post meta box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'recent_post_default_meta_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li>div>div.pb-recent-post-meta>div'
				)					
			),
			array
			(
				'id'				=>	'recent_post_default_meta_box_link_text_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li>div>div.pb-recent-post-meta>div a'
				)					
			),
			array
			(
				'id'				=>	'recent_post_default_meta_box_link_text_color_hover',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li>div>div.pb-recent-post-meta>div a:hover'
				)					
			),
			array
			(
				'id'				=>	'recent_post_default_meta_box_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-recent-post>ul>li>div>div.pb-recent-post-meta'
				)					
			)
		)
	)
));	

/******************************************************************************/

TSCSSRule::addPanel('social_icons_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) state colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'social_icon_default_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-social-icon>li>a'
				)					
			),
			array
			(
				'id'				=>	'social_icon_default_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-social-icon>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover state colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'social_icon_default_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-social-icon>li>a:hover'
				)					
			),
			array
			(
				'id'				=>	'social_icon_default_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-social-icon>li>a:hover'
				)					
			)
		)
	)
));	

TSCSSRule::addPanel('social_icons_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) state colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'social_icon_footer_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-social-icon>li>a'
				)					
			),
			array
			(
				'id'				=>	'social_icon_footer_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-social-icon>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover state colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'social_icon_footer_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-social-icon>li>a:hover'
				)					
			),
			array
			(
				'id'				=>	'social_icon_footer_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-social-icon>li>a:hover'
				)					
			)
		)
	)
));	

/******************************************************************************/

TSCSSRule::addPanel('sticky_posts',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Box','atrium'),
			'subheader'				=>	__('Box colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'sticky_post_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-post .theme-post-sticky-box'
				)					
			)
		)
	)
));	

/******************************************************************************/

TSCSSRule::addPanel('tab_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'tab_default_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default',
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default a'
				)					
			),
			array
			(
				'id'				=>	'tab_default_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default'
				)					
			),
			array
			(
				'id'				=>	'tab_default_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'tab_default_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover',
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover a'
				)					
			),
			array
			(
				'id'				=>	'tab_default_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover'
				)					
			),
			array
			(
				'id'				=>	'tab_default_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Active state','atrium'),
			'subheader'				=>	__('Active state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'tab_default_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active',
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active a'
				)					
			),
			array
			(
				'id'				=>	'tab_default_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active'
				)					
			),
			array
			(
				'id'				=>	'tab_default_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('tab_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'tab_footer_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default',
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default a'
				)					
			),
			array
			(
				'id'				=>	'tab_footer_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default'
				)					
			),
			array
			(
				'id'				=>	'tab_footer_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-default'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'tab_footer_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover',
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover a'
				)					
			),
			array
			(
				'id'				=>	'tab_footer_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover'
				)					
			),
			array
			(
				'id'				=>	'tab_footer_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Active state','atrium'),
			'subheader'				=>	__('Active state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'tab_footer_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active',
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active a'
				)					
			),
			array
			(
				'id'				=>	'tab_footer_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active'
				)					
			),
			array
			(
				'id'				=>	'tab_footer_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-tab.ui-tabs>.ui-tabs-nav .ui-state-active'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('team',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Image caption box','atrium'),
			'subheader'				=>	__('Image caption box colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'team_default_normal_state_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-image-box .pb-team-image-caption'
				)					
			),
			array
			(
				'id'				=>	'team_default_hover_state_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-image-box:hover .pb-team-image-caption'
				)					
			),
			array
			(
				'id'				=>	'team_default_normal_state_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-image-box .pb-team-image-caption'
				)					
			),
			array
			(
				'id'				=>	'team_default_hover_state_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(	
					'.pb-team>ul>li .pb-team-image-box:hover .pb-team-image-caption'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Social icons','atrium'),
			'subheader'				=>	__('Social icons color','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'team_default_normal_state_social_icon_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-team>ul>li ul.pb-team-social-icon-box>li>a'
				)					
			),
			array
			(
				'id'				=>	'team_default_hover_state_social_icon_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(
					'.pb-team>ul>li ul.pb-team-social-icon-box>li>a:hover'
				)					
			)
		)
	),	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'team_default_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-text-box>h3.pb-team-text-box-member-name'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Subheader','atrium'),
			'subheader'				=>	__('Subheader colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'team_default_subheader_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-text-box>div.pb-team-text-box-member-position'
				)					
			)
		)
	),			
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Description','atrium'),
			'subheader'				=>	__('Description colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'team_default_description_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-text-box p'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Progress bar','atrium'),
			'subheader'				=>	__('Progress bar colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'team_default_progress_bar_label_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color of the label','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-skill>div'
				)					
			),
			array
			(
				'id'				=>	'team_default_progress_bar_background_track_color',
				'type'				=>	'background-color',
				'label'				=>	__('Foreground color of progress bar','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-skill .pb-team-progress-bar>.pb-team-progress-bar-foreground'
				)					
			),
			array
			(
				'id'				=>	'team_default_progress_bar_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color of progress bar','atrium'),
				'default'			=>	'F2F2F2',
				'selector'			=>	array
				(
					'.pb-team>ul>li .pb-team-skill .pb-team-progress-bar>.pb-team-progress-bar-background'					
				)					
			)	
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Divider','atrium'),
			'subheader'				=>	__('Divider colors','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'team_default_divider_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.pb-team>ul>li'
				),
				'additionalRule'	=>	array
				(
					'border-style'				=>	'solid',
					'border-top-width'		=>	'1'
				)
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('testimonial_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text color','atrium'),
			'subheader'				=>	__('Text color of testimonials','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'testimonial_default_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-testimonial'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'testimonial_default_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-testimonial .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'testimonial_default_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-testimonial .pb-slider-pagination a:hover',
					'.pb-testimonial .pb-slider-pagination a.active',
					'.pb-testimonial .pb-slider-pagination a.selected'
				)					
			)
		)				
	)
));	

TSCSSRule::addPanel('testimonial_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text color','atrium'),
			'subheader'				=>	__('Text color of testimonials','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'testimonial_footer_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-testimonial'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'testimonial_footer_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-testimonial .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'testimonial_footer_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-testimonial .pb-slider-pagination a:hover',
					'div.theme-footer-top .pb-testimonial .pb-slider-pagination a.active',
					'div.theme-footer-top .pb-testimonial .pb-slider-pagination a.selected'
				)					
			)
		)				
	)
));

/******************************************************************************/

TSCSSRule::addPanel('twitter_user_timeline_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text color','atrium'),
			'subheader'				=>	__('Text color of tweets','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'twitter_user_timeline_default_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.pb-twitter-user-timeline .pb-twitter-user-timeline-list span'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'twitter_user_timeline_default_pagination_border_color',
				'type'				=>	'background-color',
				'label'				=>	__('Bottom border color (all states)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-twitter-user-timeline .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'twitter_user_timeline_default_pagination_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.pb-twitter-user-timeline .pb-slider-pagination a:hover',
					'.pb-twitter-user-timeline .pb-slider-pagination a.active',
					'.pb-twitter-user-timeline .pb-slider-pagination a.selected'
				)					
			)
		)				
	)
));	

TSCSSRule::addPanel('twitter_user_timeline_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text color','atrium'),
			'subheader'				=>	__('Text color of tweets','atrium'),		
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'twitter_user_timeline_footer_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-twitter-user-timeline .pb-twitter-user-timeline-list span'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Pagination','atrium'),
			'subheader'				=>	__('Pagination colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'twitter_user_timeline_footer_pagination_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (all states)','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-twitter-user-timeline .pb-slider-pagination a'
				)					
			),
			array
			(
				'id'				=>	'twitter_user_timeline_footer_pagination_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover and active state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .pb-twitter-user-timeline .pb-slider-pagination a:hover',
					'div.theme-footer-top .pb-twitter-user-timeline .pb-slider-pagination a.active',
					'div.theme-footer-top .pb-twitter-user-timeline .pb-slider-pagination a.selected'
				)					
			)
		)				
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_archives_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_archive_default_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_archive'
				)					
			),
			array
			(
				'id'				=>	'widget_archive_default_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_archive a'
				)					
			),
			array
			(
				'id'				=>	'widget_archive_default_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_archive a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_archive_default_bottom_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Bottom border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_archive>ul>li'
				),
				'additionalRule'	=>	array
				(
					'border-style'				=>	'solid',
					'border-bottom-width'		=>	'1',
					'padding-bottom'			=>	'8',
					'padding-top'				=>	'7'
				)
			)
		)
	)
));

TSCSSRule::addPanel('widget_archives_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_archive_footer_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_archive'
				)					
			),
			array
			(
				'id'				=>	'widget_archive_footer_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_archive a'
				)					
			),
			array
			(
				'id'				=>	'widget_archive_footer_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_archive a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_archive_footer_bottom_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Bottom border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_archive>ul>li'
				),
				'additionalRule'	=>	array
				(
					'border-style'				=>	'solid',
					'border-bottom-width'		=>	'1',
					'border-bottom-style'		=>	'solid',
					'padding-bottom'			=>	'8',
					'padding-top'				=>	'7'
				)
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_categories_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_categories_default_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.widget_categories ul li a'
				)					
			),
			array
			(
				'id'				=>	'widget_categories_default_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_categories ul li a'
				)					
			),					
			array
			(
				'id'				=>	'widget_categories_default_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_categories ul li a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_categories_default_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.widget_categories ul li a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_categories_default_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_categories ul li a:hover'
				)					
			),					
			array
			(
				'id'				=>	'widget_categories_default_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_categories ul li a:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Active state','atrium'),
			'subheader'				=>	__('Active tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_categories_default_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_categories ul li.current-cat a'
				)					
			),
			array
			(
				'id'				=>	'widget_categories_default_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_categories ul li.current-cat a'
				)					
			),					
			array
			(
				'id'				=>	'widget_categories_default_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_categories ul li.current-cat a'
				)					
			)
		)
	)
));	

TSCSSRule::addPanel('widget_categories_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_categories_footer_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li a'
				)					
			),
			array
			(
				'id'				=>	'widget_categories_footer_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li a'
				)					
			),					
			array
			(
				'id'				=>	'widget_categories_footer_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_categories_footer_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_categories_footer_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li a:hover'
				)					
			),					
			array
			(
				'id'				=>	'widget_categories_footer_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li a:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Active state','atrium'),
			'subheader'				=>	__('Active tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_categories_footer_selected_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li.current-cat a'
				)					
			),
			array
			(
				'id'				=>	'widget_categories_footer_selected_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li.current-cat a'
				)					
			),					
			array
			(
				'id'				=>	'widget_categories_footer_selected_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_categories ul li.current-cat a'
				)					
			)
		)
	)
));	

/******************************************************************************/

TSCSSRule::addPanel('widget_calendar_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Caption','atrium'),
			'subheader'				=>	__('Caption colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_calendar_default_caption_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_calendar table tfoot',
					'.widget_calendar table caption'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_calendar_default_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_calendar table thead th'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_default_header_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_calendar table thead th'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_default_header_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_calendar table thead th'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Cells','atrium'),
			'subheader'				=>	__('Cells colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_calendar_default_cell_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_calendar table tbody td',
					'.widget_calendar table tbody td a'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_default_cell_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_calendar table tbody td'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_default_cell_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_calendar table tbody td',
					'.widget_calendar table tbody td a'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_default_cell_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_calendar table tbody td a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_default_cell_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_calendar table tbody td a:hover'
				)						
			)
		)
	)
));


TSCSSRule::addPanel('widget_calendar_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Caption','atrium'),
			'subheader'				=>	__('Caption colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_calendar_footer_caption_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table tfoot',
					'.theme-footer-top .widget_calendar table caption'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_calendar_footer_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table thead th'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_footer_header_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table thead th'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_footer_header_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table thead th'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Cells','atrium'),
			'subheader'				=>	__('Cells colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_calendar_footer_cell_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table tbody td',
					'.theme-footer-top .widget_calendar table tbody td a'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_footer_cell_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table tbody td'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_footer_cell_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table tbody td',
					'.theme-footer-top .widget_calendar table tbody td a'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_footer_cell_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table tbody td a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_calendar_footer_cell_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_calendar table tbody td a:hover'
				)						
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_custom_menu_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Default state','atrium'),
			'subheader'				=>	__('Default state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_custom_menu_default_normal_state_link_text_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_nav_menu ul li a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_default_normal_state_link_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_nav_menu ul li a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_default_normal_state_link_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border bottom color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_nav_menu ul li a'
				),	
				'additionalRule'	=>	array
				(
					'border-bottom-width'	=>	'1',
					'border-bottom-style'	=>	'solid',						
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Other states','atrium'),
			'subheader'				=>	__('Other (hover and active) states colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_custom_menu_default_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_nav_menu ul li a:hover',
					'.widget_nav_menu ul li.current_page_item>a',
					'.widget_nav_menu ul li.current-menu-item>a',
					'.widget_nav_menu ul li.current-menu-ancestor>a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_default_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_nav_menu ul li a:hover',
					'.widget_nav_menu ul li.current_page_item>a',
					'.widget_nav_menu ul li.current-menu-item>a',
					'.widget_nav_menu ul li.current-menu-ancestor>a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_default_hover_state_link_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border bottom color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_nav_menu ul li a:hover',
					'.widget_nav_menu ul li.current_page_item>a',
					'.widget_nav_menu ul li.current-menu-item>a',
					'.widget_nav_menu ul li.current-menu-ancestor>a'
				),	
				'additionalRule'	=>	array
				(
					'border-bottom-width'	=>	'1',
					'border-bottom-style'	=>	'solid',						
				)
			)
		)
	)
));

TSCSSRule::addPanel('widget_custom_menu_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Default state','atrium'),
			'subheader'				=>	__('Default state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_custom_menu_footer_normal_state_link_text_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_nav_menu ul li a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_footer_normal_state_link_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Bankground color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_nav_menu ul li a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_footer_normal_state_link_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border bottom color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_nav_menu ul li a'
				),	
				'additionalRule'	=>	array
				(
					'border-bottom-width'	=>	'1',
					'border-bottom-style'	=>	'solid',						
				)
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Other states','atrium'),
			'subheader'				=>	__('Other (hover and active) states colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_custom_menu_footer_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_nav_menu ul li a:hover',
					'div.theme-footer-top .widget_nav_menu ul li.current_page_item>a',
					'div.theme-footer-top .widget_nav_menu ul li.current-menu-item>a',
					'div.theme-footer-top .widget_nav_menu ul li.current-menu-ancestor>a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_footer_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_nav_menu ul li a:hover',
					'div.theme-footer-top .widget_nav_menu ul li.current_page_item>a',
					'div.theme-footer-top .widget_nav_menu ul li.current-menu-item>a',
					'div.theme-footer-top .widget_nav_menu ul li.current-menu-ancestor>a'
				)					
			),
			array
			(
				'id'				=>	'widget_custom_menu_footer_hover_state_link_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border bottom color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_nav_menu ul li a:hover',
					'div.theme-footer-top .widget_nav_menu ul li.current_page_item>a',
					'div.theme-footer-top .widget_nav_menu ul li.current-menu-item>a',
					'div.theme-footer-top .widget_nav_menu ul li.current-menu-ancestor>a'
				),	
				'additionalRule'	=>	array
				(
					'border-bottom-width'	=>	'1',
					'border-bottom-style'	=>	'solid',						
				)
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_meta_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_meta_default_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_meta a'
				)					
			),					
			array
			(
				'id'				=>	'widget_meta_default_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_meta a:hover'
				)					
			)				
		)
	)
));

TSCSSRule::addPanel('widget_meta_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_meta_footer_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_meta a'
				)					
			),					
			array
			(
				'id'				=>	'widget_meta_footer_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_meta a:hover'
				)					
			)	
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_pages_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_page_default_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_pages a'
				)					
			),					
			array
			(
				'id'				=>	'widget_page_default_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_pages a:hover'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('widget_pages_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text','atrium'),
			'subheader'				=>	__('Text colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_page_footer_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_pages a'
				)					
			),					
			array
			(
				'id'				=>	'widget_page_footer_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'div.theme-footer-top .widget_pages a:hover'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_recent_comments_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Link','atrium'),
			'subheader'				=>	__('Link colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_comment_default_link_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.widget_recent_comments>ul>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('User box','atrium'),
			'subheader'				=>	__('User box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_comment_default_user_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_recent_comments>ul>li>span'
				)					
			),
			array
			(
				'id'				=>	'widget_recent_comment_default_user_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_recent_comments>ul>li>span'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('widget_recent_comments_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Link','atrium'),
			'subheader'				=>	__('Link colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_comment_footer_link_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_recent_comments>ul>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('User box','atrium'),
			'subheader'				=>	__('User box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_comment_footer_user_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_recent_comments>ul>li>span'
				)					
			),
			array
			(
				'id'				=>	'widget_recent_comment_footer_user_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_recent_comments>ul>li>span'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_recent_posts_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Link','atrium'),
			'subheader'				=>	__('Link colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_post_defualt_link_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_recent_entries>ul>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_post_defualt_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_recent_entries>ul>li>span.post-date'
				)					
			),
			array
			(
				'id'				=>	'widget_recent_post_default_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_recent_entries>ul>li>span.post-date'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('widget_recent_posts_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Link','atrium'),
			'subheader'				=>	__('Link colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_post_footer_link_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_recent_entries>ul>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_recent_post_footer_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_recent_entries>ul>li>span.post-date'
				)					
			),
			array
			(
				'id'				=>	'widget_recent_post_footer_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_recent_entries>ul>li>span.post-date'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_rss_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_default_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_rss>ul>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_default_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_rss>ul>li>span.rss-date'
				)					
			),
			array
			(
				'id'				=>	'widget_rss_default_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_rss>ul>li>span.rss-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Content','atrium'),
			'subheader'				=>	__('Content colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_default_content_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'999999',
				'selector'			=>	array
				(
					'.widget_rss>ul>li>div.rssSummary'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Author','atrium'),
			'subheader'				=>	__('Author colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_default_author_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_rss>ul>li>cite'
				)					
			),
			array
			(
				'id'				=>	'widget_rss_default_author_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_rss>ul>li>cite'
				)					
			)
		)
	)
));

TSCSSRule::addPanel('widget_rss_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Header','atrium'),
			'subheader'				=>	__('Header colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_footer_header_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_rss>ul>li>a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_footer_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_rss>ul>li>span.rss-date'
				)					
			),
			array
			(
				'id'				=>	'widget_rss_footer_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_rss>ul>li>span.rss-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Content','atrium'),
			'subheader'				=>	__('Content colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_footer_content_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_rss>ul>li>div.rssSummary'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Author','atrium'),
			'subheader'				=>	__('Author colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_rss_footer_author_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_rss>ul>li>cite'
				)					
			),
			array
			(
				'id'				=>	'widget_rss_footer_author_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_rss>ul>li>cite'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_search_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text field','atrium'),
			'subheader'				=>	__('Text field colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_search_default_normal_state_text_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_search input[type="text"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_normal_state_text_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_search input[type="text"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_normal_state_text_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_search input[type="text"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_focus_state_text_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (on focus)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_search input[type="text"]:focus'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_focus_state_text_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (on focus)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_search input[type="text"]:focus'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_focus_state_text_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (on focus)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_search input[type="text"]:focus'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Submit button','atrium'),
			'subheader'				=>	__('Submit button colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_search_default_normal_state_submit_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.widget_search input[type="submit"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_normal_state_submit_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_search input[type="submit"]'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)
			),
			array
			(
				'id'				=>	'widget_search_default_normal_state_submit_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_search input[type="submit"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_hover_state_submit_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.widget_search input[type="submit"]:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_hover_state_submit_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_search input[type="submit"]:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_search_default_hover_state_submit_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_search input[type="submit"]:hover'
				)					
			)
		)
	),			
));

TSCSSRule::addPanel('widget_search_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Text field','atrium'),
			'subheader'				=>	__('Text field colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_search_footer_normal_state_text_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="text"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_normal_state_text_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="text"]'
				),
				'additionalRule'	=>	array
				(
					'border-width'	=>	'2',
					'border-style'	=>	'solid',						
				)
			),
			array
			(
				'id'				=>	'widget_search_footer_normal_state_text_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="text"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_focus_state_text_field_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (on focus)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="text"]:focus'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_focus_state_text_field_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (on focus)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="text"]:focus'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_focus_state_text_field_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (on focus)','atrium'),
				'default'			=>	'2C343D',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="text"]:focus'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Submit button','atrium'),
			'subheader'				=>	__('Submit button colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_search_footer_normal_state_submit_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="submit"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_normal_state_submit_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="submit"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_normal_state_submit_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="submit"]'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_hover_state_submit_button_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="submit"]:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_hover_state_submit_button_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="submit"]:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_search_footer_hover_state_submit_button_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_search input[type="submit"]:hover'
				)					
			)
		)
	),			
));

/******************************************************************************/

TSCSSRule::addPanel('widget_tags_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_tag_default_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'777777',
				'selector'			=>	array
				(
					'.widget_tag_cloud a'
				)					
			),
			array
			(
				'id'				=>	'widget_tag_default_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_tag_cloud a'
				)					
			),					
			array
			(
				'id'				=>	'widget_tag_default_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_tag_cloud a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_tag_default_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'484C54',
				'selector'			=>	array
				(
					'.widget_tag_cloud a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_tag_default_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_tag_cloud a:hover'
				)					
			),					
			array
			(
				'id'				=>	'widget_tag_default_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_tag_cloud a:hover'
				)					
			)
		)
	)
));	

TSCSSRule::addPanel('widget_tags_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Normal state','atrium'),
			'subheader'				=>	__('Normal (default) tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_tag_footer_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_tag_cloud a'
				)					
			),
			array
			(
				'id'				=>	'widget_tag_footer_normal_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_tag_cloud a'
				)					
			),					
			array
			(
				'id'				=>	'widget_tag_footer_normal_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_tag_cloud a'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Hover state','atrium'),
			'subheader'				=>	__('Hover tab state colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_tag_footer_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_tag_cloud a:hover'
				)					
			),
			array
			(
				'id'				=>	'widget_tag_footer_hover_state_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_tag_cloud a:hover'
				)					
			),					
			array
			(
				'id'				=>	'widget_tag_footer_hover_state_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_tag_cloud a:hover'
				)					
			)
		)
	)
));	

/******************************************************************************/

TSCSSRule::addPanel('widget_theme_most_commented_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_default_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_most_comment>div ul>li>div.theme-widget-post-date'
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_most_comment_default_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_most_comment>div ul>li>div.theme-widget-post-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Title','atrium'),
			'subheader'				=>	__('Title colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_default_title_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_most_comment>div ul>li>a',
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_most_comment_default_title_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_most_comment>div ul>li>a:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Meta box','atrium'),
			'subheader'				=>	__('Meta box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_default_meta_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_most_comment>div ul>li>div.theme-widget-post-meta'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Carousel navigation','atrium'),
			'subheader'				=>	__('Carousel navigation colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_default_normal_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_most_comment .theme-carousel-pagination-prev',
					'.widget_theme_widget_post_most_comment .theme-carousel-pagination-next'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)
			),
			array
			(
				'id'				=>	'widget_theme_post_most_comment_default_hover_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_most_comment .theme-carousel-pagination-prev:hover',
					'.widget_theme_widget_post_most_comment .theme-carousel-pagination-next:hover'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)					
			)
		)
	)
));	

TSCSSRule::addPanel('widget_theme_most_commented_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_footer_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_most_comment>div ul>li>div.theme-widget-post-date'
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_most_comment_footer_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_most_comment>div ul>li>div.theme-widget-post-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Title','atrium'),
			'subheader'				=>	__('Title colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_footer_title_normal_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_most_comment>div ul>li>a'
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_most_comment_footer_title_hover_state_link_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_most_comment>div ul>li>a:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Meta box','atrium'),
			'subheader'				=>	__('Meta box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_footer_meta_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_most_comment>div ul>li>div.theme-widget-post-meta'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Carousel navigation','atrium'),
			'subheader'				=>	__('Carousel navigation colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_most_comment_footer_normal_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_most_comment .theme-carousel-pagination-prev',
					'.theme-footer-top .widget_theme_widget_post_most_comment .theme-carousel-pagination-next'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)
			),
			array
			(
				'id'				=>	'widget_theme_post_most_comment_footer_hover_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_most_comment .theme-carousel-pagination-prev:hover',
					'.theme-footer-top .widget_theme_widget_post_most_comment .theme-carousel-pagination-next:hover'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)					
			)
		)
	)
));

/******************************************************************************/

TSCSSRule::addPanel('widget_theme_recent_posts_default',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_default_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_recent>div ul>li>div.theme-widget-post-date'
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_recent_default_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_recent>div ul>li>div.theme-widget-post-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Title','atrium'),
			'subheader'				=>	__('Title colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_default_title_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_recent>div ul>li>a'
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_recent_default_title_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_recent>div ul>li>a:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Meta box','atrium'),
			'subheader'				=>	__('Meta box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_default_meta_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B0B0B0',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_recent>div ul>li>div.theme-widget-post-meta'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Carousel navigation','atrium'),
			'subheader'				=>	__('Carousel navigation colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_default_normal_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'E8E8E8',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_recent .theme-carousel-pagination-prev',
					'.widget_theme_widget_post_recent .theme-carousel-pagination-next'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)
			),
			array
			(
				'id'				=>	'widget_theme_post_recent_default_hover_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.widget_theme_widget_post_recent .theme-carousel-pagination-prev:hover',
					'.widget_theme_widget_post_recent .theme-carousel-pagination-next:hover'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)					
			)
		)
	)
));	

TSCSSRule::addPanel('widget_theme_recent_posts_footer',array
(	
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Date box','atrium'),
			'subheader'				=>	__('Date box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_footer_date_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_recent>div ul>li>div.theme-widget-post-date'
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_recent_footer_date_box_background_color',
				'type'				=>	'background-color',
				'label'				=>	__('Background color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_recent>div ul>li>div.theme-widget-post-date'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Title','atrium'),
			'subheader'				=>	__('Title colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_footer_title_normal_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Link color','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_recent>div ul>li>a'
				)					
			),
			array
			(
				'id'				=>	'widget_theme_post_recent_footer_title_hover_state_text_color',
				'type'				=>	'color',
				'label'				=>	__('Link color (hover state)','atrium'),
				'default'			=>	'FFFFFF',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_recent>div ul>li>a:hover'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Meta box','atrium'),
			'subheader'				=>	__('Meta box colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_footer_meta_box_text_color',
				'type'				=>	'color',
				'label'				=>	__('Text color','atrium'),
				'default'			=>	'B9BDC2',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_recent>div ul>li>div.theme-widget-post-meta'
				)					
			)
		)
	),
	array
	(
		'description'				=>	array
		(
			'header'				=>	__('Carousel navigation','atrium'),
			'subheader'				=>	__('Carousel navigation colors','atrium')	
		),
		'field'						=>	array					
		(
			array
			(
				'id'				=>	'widget_theme_post_recent_footer_normal_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color','atrium'),
				'default'			=>	'3B444E',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_recent .theme-carousel-pagination-prev',
					'.theme-footer-top .widget_theme_widget_post_recent .theme-carousel-pagination-next'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)
			),
			array
			(
				'id'				=>	'widget_theme_post_recent_footer_hover_state_carousel_pagination_border_color',
				'type'				=>	'border-color',
				'label'				=>	__('Border color (hover state)','atrium'),
				'default'			=>	'15A346',
				'selector'			=>	array
				(
					'.theme-footer-top .widget_theme_widget_post_recent .theme-carousel-pagination-prev:hover',
					'.theme-footer-top .widget_theme_widget_post_recent .theme-carousel-pagination-next:hover'
				),
				'additionalRule'	=>	array
				(
					'border-style'			=>	'solid',
					'border-width'			=>	'2'
				)					
			)
		)
	)
));

/******************************************************************************/
/******************************************************************************/