<?php

/******************************************************************************/
/******************************************************************************/

require_once('define.php');

/******************************************************************************/

$class=scandir(THEME_PATH_CLASS,1);

require_once(THEME_PATH_CLASS.'Theme.Widget.class.php');

foreach($class as $className)
{
	$file=THEME_PATH_CLASS.$className;
	if(is_file($file)) require_once($file);
}

require_once(THEME_PATH_LIBRARY.'tgm_plugin_activation/class-tgm-plugin-activation.php');

/******************************************************************************/
/******************************************************************************/