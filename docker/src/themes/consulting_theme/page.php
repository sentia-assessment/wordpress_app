<?php 
		/* Template Name: Page */

		get_header(); 

		global $aropwt_parentPost;

		$WidgetArea=new ThemeWidgetArea();

		$widgetAreaData=$WidgetArea->getWidgetAreaByPost($aropwt_parentPost->post,true,true);
		$class=$WidgetArea->getWidgetAreaCSSClass($widgetAreaData);
?>
		<div class="theme-page theme-main <?php echo $class; ?>">

			<div class="theme-page-content theme-clear-fix <?php echo $class; ?>">
<?php
		if($widgetAreaData['location']==1)
		{
?>
				<div class="theme-column-left"><?php $WidgetArea->create($widgetAreaData); ?></div>
				<div class="theme-column-right"><?php get_template_part('page','content'); ?></div>
<?php
		}
		elseif($widgetAreaData['location']==2)
		{
?>
				<div class="theme-column-left"><?php get_template_part('page','content'); ?></div>
				<div class="theme-column-right"><?php $WidgetArea->create($widgetAreaData); ?></div>	
<?php
		}
		else get_template_part('page','content');
?>
			</div>

		</div>
<?php 
		get_footer(); 