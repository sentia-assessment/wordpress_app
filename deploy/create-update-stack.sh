#!/bin/bash

usage="Usage: $(basename "$0") region stack-name [aws-cli-opts]

where:
  region       - the AWS region
  stack-name   - the stack name
  aws-cli-opts - extra options passed directly to create-stack/update-stack
"


if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "help" ] || [ "$1" == "usage" ] ; then
  echo "$usage"
  exit -1
fi

shopt -s failglob
set -eu -o pipefail

# Set params
region=$1
StackName=$2

declare -A parameters

parameters["pVpcId"]=$3
parameters["pSolutionName"]=$4
parameters["pDomain"]=$5
parameters["pWordPressUrl"]=$6
parameters["pTLD"]=$7
parameters["pVersion"]=${8}
parameters["pImageUri"]=${9}
parameters["pSSL"]=${10}
parameters["pHostedZoneId"]=${12}

cfn_bucket=${11}

templateURL="https://s3.amazonaws.com/$cfn_bucket/wordpress.yml"

stack_parameters=''
for i in "${!parameters[@]}" ; do
   stack_parameters=$stack_parameters' '$(printf "ParameterKey=%s,ParameterValue=%s" $i ${parameters[${i}]})
done

echo "Checking if stack exists ..."

if ! aws cloudformation describe-stacks --region $1 --stack-name $2 ; then

  echo -e "\nStack does not exist, creating ..."
  aws cloudformation create-stack \
  --stack-name $StackName \
  --template-url $templateURL \
  --parameters $stack_parameters \
  --capabilities CAPABILITY_NAMED_IAM \
  --no-disable-rollback \
  --region $region \
  --output json \

  echo "Waiting for stack to be created ..."
  aws cloudformation wait stack-create-complete \
  --region $region \
  --stack-name $StackName \

else
    
  echo -e "\nStack exists, attempting update ..."

  set +e

  update_output=$(aws cloudformation update-stack \
    --region $region \
    --stack-name $StackName \
    --template-url $templateURL \
    --parameters $stack_parameters \
    --capabilities CAPABILITY_NAMED_IAM \
    --output json \
    2>&1)

  status=$?
  set -e

  echo "$update_output"

  if [ $status -ne 0 ] ; then

    # Don't fail for no-op update
    if [[ $update_output == *"ValidationError"* && $update_output == *"No updates"* ]] ; then
      echo -e "\nFinished create/update - no updates to be performed"
      exit 0
    else
      exit $status
    fi

  fi

  echo "Waiting for stack update to complete ..."
  aws cloudformation wait stack-update-complete \
    --region $region \
    --stack-name $StackName \

fi

echo "Finished create/update successfully!"